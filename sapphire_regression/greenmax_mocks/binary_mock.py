import csv
import random
import os

relay_string = 'Relay State: {}'.format
channel_string = 'Channel: {}'.format

target = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'binary.csv')

with open(target, 'w', newline='') as f:
    writer = csv.writer(f)
    for i in range(1, 17):
        row = [relay_string(1), channel_string(i)]
        writer.writerow(row)
