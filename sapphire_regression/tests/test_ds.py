# test_ds.py
"""Unit tests for the ds module"""
from context import leviton_test
from leviton_test import ds
import unittest
from unittest import mock
from leviton_test import config
from leviton_test.ds import ds_toggle_relay, ds_all_relays_off, TIMEOUT


class DSTestCase(unittest.TestCase):

    @mock.patch('leviton_test.ds.subprocess')
    def test_ds_toggle(self, mock_ds_subprocess):
        ds_toggle_relay(2)
        mock_ds_subprocess.run.assert_called_with([config.GM_DS_EXE_PATH, '2'], timeout=TIMEOUT)

    @unittest.expectedFailure
    @mock.patch('leviton_test.ds.subprocess')
    def test_ds_toggle_fail(self, mock_ds_subprocess):
        ds_toggle_relay(7)
        mock_ds_subprocess.run.assert_not_called()

    @mock.patch('leviton_test.ds.binary_state')
    @mock.patch('leviton_test.ds.subprocess')
    def test_ds_all_relays_off(self, mock_ds_subprocess, mock_ds_binary_state):
        fake_state = { string : 0 for string in ['channel {}'.format(i) for i in range(1,17)]}
        fake_state['channel 1'] = 1
        mock_ds_binary_state.return_value = fake_state
        ds_all_relays_off()
        mock_ds_subprocess.run.assert_called_with([config.GM_DS_EXE_PATH, '1'], timeout=TIMEOUT)


if __name__ == '__main__':
    unittest.main()
