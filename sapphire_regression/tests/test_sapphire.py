# test_view.py
"""This module tests view.py"""

import logging
logging.disable(logging.CRITICAL)

import unittest
from unittest import mock
from unittest.mock import call
from xml.etree.ElementTree import Element, SubElement, tostring

from context import leviton_test
from leviton_test.view import View

class MockXMLTree(object):

    def get_test_xml_tree(self):
        top = Element('top')
        child = SubElement(top, 'node')
        child.set('bounds', '[0,0][10,10]')
        child.set('enabled', 'true')
        child.set('class', 'android.widget.Button')
        child.set('text', 'A')
        child.set('content-desc', '')
        return top


class ViewTestCase(unittest.TestCase, MockXMLTree):

    @mock.patch('leviton_test.adb.etree.parse')
    @mock.patch('leviton_test.adb.ADB')
    def setUp(self, mock_ADB, mock_etree_parse):
        mock_ADB.tree_from_android.return_value = None
        mock_etree_parse.return_value = self.get_test_xml_tree()
        self.view = View()

    def test_view_tree(self):
        self.assertEqual(len(self.view.buttons), 1)
        self.assertEqual(self.view.buttons[0].x, 5)
        self.assertEqual(self.view.buttons[0].y, 5)

    @mock.patch('leviton_test.adb.logging')
    def test_wrong_button_logs(self, mock_logging):
        mock_logging.warning.return_value = None
        self.view.press('Not actually a button')
        self.assertTrue(mock_logging.warning.called)

    @mock.patch('subprocess.run')
    def test_button_press_adb_command(self, mock_subprocess_run):
        self.view.press('A')
        expected = [
            call(['adb', 'shell', 'sendevent', '/dev/input/event1', '1', '330', '1']),
            call(['adb', 'shell', 'sendevent', '/dev/input/event1', '3', '0', '12']),
            call(['adb', 'shell', 'sendevent', '/dev/input/event1', '3', '1', '21']),
            call(['adb', 'shell', 'sendevent', '/dev/input/event1', '0', '0', '0']),
            call(['adb', 'shell', 'sendevent', '/dev/input/event1', '1', '330', '0']),
            call(['adb', 'shell', 'sendevent', '/dev/input/event1', '0', '0', '0'])
        ]
        self.assertEqual(mock_subprocess_run.mock_calls, expected)

if __name__ == '__main__':
    unittest.main()
