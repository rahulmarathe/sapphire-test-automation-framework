# test_canparser.py
import unittest
from context import leviton_test
from leviton_test.canparser import (
    command_type,
    bits_from_msg,
    start_time,
    channel_number,
    fade_time,
    target_level,
    increment_value,
    format_channel_fade_msg,
    format_msg,
    priority_level,
    is_fade_time,
    is_fade_rate,
    hardware_level,
    format_channel_status_publish_msg,
    is_absolute_fade,
    is_relative_fade,
    group_number,
    format_group_status_publish_msg,
)


class GeneralUtilsTestCase(unittest.TestCase):

    def test_bits_from_msg(self):
        s = b'asdfjkl;jkl;jaks;ldf'
        self.assertEqual([int(num, base=2) for num in bits_from_msg(s)], [int(i) for i in s])


class FormatMsgTestCase(unittest.TestCase):

    def test_format_msg_equals_format_channel_fade_msg(self):
        channel_fade_msg = [2, 45, 0, 112, 150, 132, 204]
        self.assertEqual(format_channel_fade_msg(channel_fade_msg), format_msg(channel_fade_msg))

    def test_format_msg_equals_format_channel_status_publish_msg(self):
        channel_status_publish_msg = [0, 50, 3, 114, 0, 143, 215, 0]
        self.assertEqual(format_channel_status_publish_msg(channel_status_publish_msg), format_msg(channel_status_publish_msg))

    def test_format_msg_equals_format_group_status_publish_msg(self):
        group_status_publish_msg = [0, 70, 1, 114, 0, 0, 0, 0]
        self.assertEqual(format_group_status_publish_msg(group_status_publish_msg), format_msg(group_status_publish_msg))


class CommandTypeTestCase(unittest.TestCase):

    def test_channel_fade_match(self):
        self.assertEqual(command_type([2, 45, 0, 112, 150, 132, 204]), 'Channel Fade')

    def test_channel_status_publish_match(self):
        self.assertEqual(command_type([0, 50, 1, 112, 0, 143, 215, 0]), 'Channel Status Publish')

    def test_group_status_publish_match(self):
        self.assertEqual(command_type([0, 70, 0, 112, 0, 0, 0, 0]), 'Group Status Publish')



class ChannelFadeTestCase(unittest.TestCase):

    def test_channel_fade_priority_level(self):
        self.assertEqual(priority_level([2, 45, 0, 112, 150, 132, 204]), 7)

    def test_start_time_000(self):
        self.assertEqual(start_time([2, 45, 0, 112, 150, 132, 204]), 0)

    def test_start_time_001(self):
        self.assertEqual(start_time([2, 45, 0, 114, 150, 132, 204]), 52)

    def test_start_time_010(self):
        self.assertEqual(start_time([2, 45, 0, 116, 150, 132, 204]), 564)

    def test_start_time_011(self):
        self.assertEqual(start_time([2, 45, 0, 118, 150, 132, 204]), 1588)

    def test_start_time_100(self):
        self.assertEqual(start_time([2, 45, 0, 120, 150, 132, 204]), 3636)

    def test_start_time_101(self):
        self.assertEqual(start_time([2, 45, 0, 122, 150, 132, 204]), 7732)

    def test_start_time_110(self):
        self.assertEqual(start_time([2, 45, 0, 124, 150, 132, 204]), 15924)

    def test_start_time_111(self):
        self.assertEqual(start_time([2, 45, 0, 126, 150, 132, 204]), 32308)

    def test_increment_value_000(self):
        self.assertEqual(increment_value([2, 45, 0, 112, 150, 132, 204]), 0.1)

    def test_increment_value_001(self):
        self.assertEqual(increment_value([2, 45, 0, 114, 150, 132, 204]), 1)

    def test_increment_value_010(self):
        self.assertEqual(increment_value([2, 45, 0, 116, 150, 132, 204]), 2)

    def test_increment_value_011(self):
        self.assertEqual(increment_value([2, 45, 0, 118, 150, 132, 204]), 4)

    def test_increment_value_100(self):
        self.assertEqual(increment_value([2, 45, 0, 120, 150, 132, 204]), 8)

    def test_increment_value_101(self):
        self.assertEqual(increment_value([2, 45, 0, 122, 150, 132, 204]), 16)

    def test_increment_value_110(self):
        self.assertEqual(increment_value([2, 45, 0, 124, 150, 132, 204]), 32)

    def test_increment_value_111(self):
        self.assertEqual(increment_value([2, 45, 0, 126, 150, 132, 204]), 64)

    def test_fade_time0(self):
        self.assertEqual(fade_time([2, 45, 0, 112, 150, 132, 204]), 15)

    def test_fade_time1(self):
        self.assertEqual(fade_time([2, 45, 0, 114, 160, 132, 204]), 212)

    def test_fade_time_2(self):
        self.assertEqual(fade_time([2, 45, 0, 120, 200, 132, 204]), 5236)

    def test_channel_number_5(self):
        self.assertEqual(channel_number([2, 45, 0, 112, 150, 132, 204]), 5)

    def test_channel_number_13(self):
        self.assertEqual(channel_number([2, 45, 0, 112, 150, 140, 204]), 13)

    def test_target_level_80_percent(self):
        self.assertEqual(target_level([2, 45, 0, 112, 150, 132, 204]), .8)

    def test_target_level_39_percent(self):
        self.assertEqual(target_level([2, 45, 0, 112, 150, 132, 100]), .39)

    def test_format_channel_fade_msg(self):
        self.assertEqual(format_channel_fade_msg([2, 45, 0, 112, 150, 132, 204]),
            'Channel Fade: channel=5, target=80%, time=15.0, increment=0.1, start=0, priority=7')


class ChannelStatusPublishTestCase(unittest.TestCase):

    def test_priority(self):
        self.assertEqual(priority_level([0, 50, 1, 112, 0, 143, 215, 0]), 7)

    def test_fade_rate_time_0(self):
        self.assertEqual(fade_time([0, 50, 1, 112, 0, 143, 215, 0]), 0)

    def test_fade_rate_time_0(self):
        self.assertEqual(fade_time([0, 50, 1, 114, 0, 143, 215, 0]), 52)

    def test_channel_number(self):
        self.assertEqual(channel_number([0, 50, 1, 114, 0, 143, 215, 0]), 16)

    def test_is_fade_time(self):
        self.assertEqual(is_fade_time([0, 50, 0, 114, 0, 143, 215, 0]), True)

    def test_is_fade_time2(self):
        self.assertEqual(is_fade_time([0, 50, 2, 114, 0, 143, 215, 0]), True)

    def test_is_fade_time2(self):
        self.assertFalse(is_fade_time([0, 50, 1, 114, 0, 143, 215, 0]))

    def test_is_fade_rate(self):
        self.assertEqual(is_fade_rate([0, 50, 1, 114, 0, 143, 215, 0]), True)

    def test_is_fade_rate(self):
        self.assertEqual(is_fade_rate([0, 50, 3, 114, 0, 143, 215, 0]), True)

    def test_hw_level(self):
        self.assertEqual(hardware_level([0, 50, 3, 114, 0, 143, 215, 0]), .84)

    def test_target_level_1(self):
        self.assertEqual(target_level([0, 50, 3, 114, 0, 143, 215, 0]), 0)

    def test_target_level_2(self):
        self.assertEqual(target_level([0, 50, 3, 114, 0, 143, 215, 60]), .24)

    def test_is_absolute_fade(self):
        self.assertEqual(is_absolute_fade([0, 50, 0, 114, 0, 143, 215, 0]), True)

    def test_is_relative_fade(self):
        self.assertEqual(is_relative_fade([0, 50, 3, 114, 0, 143, 215, 0]), True)

    def test_format_msg_1(self):
        self.assertEqual(format_channel_status_publish_msg([0, 50, 3, 114, 0, 143, 215, 0]),
            'Channel Status Publish: channel=16, target=0%, rate=52, increment=1, start=52, priority=7, relative fade')

    def test_format_msg_2(self):
        self.assertEqual(format_channel_status_publish_msg([0, 50, 0, 114, 16+127, 143, 215, 0]),
            'Channel Status Publish: channel=16, target=0%, time=52, increment=1, start=52, priority=7, absolute fade')

    def test_increment_value(self):
        self.assertEqual(increment_value([0, 50, 0, 114, 0, 143, 215, 0]), 1)

    def test_start_time(self):
        self.assertEqual(start_time([0, 50, 3, 114, 0, 143, 215, 60]), 52)


class GroupStatusPublishTestCase(unittest.TestCase):

    def test_priority(self):
        self.assertEqual(priority_level([0, 70, 0, 112, 0, 0, 0, 0]), 7)

    def test_fade_time_1(self):
        self.assertEqual(fade_time([0, 70, 0, 112, 0, 0, 0, 0]), 0)

    def test_fade_time_2(self):
        self.assertEqual(fade_time([0, 70, 0, 112, 120, 0, 0, 0]), 12)

    def test_increment_value_1(self):
        self.assertEqual(increment_value([0, 70, 0, 112, 120, 0, 0, 0]), 0.1)

    def test_increment_value_2(self):
        self.assertEqual(increment_value([0, 70, 0, 114, 120, 0, 0, 0]), 1)

    def test_start(self):
        self.assertEqual(start_time([0, 70, 0, 112, 0, 0, 0, 0]), 0)

    def test_is_absolute(self):
        self.assertTrue(is_absolute_fade([0, 70, 0, 112, 0, 0, 0, 0]))

    def test_is_relative(self):
        self.assertTrue(is_relative_fade([0, 70, 2, 112, 0, 0, 0, 0]))

    def test_is_fade_rate(self):
        self.assertTrue(is_fade_rate([0, 70, 1, 112, 0, 0, 0, 0]))

    def test_is_fade_time(self):
        self.assertTrue(is_fade_time([0, 70, 0, 112, 0, 0, 0, 0]))

    def test_group_number(self):
        self.assertEqual(group_number([0, 70, 0, 112, 0, 0, 0, 0]), 0)

    def test_format_msg(self):
        self.assertEqual(format_group_status_publish_msg([0, 70, 1, 114, 0, 0, 0, 0]),
            'Group Status Publish: group=0, rate=52, increment=1, start=52, priority=7, absolute fade')


if __name__ == '__main__':
    unittest.main()
