import unittest
from unittest import mock
from datetime import datetime
from context import leviton_test
from leviton_test.greenmax import event_from_row, extract_events, voltage_from_percentage


class EventFromRowTestCase(unittest.TestCase):

    row = ['Relay State: 1', 'Channel: 8']

    def test_channel(self):
        self.assertEqual(event_from_row(self.row)['channel'], 8)

    def test_state(self):
        self.assertEqual(event_from_row(self.row)['state'], 1)


class ExtractEventsTestCase(unittest.TestCase):

    @mock.patch('csv.reader')
    @mock.patch('greenmax.open')
    def test_events_are_correct(self, mock_open, mock_csv_reader):
        mock_open.__exit__.return_value = None
        mock_csv_reader.return_value = [
            ['Relay State: 1', 'Channel: 8'],
            ['Relay State: 0', 'Channel: 8'],
        ]

        expected = [
            {'channel': 8, 'state': 1},
            {'channel': 8, 'state': 0}
        ]
        self.assertEqual(extract_events('garbage_file_path'), expected)

    @mock.patch('csv.reader')
    @mock.patch('greenmax.open')
    def test_events_double_digit_channel(self, mock_open, mock_csv_reader):
        mock_csv_reader.return_value = [
            ['Relay State: 1', 'Channel: 18'],
            ['Relay State: 0', 'Channel: 18'],
        ]


        expected = [
            {'channel': 18, 'state': 1},
            {'channel': 18, 'state': 0}
        ]
        self.assertEqual(extract_events('garbage_file_path'), expected)


class VoltageFromPercentageTestCase(unittest.TestCase):

    deviation = 0.5

    def test_0_perc(self):
        result = voltage_from_percentage(0)
        lower_bound, upper_bound = self.deviate(14)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_10_perc(self):
        result = voltage_from_percentage(0.1)
        lower_bound, upper_bound = self.deviate(0.79)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_15_perc(self):
        result = voltage_from_percentage(0.15)
        lower_bound, upper_bound = self.deviate(0.94)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_25_perc(self):
        result = voltage_from_percentage(0.25)
        lower_bound, upper_bound = self.deviate(1.79)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_35_perc(self):
        result = voltage_from_percentage(0.35)
        lower_bound, upper_bound = self.deviate(3.28)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_45_perc(self):
        result = voltage_from_percentage(0.45)
        lower_bound, upper_bound = self.deviate(4.33)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_55_perc(self):
        result = voltage_from_percentage(0.55)
        lower_bound, upper_bound = self.deviate(5.36)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_65_perc(self):
        result = voltage_from_percentage(0.65)
        lower_bound, upper_bound = self.deviate(6.37)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_75_perc(self):
        result = voltage_from_percentage(0.75)
        lower_bound, upper_bound = self.deviate(7.39)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_85_perc(self):
        result = voltage_from_percentage(0.85)
        lower_bound, upper_bound = self.deviate(8.38)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_95_perc(self):
        result = voltage_from_percentage(0.95)
        lower_bound, upper_bound = self.deviate(9.37)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def test_45_perc(self):
        result = voltage_from_percentage(1.0)
        lower_bound, upper_bound = self.deviate(9.96)
        self.assertTrue(lower_bound <= result <= upper_bound, msg='{} not between {} or {}'.format(result, lower_bound, upper_bound))

    def deviate(self, val):
        lower = val - self.deviation
        upper = val + self.deviation
        return (lower, upper)


if __name__ == '__main__':
    unittest.main()
