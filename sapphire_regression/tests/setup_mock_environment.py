import os
import subprocess

mocks_path = os.path.join(
     'C:\\',
     'Users',
     'BEK',
     'sapphire_regression',
     'branches',
     'pre_code_review',
     'sapphire_regression',
     'greenmax_mocks'
)

config_vars = dict(
    IP = '192.168.2.181',
    BINARY_CSV_PATH = os.path.join(mocks_path, 'binary.csv'),
    BINARY_EXE_PATH = os.path.join(mocks_path, 'binary_mock.py'),
    WAIT_TIME = 3,
    VERBOSE = True,
    ANALOG_EXE_PATH = os.path.join(mocks_path, 'analog_mock.py'),
    ANALOG_CSV_PATH = os.path.join(mocks_path, 'analog.csv'),
    DS_EXE_PATH = os.path.join(mocks_path, 'ds_mock.py'),
)

NOT_ADMINISTRATOR_ERROR = 'ERROR: Access to the registry path is denied.'

for var,val in config_vars.items():
    command = ['setx', str(var), str(val), '/m']
    result = subprocess.run(command, stdout=subprocess.PIPE)
    if result == NOT_ADMINISTRATOR_ERROR:
        raise RuntimeError('Please re-run script with adminstrator privileges')
