# ds.py
"""
This module is for controlling the voltage being sent to the Sapphire Analog Inputs(AI)
This allows the framework to simulate a photocell or potentiometer - Scott M.
"""

import subprocess
from leviton_test.config import Configuration



config = Configuration()


def set_voltage(ch1_voltage, ch2_voltage):
    """Sets the Analog output voltage on the DAQ 6225. Chanel 0 is for the first Sapphire AI, channel 1 is for the second AI"""
    try:
        if '.py' in config.SET_DAQ_VOLTAGE:
            cmdline_args = ['python', config.SET_DAQ_VOLTAGE, str(ch1_voltage), str(ch2_voltage)]
        else:
            cmdline_args = [config.SET_DAQ_VOLTAGE, str(ch1_voltage), str(ch2_voltage)]
        print(("Setting DAQ Channel_1: {}V, Channel_2: {}V").format(ch1_voltage, ch2_voltage))
        subprocess.run(cmdline_args, timeout=config.SAPPHIRE_WAIT_TIME)
    except subprocess.TimeoutExpired:
        pass
