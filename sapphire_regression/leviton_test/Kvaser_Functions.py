# canparser.py
"""Parse LumaCAN3 messages.

This module defines utilities for parsing LumaCAN3 messages.  It uses the Kvaser CANlib to read in messages
and parse them.  Only a subset of LumaCAN3 is implemented.


.. _kvaser-msg-format:

Kvaser Message Format
--------------------------
Kvaser messages are received and sent as lists of integers, with a max length of 8.

For example:
    msg = [0, 13, 2, 5, 3, 4, 3, 2] # This is a valid Kvaser message

"""
from canlib import canlib
import unittest
from itertools import zip_longest
from datetime import datetime, timedelta
import pytz
import math
import time
import struct


def bits_from_msg(msg):
    """Returns a str of bits from msg.


    Parameters
    ----------
        msg: list(bytearray)
            An 8-frame Kvaser messsage.

    Returns
    -------
        list(str)
            The message in the form of a string of bits


    Examples
    --------

    To print messages in bit form.

        >>> ch = set_up_channel(0)
        >>> for msg in messages(ch):
        >>>     bits = bits_from_msg(msg)
        >>>     print(bits)
        ['10', '1000001', '10', '1110000', '110010', '0', '0', '0']
        ['11111111']
        .
        .
        .


    Convert bits to integers (as you'd see in Kvaser CANKing).

        >>> msg = ['10', '1000001', '10', '1110000', '110010', '0', '0', '0']
        >>> ints = [int(bits, base=2) for bits in msg]
        >>> print(ints)
        [2, 65, 2, 112, 50, 0, 0, 0]

    """
    #header = bytearray(msg[0].to_bytes(8, byteorder = 'big'))
    message = msg[1]
    return ['{0:b}'.format(num).zfill(8) for num in message]


def bits_from_header(msg):
    header = bytearray(msg[0].to_bytes(8, byteorder = 'big')) #header comes in as a giant int, this converts it into bytes so that it can be asily covperted to a byte array
    #print('header---' + str(header))
    #print(msg[0]) #returns the header as a raw integer
    return ['{0:b}'.format(num).zfill(8) for num in header] #breaks it up into octets for wasier parsing



'''
def str_from_msg(msg):
    """Returns a formatted string representation of `msg`.

    :param  msg:  The raw message from the Kvaser CANlib, use return value of :py:func:`~leviton_test.canparser.messages`
    :type msg: A list of bytes

    Examples
    --------


        >>> ch = set_up_channel(0)
        >>> for msg in messages(ch):
        >>>     print(str_from_msg(msg))
        Channel Fade: channel=1, target=0%, time=10.0, increment=0.1, start=0, priority=7
        Channel Fade: channel=2, target=0%, time=20.0, increment=0.1, start=0, priority=7
        .
        .
        .


    Returns
    -------
        str
            The message in readable form

    """
    formatted = format_msg(msg)
    if formatted == msg:
        return 'No parsing implemented for message: {}'.format(list(msg))
    else:
        return formatted
'''

# ++++++++++++++++++++++++
# Kvaser API and main loop
# ++++++++++++++++++++++++


def set_up_channel(channel=0,
                 openFlags=canlib.canOPEN_ACCEPT_VIRTUAL,
                 bitrate=canlib.canBITRATE_125K,
                 bitrateFlags=canlib.canDRIVER_NORMAL):
    """Returns a Kvaser CANLib channel.

    Parameters
    ----------
        channel: int
            The channel number to setup.

    Returns
    -------
        canlib.canlib.canChanenl
            The CAN channel to read and write messages from. Also, see: `canlib.canlib.canChannel <https://github.com/Kvaser/canlib-samples/blob/master/Samples/Python/canlib.py#L840/>`_
    Examples
    --------

    >>> from leviton_test.canparser import (
    ...     set_up_channel,
    ...     messages,
    ...     str_from_msg,
    ... )
    >>> channel = set_up_channel(0)
    Using channel: Kvaser Leaf Light HS (channel 0), EAN: 73-30130-00241-8
    >>> type(channel)
    <class 'canlib.canlib.canChannel'>
    >>> for msg in messages(channel):
    >>>     print(str_from_msg(msg))
    Channel Fade: channel=1, target=0%, time=10.0, increment=0.1, start=0, priority=7
    .
    .
    .

    """
    cl = canlib.canlib()
    ch = cl.openChannel(channel, openFlags)
    print("Using channel: %s, EAN: %s" % (ch.getChannelData_Name(),
                                          ch.getChannelData_EAN()))
    ch.setBusOutputControl(bitrateFlags)
    ch.setBusParams(bitrate)
    ch.busOn()
    return ch


def tear_down_channel(ch):
    """Tear down the Kvaser channel.

    Parameters
    ----------
        ch: `canlib.canlib.canChannel <https://github.com/Kvaser/canlib-samples/blob/master/Samples/Python/canlib.py#L840/>`_
            The Kvaser channel that you want to tear down, (was probably the return value of :py:func:`~leviton_test.canparser.set_up_channel`)

    Examples
    --------

    >>> from leviton_test.canparser import (
    ...     set_up_channel,
    ...     tear_down_channel,
    ... )
    >>> channel = set_up_channel(0)
    Using channel: Kvaser Leaf Light HS (channel 0), EAN: 73-30130-00241-8
    >>> tear_down_channel(channel)  # Channel closed

    """
    ch.busOff()
    ch.close()


def messages(ch, limit=math.inf, filter=lambda x: x, timeout=0, with_header = True):
    """Returns a generator of only the message part of kvaser output.

    Parameters
    ----------
        ch: canlib.canlib.canChannel
            The channel returned from :py:func:`~leviton_test.canparser.set_up_channel`.  Also, see: `canlib.canlib.canChannel <https://github.com/Kvaser/canlib-samples/blob/master/Samples/Python/canlib.py#L840/>`_
        limit: int
            The max number of messages before tearing down the channel
        filter: func
            A function that returns a boolean, messages will only return messages that return True
        timeout: int
            The time in seconds that messages will loop, leave at zero for infinite loop.

    The following example prints out raw CAN messages.


    Examples
    --------

    .. code-block:: python

        >>> ch = set_up_channel(0)
        >>> for msg in messages(ch):
        >>>     print(msg)
        bytearray(b'\\x00\\rQ\\x0f\\n\\x1d')
        bytearray(b'\\x00\\x0b\\x01\\x13&\\x00')
        .
        .
        .

    """
    for (msgId, msg, dlc, flg, time) in tail_kvaser_channel(ch, timeout=timeout):
        if limit <= 0:
            break
        elif filter(msg):
            limit -= 1
            if with_header:
                yield msgId, msg
            else:
                yield msg
        else:
            pass



def tail_kvaser_channel(ch, timeout=0):
    """Returns a generator of real-time kvaser output.

    This generator behaves like linux's `tail -f` command.

    Usage:

        for (msgId, msg, dlc, flg, time) in tail_kvaser_channel():
            print(msg)

    """
    start = time.time()
    while True:
        elapsed = time.time() - start
        if timeout > 0 and elapsed > timeout:
            raise StopIteration
        try:
            yield ch.read()
        except (canlib.canNoMsg) as ex:
            pass
        except (canlib.canError) as ex:
            pass


#if __name__ == "__main__":
    #print(next(messages(set_up_channel(), limit=math.inf, filter=lambda x: x, timeout=0, with_header = True)))
