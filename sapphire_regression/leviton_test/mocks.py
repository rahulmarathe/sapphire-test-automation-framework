from view import View
from adb import ADB
from xml.etree.ElementTree import Element, SubElement, tostring
import xml.etree.ElementTree as etree

class MockADB(ADB):

    def __init__(self, path_to_xml=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.path_to_xml = path_to_xml
        self.SAPPHIRE_IP = None # Not needed, we are mocking out any ADB activity
        self.command_return_values = kwargs.get('command_return_values', None)
        self.command_called_with = []

    def command(self, params, *args, **kwargs):
        self.command_called_with.append((params, args, kwargs))
        if self.command_return_values:
            return self.command_return_values.pop()
        else:
            return None

    def replace_file(self, old_file, new_file):
        pass

    def get_parsed_tree(self, buttons=[]):
        if self.path_to_xml:
            return etree.parse(self.path_to_xml)
        else:
            top = Element('top')
            for i, text in enumerate(buttons):
                bigger = i * 20
                top_left_x = bigger
                top_left_y = bigger
                btm_right_x = bigger + 10
                btm_right_y = bigger + 10
                self.create_xml_button(top, top_left_x, top_left_y, btm_right_x, btm_right_y, text, enabled=True)
            return top

    def create_xml_button(self, parent, top_left_x, top_left_y, btm_right_x, btm_right_y, text, enabled=True, content_desc=''):
        child = SubElement(parent, 'node')
        bounds_string = '[{},{}][{},{}]'.format(top_left_x, top_left_y, btm_right_x, btm_right_y)
        child.set('bounds', bounds_string)
        child.set('class', 'android.widget.Button')
        child.set('text', text)
        child.set('enabled', str(enabled).lower())
        child.set('content-desc', content_desc)


class MockView(View, MockADB):
    pass


if __name__ == '__main__':
    path = "C:\LabView Development\Phython Sheduler\\regression-create-mock-device\\views\\newton.xml"
    print(path)
    view = MockView(path_to_xml=path)
    import pdb;pdb.set_trace()
