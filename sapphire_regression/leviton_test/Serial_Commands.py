'''This Module contains all the serial command calls for Saphire testing. - Scott M.'''

import subprocess
import time
import serial
import os
from leviton_test.config import Configuration

config = Configuration()


def check_ADC_values():
    #This function is sensetive to the relative line breaks. It is kind of a pain extracting meaningful serial information from SAP using PySerial. If it looks cludgey, it's because it is
    port = config.SAPPHIRE_SERIAL_COM_PORT
    baud = '115200'
    #try:
    ser = serial.Serial(port, baud, timeout=2)
    # open the serial port
    try:
        if ser.isOpen():
            ser.write('\n'.encode('ascii'))
            print(ser.name + ' is open...')
            print('Reading ADC Values')
            time.sleep(1)
            ser.write('cat /sys/bus/i2c/devices/1-0029/device0/in0_raw'.encode('ascii') + '\r'.encode('ascii'))
            ADC_1 = ser.read(100)
            ADC_1_int = ADC_1.decode('UTF-8').split('\r\n')
            #print(ADC_1_int[2])
            ser.write('\n'.encode('ascii'))
            time.sleep(1)
            ser.write('cat /sys/bus/i2c/devices/1-0029/device0/in1_raw'.encode('ascii') + '\r'.encode('ascii'))
            ADC_2 = ser.read(100)
            ADC_2_int = ADC_2.decode('UTF-8').split('\r\n')
            time.sleep(1)

            return ADC_1_int[2], ADC_2_int[2]
    except:
        print('Warning: The serial port was occupied...')


def start_adbd():
    port = config.SAPPHIRE_SERIAL_COM_PORT
    baud = '115200'
    try:
        ser = serial.Serial(port, baud, timeout=1)
        # open the serial port
        if ser.isOpen():
            ser.write('\n'.encode('ascii'))
            print(ser.name + ' is open...')
            print('Starting Android Debug Bridge')
            ser.write('stop adbd'.encode('ascii') + '\r\n'.encode('ascii'))
            #out = ser.read(100)
            #print(out.decode('UTF-8'))
            time.sleep(1)
            ser.write('start adbd'.encode('ascii') + '\r\n'.encode('ascii'))
            #out = ser.read(100)
            #print(out.decode('UTF-8'))
            time.sleep(1)
            ser.close()
            time.sleep(2)
    except:
        print('Warning: The serial port was occupied so `start adbd` was not called...')


""" This method is for controlling the relay board used to multiplex the analog inputs on the sapphire"""
def toggle_numato(numato_relay_num, state, reset = None):
    port = config.NUMATO_MUX_RELAY_COM_PORT
    baud = '9600'
    if reset == True:
        serial_command = 'reset'
    else:
        serial_command = 'relay' + ' ' + str(state) + ' ' + str(numato_relay_num)
    #print(str(serial_command))
    try:
        ser = serial.Serial(port, baud, timeout=1)
        # open the serial port
        if ser.isOpen():
            print(ser.name + ' is open...')
            print('Setting Relay: {} to {}'.format(numato_relay_num, state))
            ser.write('\r'.encode('ascii'))
            ser.write(serial_command.encode('ascii'))
            ser.write('\r'.encode('ascii'))
            time.sleep(1)
            ser.close()
    except:
        print('Warning: The serial port was occupied...')
