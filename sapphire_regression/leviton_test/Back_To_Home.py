# ds.py
"""
This module is for controlling the voltage being sent to the Sapphire Analog Inputs(AI)
This allows the framework to simulate a photocell or potentiometer - Scott M.
"""

import subprocess
from leviton_test.config import Configuration
from leviton_test.config import Configuration
from leviton_test.adb import ADB
from leviton_test.view import View
from leviton_test.utils import DelayedKeyboardInterrupt, Timer
from leviton_test.parser import messages
from leviton_test.parser import generics



config = Configuration()


def homescreen(back_to_settings = False):
    #gets the view back to the homescreen
    view = View()
    if back_to_settings == False:
        if not view.settings_button:
            while not view.has_text('Exit'):
                view.press('Done')
            view.press('Exit')
    if back_to_settings == True:
        if not view.settings_button:
            while not view.has_text('Exit'):
                view.press('Done')
            view.press('Exit')
        view.press_settings()
        if 'Sign out' not in view.text_elements:
            view.press('Sign in')
            view.edit_texts[1].press()
            view.keyevent('5625')
            view.press('OK')
