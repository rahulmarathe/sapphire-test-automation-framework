import sqlite3
import os
from collections import namedtuple
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta
from calendar import day_name
import leviton_test.commands as cmd

DB_PATH = os.path.join(os.getcwd(), 'DB')

Date = namedtuple('Date', ['month', 'day', 'priority'])
Event = namedtuple('Event', ['name', 'priority', 'time', 'days', 'command'])
Occurrence = namedtuple('Occurrence', ['command', 'time', 'priority'])

days = [
    'Wednesday',
    'Sunday',
    'Monday',
    'Thursday',
    'Saturday',
    'Tuesday',
    'Friday',
]


days = [d.upper() for d in days]

def day_from_bytes(byte):
    global days
    daylist = []
    for d in days:
        if d.encode('utf8') in byte:
            daylist.append(d)
    return daylist


def lookup(vector, path):
    """

    Path should be something like:
        os.path.join("C:\\", "LabView Development", "Phython Sheduler", "builder", "sapphireUX_RegressionV2", "drivers",  "Sapphire.xml")
    """
    path = os.path.join(path)
    tree = ET.parse(path)
    root = tree.getroot()
    nary = 'primary' if 'primary' in vector else 'secondary'
    guid = ''
    for s in vector.split('.'):
        if '-' in s:
            guid = s
    xpath = ".//vector[@name='{guid}']//scene[@name='{nary}']//channelSetting".format(guid=guid, nary=nary)
    elem = root.find(xpath)
    channel = elem.get('channel').strip('C')
    toggle = 'on' if elem.get('level') == '100' else 'off'
    return channel + '_' + toggle


def first(weekday, hour, minute, pre):
    start = datetime(pre.year, 1, 1, hour, minute)
    day = timedelta(days=1)
    days = [start]
    current = start
    for i in range(7):
        current += day
        days.append(current)
    result = dict()
    for d in days:
        lowered = day_name[d.weekday()].lower()
        result[lowered] = d
    return result.get(weekday.lower())


def generate_all_days(event, pre, post):
    hour = int(event.time.split(':')[0])
    minute = int(event.time.split(':')[1])
    return generate_all_days_from_weekdays_hour_minute(event.days, hour, minute, pre, post)


def generate_all_days_from_weekdays_hour_minute(weekdays, hour, minute, pre, post):
    if type(weekdays) is list:
        result = []
        for weekday in weekdays:
            result += generate_all_for_weekday(weekday, hour, minute, pre, post)
    else:
        current = first(weekdays, hour=hour, minute=minute, pre=pre)
        week = timedelta(days=7)
        result = [current]
        for _ in range(52):
            current += week
            result.append(current)
    return result


def generate_all_for_weekday(weekday, hour, minute, pre, post):
    current = first(weekday, hour=hour, minute=minute, pre=pre)
    week = timedelta(days=7)
    result = [current]
    while current < post:
        current += week
        result.append(current)
    return result


def get_occurrences(pre=datetime(2016, 2, 1), post=datetime(2016, 2, 8)):
    events = pair_events_and_dates(pre, post)
    pairs = []
    for event in events:
        for date in event['dates']:
            pair = (event['event'], date)
            pairs.append(pair)
    # print(list(p for p in pairs if p[0].name == 'holiday1'))
    specials = get_special_events(pre, post)
    all_events = [Occurrence(event.command, time, event.priority) for event, time in pairs] + specials
    events = list(set(event for event in all_events if pre <= event.time <= post))
    return sorted(events, key=lambda x: x[1])


def pair_events_and_dates(pre, post):
    events = get_events()
    dates = get_dates()
    event_times = []
    holiday = [e for e in events if e.priority != 3]
    for e in holiday:
        event_times.append({'event': e, 'dates': find_dates(e, dates, pre, post)})

    weekly = [e for e in events if e.priority == 3]
    for e in weekly:
        event_times.append({'event': e, 'dates': generate_all_days(e, pre, post)})

    # Remove duplicates and order times
    for idx, _ in enumerate(event_times):
        event_times[idx]['dates'] = sorted(list(set(event_times[idx]['dates'])))
    return event_times


def get_events():
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    events = []
    commands = cmd.vector_to_commands_dictionary()

    for row in c.execute('SELECT event_name, event_priority, event_time, event_repeat_days, event_command FROM event_tbl'):
        name, priority, time, db_days, command_vector = row
        repeat_days = []
        for d in days:
            if d.encode('utf8') in db_days:
                repeat_days.append(d)
        for command in commands[command_vector]:
            e = Event(
                name=name,
                priority=priority,
                time=time,
                days=repeat_days,
                command=cmd.format_command_attr(command.channel, command.level)
            )
            events.append(e)
    conn.close()
    return events


def get_dates():
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    dates = []
    for row in c.execute('SELECT event_month, event_day, event_priority FROM date_tbl'):
        dates.append(Date(row[0] + 1, row[1], row[2]))
    conn.close()
    return dates


def find_dates(event, dates, pre, post):
    filtered = [d for d in dates if d.priority == event.priority]
    datetimes = []
    for date in filtered:
        hour = int(event.time.split(':')[0])
        minute = int(event.time.split(':')[1])
        for year in range(pre.year, post.year + 1):
            datetimes.append(datetime(year, date.month, date.day, hour, minute))
    return datetimes


def get_special_events(pre, post):
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    occurrences = []
    commands = cmd.vector_to_commands_dictionary()
    for row in c.execute(
            'SELECT event_name, event_month, event_day, event_time,' +
            ' event_weekDay, is_weekly, event_offset, event_command, event_repeat_days FROM special_tbl'):
        name, month, event_day, time, day_of_week, is_weekly, offset, command_vector, repeat_days = row
        # Skip any with an offset
        if offset:
            continue
        month += 1
        event = None
        # event_days = []
        command_obj_list = commands[command_vector]
        command_string_list = [
            cmd.format_command_attr(command.channel, command.level)
            for command in command_obj_list
        ]
        priority = -1
        # Simple Case
        # A special event on a particular date
        if day_of_week == 0 and not is_weekly:
            hourminute = datetime.strptime(time, '%H:%M')
            dates = []
            for year in range(pre.year, post.year + 1):
                dates.append(datetime(year, month, event_day, hourminute.hour, hourminute.minute))


        # Weekly special events
        elif is_weekly:
            hour = datetime.strptime(time, '%H:%M').hour
            minute = datetime.strptime(time, '%H:%M').minute
            weekdays = []
            for d in days:
                if d.encode('utf8') in repeat_days:
                    weekdays.append(d)
            dates = generate_all_days_from_weekdays_hour_minute(weekdays, hour, minute, pre, post)

        for date in dates:
            for command in command_string_list:
                occurrence = Occurrence(command, date, priority)
                occurrences.append(occurrence)


    conn.close()
    return occurrences


if __name__ == '__main__':
    events = get_occurrences()
    for e in events:
        print(e)
