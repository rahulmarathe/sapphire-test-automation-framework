import signal
import time
import subprocess
from collections import namedtuple
from leviton_test.Serial_Commands import toggle_numato

# Used in coordinates calculations
Point = namedtuple('Point', ['x', 'y'])







"""This acts as a wrapper for multiplexing the sapphire analog input devices using the numato relay module"""
def sapphire_AI_select(analog_input = None, binary_or_analog_device = None):
    if analog_input == 1:
        toggle_numato(numato_relay_num = 1, state = 'off')
        time.sleep(2)
    elif analog_input == 2:
        toggle_numato(numato_relay_num = 1, state = 'on')
        time.sleep(2)
    if binary_or_analog_device == 'binary':
        toggle_numato(numato_relay_num = 0, state = 'on')
        time.sleep(2)
    elif binary_or_analog_device == 'analog':
        toggle_numato(numato_relay_num = 0, state = 'off')
        time.sleep(2)



def subprocess_wrapper(*popenargs, input=None, timeout=None, check=False, **kwargs):
    cmdline_args = list(popenargs)
    print(cmdline_args)
    for popenarg in popenargs:
        for arg in popenarg:
            if arg.endswith('.py'):
                cmdline_args = ['python'] + list(*popenargs)
                break

    subprocess.run(cmdline_args, input=input, timeout=timeout, check=check, **kwargs)


class DelayedKeyboardInterrupt(object):
    def __enter__(self):
        self.signal_received = False
        self.old_handler = signal.getsignal(signal.SIGINT)
        signal.signal(signal.SIGINT, self.handler)

    def handler(self, sig, frame):
        self.signal_received = (sig, frame)
        inp = input('Paused...\nType "exit" to exit tests, or just press enter to continue:\n')
        if inp.lower().strip() == 'exit':
            raise RuntimeError('exited...')

    def __exit__(self, type, value, traceback):
        signal.signal(signal.SIGINT, self.old_handler)
        if self.signal_received:
            self.old_handler(*self.signal_received)


class Timer(object):
    def __init__(self):
        pass

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.elapsed = self.end - self.start
        self.elapsed_minutes = round(self.elapsed / 60, 2)


def coordinates_from_bounds_string(string):
    """Returns a tuple (x, y) from a string '[x, y][x1, y2]'. """
    # '[x, y]' becomes 'x, y'
    cutoff_ends = string[1:-1]
    coords = cutoff_ends.replace('[', '').replace(']', ',').split(',')
    stripped = [elem.strip() for elem in coords]
    integers = tuple(int(elem) for elem in stripped)
    return (integers[0], integers[1]), (integers[2], integers[3])


def center(coords1, coords2):
    """Returns the average center between to xy-coordinates."""
    x1, x2 = coords1[0], coords2[0]
    y1, y2 = coords1[1], coords2[1]
    width = x2 - x1
    height = y2 - y1
    slope = height / width
    centerx = int(x1 + (width/2))
    centery = int(y1 + (slope * (width/2)))
    return Point(x=centerx, y=centery)


def test_XML_processing():
    assert coordinates_from_bounds_string('[123,222][777,777]') == ((123, 222), (777, 777))
    point = center((2,4), (8, 10))
    assert point.x == 5
    assert point.y == 7
