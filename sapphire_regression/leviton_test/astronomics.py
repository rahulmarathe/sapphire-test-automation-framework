# astronomics.py
from astral import Astral, Location
from datetime import datetime

def get_location(lat, lng, timezone):
    l = Location()
    l.longitude = float(lng)
    l.latitude = float(lat)
    l.timezone = timezone
    return l


def sunrise(lat, lng, year=None, month=None, day=None, timezone='US/Pacific'):
    date = default_datetime(year, month, day)
    l = get_location(lat, lng, timezone)
    sunrise = l.sun(date=date)['sunrise']
    return sunrise


def sunset(lat, lng, year=None, month=None, day=None, timezone='US/Pacific'):
    date = default_datetime(year, month, day)
    l = get_location(lat, lng, timezone)
    sunset = l.sun(date=date)['sunset']
    return sunset


def default_datetime(year=None, month=None, day=None):
    now = datetime.now()
    if not year:
        year = now.year
    if not month:
        month = now.month
    if not day:
        day = now.day
    return datetime(year, month, day)
