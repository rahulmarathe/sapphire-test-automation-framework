import time
import subprocess
from leviton_test.Serial_Commands import toggle_numato


"""This acts as a wrapper for multiplexing the sapphire analog input devices using the numato relay module"""
def dev_mux_1(AI_1 = None):
    if AI_1 == 'Reset':
        toggle_numato(numato_relay_num = '', state = '', reset = True)
    if AI_1 == 'Binary':
        toggle_numato(numato_relay_num = 0, state = 'off')
        toggle_numato(numato_relay_num = 7, state = 'off')
    if AI_1 == 'Analog':
        toggle_numato(numato_relay_num = 0, state = 'on')
        toggle_numato(numato_relay_num = 1, state = 'on')
    if AI_1 == None:
        toggle_numato(numato_relay_num = 0, state = 'off')
        toggle_numato(numato_relay_num = 7, state = 'off')

def dev_mux_2(AI_2 = None):
    if AI_2 == 'Reset':
        toggle_numato(numato_relay_num = '', state = '', reset = True)
    if AI_2 == 'Binary':
        toggle_numato(numato_relay_num = 2, state = 'off')
        toggle_numato(numato_relay_num = 5, state = 'off')
    if AI_2 == 'Analog':
        toggle_numato(numato_relay_num = 2, state = 'on')
        toggle_numato(numato_relay_num = 3, state = 'on')
    if AI_2 == None:
        toggle_numato(numato_relay_num = 2, state = 'off')
        toggle_numato(numato_relay_num = 5, state = 'off')

def both_switches(state = None):
    if state == 'on':
        toggle_numato(numato_relay_num = 'A0', state = 'writeall')
    if state == 'off':
        toggle_numato(numato_relay_num = '00', state = 'writeall')

def switch_state(SW1 = None, SW2 = None):
    #architecture of this method allows for more switch states as they get implimented on Sapphire
    if SW1 == 'on':
        toggle_numato(numato_relay_num = 0, state = 'off')
        toggle_numato(numato_relay_num = 7, state = 'on')
    if SW1 == 'off':
        toggle_numato(numato_relay_num = 0, state = 'off')
        toggle_numato(numato_relay_num = 7, state = 'off')
    if SW2 == 'on':
        toggle_numato(numato_relay_num = 2, state = 'off')
        toggle_numato(numato_relay_num = 5, state = 'on')
    if SW2 == 'off':
        toggle_numato(numato_relay_num = 2, state = 'off')
        toggle_numato(numato_relay_num = 5, state = 'off')
