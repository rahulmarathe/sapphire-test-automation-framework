
import subprocess
import shlex
import time
import re
import os
import csv
from datetime import datetime
from leviton_test.config import Configuration


config = Configuration()


def event_from_row(row):
    """Returns an event from a csv row."""
    channel = int(row[1].split()[-1])
    state_string = row[0].split()[-1]
    state = float(state_string) if '.' in state_string else int(state_string)
    return_dict = {'channel': channel, 'state': state}
    return return_dict


def extract_events(myfile):
    """Returns a list of events from a csv file."""
    with open(myfile, 'r', newline='') as f:
        reader = csv.reader(f)
        events = [event_from_row(row) for row in reader if len(row) == 2]
    os.remove(myfile)
    return events


def add_event_to_state(event, state):
    num =  int(event['channel'])
    state[num] = event['state']


def final_state_from_events(events):
    """Returns the final state from a series of events."""
    state = dict()
    for e in events:
        add_event_to_state(e, state)
    return state


def state_from_csv(csvfile):
    """Returns a dict representing the current state."""
    return final_state_from_events(extract_events(csvfile))


def call_exe_or_py(filepath, wait_time=None):
    args = []
    if '.py' in filepath:
        args = ['python', filepath]
    else:
        args = [filepath]
    if not wait_time:
        if 'binary' in filepath.lower():
            wait_time = config.SAPPHIRE_WAIT_TIME
        elif 'analog' in filepath.lower():
            wait_time = config.SAPPHIRE_WAIT_TIME
        else:
            wait_time = 5
    try:
        #print(args)
        subprocess.run(args, timeout=wait_time)
    except subprocess.TimeoutExpired:
        pass


def voltage_from_percentage(percentage):
    """Returns the voltage calculated from the percentage you'll see on a Sapphire
    slider.  The voltage reader returns 0 as voltage 14, so if :arg:`percentage`
    is 0 than return 14."""
    if percentage == 0:
        return 14
    return   ((31.609 * pow(percentage, 4)) -
                (72.219 * pow(percentage, 3)) +
                (54.939 * pow(percentage, 2)) -
                (4.9149 * percentage )) + 0.7227


def binary_state(wait_time=0):
    """Returns a dict representing the current binary state."""
    call_exe_or_py(config.GM_BINARY_EXE_PATH, wait_time=wait_time)
    return final_state_from_events(extract_events(config.GM_BINARY_CSV_PATH))


def analog_state(wait_time=6):
    """Returns a dict representing the current analog state."""
    call_exe_or_py(config.GM_ANALOG_EXE_PATH, wait_time=wait_time)
    return final_state_from_events(extract_events(config.GM_ANALOG_CSV_PATH))
