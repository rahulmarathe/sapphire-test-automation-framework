=====
Setup
=====

This covers how to install and uninstall the Leviton Test Framework and its
dependencies.


Requirements
------------

1. Python 3.6
2. `Kvaser CANlib SDK <https://www.kvaser.com/developer/canlib-sdk/>`_
3. ` Kvaser CANKing and associated Kvaser firmware <https://www.kvaser.com/downloads-kvaser/>`_

Installing
-------------------------------------

0. Install the above requirements
1. Unzip leviton_test_framework.zip
2. Call the following command in your shell

.. warning:: This setup.py is from the output of the leviton_test_framework.zip file, NOT from the project root.
             This is had to be done because the canlib dependency is not on PyPI.

::

    $ python setup.py --install


Uninstalling
------------------------------------------------------------

1. Type the following command in shell

::

    $ python setup.py --uninstall-all


Uninstalling (But Keep Dependencies)
----------------------------------------------------------------

1. Type the following command in shell

::

    $ python setup.py --uninstall



Setting Up the Test Environment
===============================

First, we need to set our Kvaser and Tera Term settings.


Kvaser Bus Parameters
---------------------

To use CANking to verify CAN bus traffic, set the bus parameters to the following:


  ============== ===============
  Kvaser Bus Parameters
  ------------------------------
  Bus Speed        125000 bit/s
  -------------- ---------------
  Sampling Point   87.5%
  -------------- ---------------
  SJW              1
  ============== ===============


Tera Term Settings
-------------------

This section covers settings for if you want to use the Android scripting library.
To startup adbd, set your teraterm settings to the following:

  ========= ===========
  Tera Term Parameters
  ---------------------
  Baud rate  115200
  --------- -----------
  Data       8-bit
  --------- -----------
  Parity     None
  ========= ===========

To use adb to connect with your Sapphire device, after having configured minicom/teraterm, open minicom or teraterm and type::

    $ start adbd
    $ ifconfig eth0
    eth0: ip 192.168.2.220 mask 255.255.255.0 flags [up broadcast running multicast]


Helpful ADB Commands
====================


Enable Pointer Location for X Y Coordinates On Screen
-----------------------------------------------------

::

	$ adb shell settings put system pointer_location 1
