==============
LumaCAN Parser
==============

The currently supported message formats are:

    * Channel Fade
    * Channel Status Publish
    * Group Status Publish
    * Group Fade

Any other message will just be returned in raw format.


Setting Up and Tearing Down Your Kvaser CAN Channels
----------------------------------------------------

Channels to the CAN bus need to be set up before reading and writing messages to the bus.
The functions for doing this are :py:func:`~leviton_test.canparser.set_up_channel` and :py:func:`~leviton_test.canparser.tear_down_channel`.

.. autofunction:: leviton_test.canparser.set_up_channel

.. autofunction:: leviton_test.canparser.tear_down_channel

.. note:: See :py:func:`str_from_msg` for how to read these messages in a human-readable form.


Reading Messages
----------------

.. autofunction:: leviton_test.canparser.messages

.. autofunction:: leviton_test.canparser.str_from_msg

.. autofunction:: leviton_test.canparser.bits_from_msg


Writing Messages
----------------

.. autofunction:: leviton_test.canparser.time_msg_from_datetime

.. autofunction:: leviton_test.canparser.date_msg_from_datetime

.. autofunction:: leviton_test.canparser.broadcast_msg


Getting Information from Messages
----------------------------------

.. autofunction:: leviton_test.canparser.command_type



.. _kvaser-msg-format:

Kvaser Message Format
--------------------------
Kvaser messages are received and sent as lists of integers or as bytearrays, with a max length of 8.

The parsing functions accept messages in the form of bytearrays (as output from :py:func:`~leviton_test.canparser.messages`).
The functions to send messages on the bus take msgs as list of ints.  This was chosen because bytearrays
are not as easy to construct and they are not the way we see messages in CANKing, even though they are the
return value of CANLib.


For example:

.. code-block:: python

    >>> msg = [0, 13, 2, 5, 3, 4, 3, 2] # This is a valid Kvaser message
    >>> msg  = bytearray(b'\x00F\x00p\x00\x00\x00\x00') # Also a valid Kvaser message
    >>> msg
    bytearray(b'\x00F\x00p\x00\x00\x00\x00')
    >>> len(msg)
    8

To convert from an byte array into a list of ints:


.. code-block:: python

  >>> msg  = bytearray(b'\x00F\x00p\x00\x00\x00\x00')
  >>> list(msg)
  [0, 70, 0, 112, 0, 0, 0, 0]
  >>>
