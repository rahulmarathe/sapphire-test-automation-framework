Tutorials
=========

1. Configuration
----------------

  Some framework features require configuration parameters.  Here, we'll use a
  configuration that will enable all of the framework's features to be used.
  This will include the LumaCAN 3 parser, the GreenMAX interface, and the Android
  interface.

  The framework gathers configuration parameters from either a ``config.py`` in the
  current working directory, or from system environment variables.

  .. important:: You must either have a ``config.py`` in your current working directory
                 or your configuration parameters set in your system's environment
                 variables.

  Here is an example ``config.py``:

  .. code-block:: python

      # config.py
      """This file defines config-related globals, such as LabView exe paths and devices IP's."""

      import os

      # Use os.path for easy cross-platform path building
      path = os.path.join('C:\\', 'example_folder')

      # Define environment variables here
      GM_BINARY_EXE_PATH = os.path.join(path, 'binary.exe') #  C:\\example_folder\binary.exe
      GM_ANALOG_EXE_PATH = os.path.join(path, 'analog.exe') #  C:\\example_folder\analog.exe
      GM_BINARY_CSV_PATH = os.path.join(path, 'binary.csv') #  C:\\example_folder\binary.csv
      GM_ANALOG_CSV_PATH = os.path.join(path, 'analog.csv') #  C:\\example_folder\analog.csv
      GM_DS_EXE_PATH = os.path.join(path, 'ds.exe') #  C:\\example_folder\ds.exe
      SAPPHIRE_IP = '192.168.2.181'
      SAPPHIRE_WAIT_TIME = 3
      SAPPHIRE_VERBOSE = True


Configuration Parameters Descriptions
-------------------------------------


  =========================== ====================================== =====================================
  Configuration Parameters Table
  --------------------------------------------------------------------------------------------------------
  Environment Variable        Description                             Example Value
  =========================== ====================================== =====================================
  SAPPHIRE_IP                   The IP address of your Sapphire       '192.168.2.181'
                                device
  --------------------------- -------------------------------------- -------------------------------------
  GM_BINARY_EXE_PATH           Path to the LabView Binary Reader exe  'C:\\mygreenmax\\binary.exe'
  --------------------------- -------------------------------------- -------------------------------------
  GM_BINARY_CSV_PATH           Path to the LabView Binary Reader exe  'C:\\mygreenmax\\output\\binary.csv'
                               output CSV file location.
  --------------------------- -------------------------------------- -------------------------------------
  SAPPHIRE_WAIT_TIME           The amount of time to read              3
                               binary/analog values before closing
                               the LabView exe's (in seconds)
  --------------------------- -------------------------------------- -------------------------------------
  SAPPHIRE_VERBOSE             Sapphire test output verbosity          True
  --------------------------- -------------------------------------- -------------------------------------
  GM_ANALOG_EXE_PATH          Path to the LabView analog reader .exe  'C:\\mygreenmax\\analog.exe'
  --------------------------- -------------------------------------- -------------------------------------
  GM_ANALOG_CSV_PATH          Path to the LabView analog reader .exe
                              CSV output file location.                'C:\\mygreenmax\\analog.csv'
  --------------------------- -------------------------------------- -------------------------------------
  GM_DS_EXE_PATH               Path to the LabView greenmax DS
                               switch .exe file                       'C:\\mygreenmax\\ds.exe'
  =========================== ====================================== =====================================
