import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View
from leviton_test import Serial_Commands
from leviton_test.Voltage_Control import set_voltage
from leviton_test.utils import sapphire_AI_select



class SecretSauceTestCase(sapphire.SapphireTestCase):

    def run(self):
        self.print_intro()
        view = self.get_view()
        if not view.settings_button:
            view.press('Exit')
        view.press_settings()
        if 'Sign out' not in view.text_elements:
            view.press('Sign in')
            view.edit_texts[1].press()
            view.keyevent('5625')
            view.press('OK')


        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        view.press('Settings for analog input 1')

        view.press('Analog Input Id')
        view.edit_texts[0].press()
        view.keyevent('65537') #out of range value
        view.press('Save')
        self.assertTrue(view.has_text('Not set'), 'Out of range Analog ID not retained')
        view.press('Analog Input Id')
        #view.edit_texts[0].press()
        AI_1_dev_num = 66
        view.keyevent(AI_1_dev_num)
        view.press('Save')
        self.assertTrue(view.has_text('66'), 'Device ID Assigned')

        view.press('Device type')
        view.press('Switch')
        self.assertTrue(view.has_text('Switch'),'Check for Switch Device type')

        view.press('Device type')
        view.press('Occupancy Sensor')
        self.assertTrue(view.has_text('Occupancy Sensor'),'Check for Occupancy Sensor Device type')

        view.press('Device type')
        view.press('Photocell')
        self.assertTrue(view.has_text('Photocell'),'Check for Photocell Device type')

        view.press('Device type')
        view.press('Contact Control')
        self.assertTrue(view.has_text('Contact Control'),'Check for Contact Control Device type')

        view.press('Device type')
        view.press('Potentiometer')
        self.assertTrue(view.has_text('Potentiometer'),'Check for Potentiometer Device type')

        view.press('Device type')
        view.press('Cancel')
        self.assertTrue(view.has_text('Potentiometer'),'Cancel selection changes nothing')

        view.press('Sample interval')
        view.edit_texts[0].press()
        view.keyevent('150') #standard sample interval
        view.press('Save')
        self.assertTrue(view.has_text('150'),'Set Sampling Interval')
        if view.has_text('Enabled'):
            view.press('Enable device')
            self.assertTrue(view.has_text('Disabled'),'Disable Input Device')
            view.press('Enable device')
            self.assertTrue(view.has_text('Enabled'),'Enable Input Device')
        else:
            view.has_text('Disabled')
            view.press('Enable device')
            self.assertTrue(view.has_text('Enabled'),'Enable Input Device')

        view.press('Device type')
        view.press('Switch')


        """This section sets the max voltage of a device and enteres it into the Sapphire settings. This calibrates the ADC scaler to the device maximum
        this same structure is repeated for each device as needed
        """
        #connects a binary device (switch, contact, etc.) to analog input 1
        """Calibrate switch"""
        sapphire_AI_select(analog_input = 1, binary_or_analog_device = 'binary')
        set_voltage(channel = 'Switch', voltage = '24')
        adc_values = Serial_Commands.check_ADC_values()
        print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
        time.sleep(1)
        ADC_max_24 = adc_values[0]
        view.press('Hardware Maximum')
        view.edit_texts[0].press()
        view.keyevent(ADC_max_24)
        view.press('Save')
        #print(str(ADC_max_24))
        self.assertTrue(view.has_text(str(ADC_max_24)), 'HW Maximum Assigned')
        set_voltage(channel = 'Switch', voltage = '0')

        """Check switch CAN behavior"""
        view.press('Done')
        view.press('Done')
        view.press('Exit')
        with self.messages_buffering():
            set_voltage(channel = 'Switch', voltage = '24')
        self.can_check(self.can_check(button = 'Checking Switch On', message_type = 81, input_device_type = 1, input_device_number = 66, input_device_status = 0, input_device_state = 100))
        with self.messages_buffering():
            set_voltage(channel = 'Switch', voltage = '0')
        self.can_check(self.can_check(button = 'Checking Switch Off', message_type = 81, input_device_type = 1, input_device_number = 66, input_device_status = 0, input_device_state = 0))
        #CAN Checking
        #device input query

        """skipping broadcast message check. If this feature doesn't work, it will become really obvious when testing doesn't see any CAN traffic"""
        view = self.get_view()
        if not view.settings_button:
            view.press('Exit')
        view.press_settings()
        if 'Sign out' not in view.text_elements:
            view.press('Sign in')
            view.edit_texts[1].press()
            view.keyevent('5625')
            view.press('OK')


        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        view.press('Settings for analog input 1')


        """Calibrate Photocell"""
        view.press('Device type')
        view.press('Photocell')
        #connects an analog device (switch, contact, etc.) to analog input 1
        sapphire_AI_select(analog_input = 1, binary_or_analog_device = 'analog')
        set_voltage(channel = '0', voltage = '10')
        adc_values = Serial_Commands.check_ADC_values()
        print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
        time.sleep(1)
        ADC_max_24 = adc_values[0]
        view.press('Hardware Maximum')
        view.edit_texts[0].press()
        view.keyevent(ADC_max_10)
        print(str(ADC_max_10))
        #CAN checks

        #be careful setting conditions for input device states, always be aware of what type of device before setting P/F for state bits
        with self.messages_buffering():
            set_voltage(channel = '0', voltage = '1')
        self.can_check(self.can_check(button = 'Checking Photocell at 1V', message_type = 81, input_device_type = 3, input_device_number = 66, input_device_status = 0, input_device_state = 10))

        with self.messages_buffering():
            set_voltage(channel = '0', voltage = '3')
        self.can_check(self.can_check(button = 'Checking Photocell at 3V', message_type = 81, input_device_type = 3, input_device_number = 66, input_device_status = 0, input_device_state = 30))

        with self.messages_buffering():
            set_voltage(channel = '0', voltage = '5')
        self.can_check(self.can_check(button = 'Checking Photocell at 5V', message_type = 81, input_device_type = 3, input_device_number = 66, input_device_status = 0, input_device_state = 50))

        with self.messages_buffering():
            set_voltage(channel = '0', voltage = '8')
        self.can_check(self.can_check(button = 'Checking Photocell at 8V', message_type = 81, input_device_type = 3, input_device_number = 66, input_device_status = 0, input_device_state = 80))

        with self.messages_buffering():
            set_voltage(channel = '0', voltage = '10')
        self.can_check(self.can_check(button = 'Checking Photocell at 10V', message_type = 81, input_device_type = 3, input_device_number = 66, input_device_status = 0, input_device_state = 100))





    def wait(self, seconds, interval=5):
        print('Waiting to check backlight timing...')
        for i in range(0, seconds, interval):
            print('Waiting {} more seconds'.format(seconds-i))
            time.sleep(interval)
