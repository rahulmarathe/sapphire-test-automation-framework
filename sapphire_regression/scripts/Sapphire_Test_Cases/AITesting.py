import threading
from threading import Thread
import multiprocessing
from multiprocessing import Process
import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View
from leviton_test import Serial_Commands
from leviton_test.Voltage_Control import set_voltage
from leviton_test.Input_Device_MUX import dev_mux_1, dev_mux_2, switch_state, both_switches



class AITestCase(sapphire.SapphireTestCase):

    def runInParallel(*fns): #got this nice parallel process function wrapper from stackoverflow
      proc = []
      for fn in fns:
        p = Process(target=fn)
        p.start()
        proc.append(p)
      for p in proc:
        p.join()

    def default_device_states(self):
        print('Setting Default Hardware Values')
        #sets multiplexing relays to 0
        dev_mux_1(AI_1 = 'Reset')
        #setting all DAQ voltages to 0
        set_voltage(ch1_voltage = '0', ch2_voltage = '0')

    def homescreen(self, back_to_settings = False):
        #gets the view back to the homescreen
        view = self.get_view()
        if back_to_settings == False:
            if not view.settings_button:
                while not view.has_text('Exit'):
                    view.press('Done')
                view.press('Exit')
        if back_to_settings == True:
            if not view.settings_button:
                while not view.has_text('Exit'):
                    view.press('Done')
                view.press('Exit')
            view.press_settings()
            if 'Sign out' not in view.text_elements:
                view.press('Sign in')
                view.edit_texts[1].press()
                view.keyevent('5625')
                view.press('OK')

    def go_to_analog_1_settings(self, device_type):
        self.homescreen(back_to_settings = True)
        view = self.get_view()
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        view.press('Settings for analog input 1')
        view.press('Device type')
        view.press(device_type)

    def go_to_analog_2_settings(self, device_type):
        self.homescreen(back_to_settings = True)
        view = self.get_view()
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        view.press('Settings for analog input 2')
        view.press('Device type')
        view.press(device_type)

    def view_test_input_1(self, devnum_1):
        view = self.get_view()
        self.homescreen(back_to_settings = True)
        self.default_device_states()
        devnum = devnum_1
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        view.press('Settings for analog input 1')
        view.press('Analog Input Id')
        #view.edit_texts[0].press() #THIS USED TO KILL THE SAPPHIRE
        view.keyevent('65537') #out of range value
        view.press('Save')
        self.assertTrue(view.has_text('Not set'), 'Out of range Analog ID not retained')
        view.press('Analog Input Id')
        view.edit_texts[0].press()
        view.keyevent(devnum)
        view.press('Save')
        self.assertTrue(view.has_text(str(devnum)), 'Device ID Assigned')

        view.press('Device type')
        view.press('Switch')
        self.assertTrue(view.has_text('Switch'),'Check for Switch Device type')

        view.press('Device type')
        view.press('Occupancy Sensor')
        self.assertTrue(view.has_text('Occupancy Sensor'),'Check for Occupancy Sensor Device type')

        view.press('Device type')
        view.press('Photocell')
        self.assertTrue(view.has_text('Photocell'),'Check for Photocell Device type')

        view.press('Device type')
        view.press('Contact Control')
        self.assertTrue(view.has_text('Contact Control'),'Check for Contact Control Device type')

        view.press('Device type')
        view.press('Potentiometer')
        self.assertTrue(view.has_text('Potentiometer'),'Check for Potentiometer Device type')

        view.press('Device type')
        view.press('Cancel')
        self.assertTrue(view.has_text('Potentiometer'),'Cancel selection changes nothing')

        view.press('Sample interval')
        view.edit_texts[0].press()
        view.keyevent('150') #standard sample interval
        view.press('Save')
        self.assertTrue(view.has_text('150'),'Set Sampling Interval')

        #self.assertTrue(view.parent_child_check(parent = 'Enable device', child = 'Enabled') == 1, 'Enable Device: Enabled')
        #view.press('Enable device')
        #view = self.get_view()
        #self.assertTrue(view.parent_child_check(parent = 'Enable device', child = 'Disabled') == 1, 'Enable Device: Disabled')

        if view.has_text('Enabled'):
            view.press('Enable device')
            self.assertTrue(view.has_text('Disabled'),'Disable Input Device')
            view.press('Enable device')
            self.assertTrue(view.has_text('Enabled'),'Enable Input Device')
        else:
            view.has_text('Disabled')
            view.press('Enable device')
            self.assertTrue(view.has_text('Enabled'),'Enable Input Device')


    def view_test_input_2(self, devnum_2):
        view = self.get_view()
        self.homescreen(back_to_settings = True)
        self.default_device_states()

        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        view.press('Settings for analog input 2')
        devnum = devnum_2
        view.press('Analog Input Id')
        view.edit_texts[0].press()
        view.keyevent('65537') #out of range value
        view.press('Save')
        self.assertTrue(view.has_text('Not set'), 'Out of range Analog ID not retained')
        view.press('Analog Input Id')
        #view.edit_texts[0].press()2
        view.keyevent(devnum)
        view.press('Save')
        self.assertTrue(view.has_text(str(devnum)), 'Device ID Assigned')

        view.press('Device type')
        view.press('Switch')
        self.assertTrue(view.has_text('Switch'),'Check for Switch Device type')

        view.press('Device type')
        view.press('Occupancy Sensor')
        self.assertTrue(view.has_text('Occupancy Sensor'),'Check for Occupancy Sensor Device type')

        view.press('Device type')
        view.press('Photocell')
        self.assertTrue(view.has_text('Photocell'),'Check for Photocell Device type')

        view.press('Device type')
        view.press('Contact Control')
        self.assertTrue(view.has_text('Contact Control'),'Check for Contact Control Device type')

        view.press('Device type')
        view.press('Potentiometer')
        self.assertTrue(view.has_text('Potentiometer'),'Check for Potentiometer Device type')

        view.press('Device type')
        view.press('Cancel')
        self.assertTrue(view.has_text('Potentiometer'),'Cancel selection changes nothing')

        view.press('Sample interval')
        view.edit_texts[0].press()
        view.keyevent('150') #standard sample interval
        view.press('Save')
        self.assertTrue(view.has_text('150'),'Set Sampling Interval')
        if view.has_text('Enabled'):
            view.press('Enable device')
            self.assertTrue(view.has_text('Disabled'),'Disable Input Device')
            view.press('Enable device')
            self.assertTrue(view.has_text('Enabled'),'Enable Input Device')
        else:
            view.has_text('Disabled')
            view.press('Enable device')
            self.assertTrue(view.has_text('Enabled'),'Enable Input Device')

    def calibrate_digital_device(self, AInum, dev_type, sample_rate, threshold):
        self.homescreen(back_to_settings = True)
        view = self.get_view()
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        if AInum == 1:
            view = self.get_view()
            view.press('Settings for analog input 1')
            view.press('Sample interval')
            view.edit_texts[0].press()
            view.keyevent(sample_rate)
            view.press('Save')
            self.assertTrue(view.has_text(str(sample_rate)),'Set Sampling Interval' + ': ' + str(sample_rate) +  '-->' + str(dev_type))
            view.press('Threshold')
            view.edit_texts[0].press()
            view.keyevent(threshold)
            view.press('Save')
            self.assertTrue(view.has_text(str(threshold)),'Set Threshold' + ': ' + str(threshold) +  '-->' + str(dev_type))
            view = self.get_view()
            if dev_type == 'Switch':
                view.press('Device type')
                view.press('Switch')
                dev_mux_1(AI_1 = 'Binary')
                switch_state(SW1 = 'on')
            adc_values = Serial_Commands.check_ADC_values()
            print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
            time.sleep(1)
            ADC_max_1 = adc_values[0]
            view.press('Hardware Maximum')
            view.edit_texts[0].press()
            view.keyevent(ADC_max_1)
            view.press('Save')
            self.assertTrue(view.has_text(str(ADC_max_1)), 'AI_1 HW Maximum Assigned' + ': ' + str(ADC_max_1) + '-->' + str(dev_type))
        if AInum == 2:
            view = self.get_view()
            view.press('Settings for analog input 2')
            view.press('Sample interval')
            view.edit_texts[0].press()
            view.keyevent(sample_rate)
            view.press('Save')
            self.assertTrue(view.has_text(str(sample_rate)),'Set Sampling Interval' + ': ' + str(sample_rate) +  '-->' + str(dev_type))
            view.press('Threshold')
            view.edit_texts[0].press()
            view.keyevent(threshold)
            view.press('Save')
            self.assertTrue(view.has_text(str(threshold)),'Set Threshold' + ': ' + str(threshold) +  '-->' + str(dev_type))
            view = self.get_view()
            if dev_type == 'Switch':
                view.press('Device type')
                view.press('Switch')
                dev_mux_2(AI_2 = 'Binary')
                switch_state(SW2 = 'on')
            adc_values = Serial_Commands.check_ADC_values()
            print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
            time.sleep(1)
            ADC_max_2 = adc_values[1]
            view.press('Hardware Maximum')
            view.edit_texts[0].press()
            view.keyevent(ADC_max_2)
            view.press('Save')
            self.assertTrue(view.has_text(str(ADC_max_2)), 'AI_2 HW Maximum Assigned' + ': ' + str(ADC_max_2) + '-->' + str(dev_type))

    def calibrate_analog_device(self, AInum, dev_type, sample_rate, threshold):
        self.homescreen(back_to_settings = True)
        view = self.get_view()
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        if AInum == 1:
            view = self.get_view()
            view.press('Settings for analog input 1')
            view.press('Sample interval')
            view.edit_texts[0].press()
            view.keyevent(sample_rate)
            view.press('Save')
            self.assertTrue(view.has_text(str(sample_rate)),'Set Sampling Interval' + ': ' + str(sample_rate) +  '-->' + str(dev_type))
            view.press('Threshold')
            view.edit_texts[0].press()
            view.keyevent(threshold)
            view.press('Save')
            self.assertTrue(view.has_text(str(threshold)),'Set Threshold' + ': ' + str(threshold) +  '-->' + str(dev_type))
            view = self.get_view()
            if dev_type == 'Photocell':
                view.press('Device type')
                view.press('Photocell')
                dev_mux_1(AI_1 = 'Analog')
                time.sleep(1)
                set_voltage(ch1_voltage = '10', ch2_voltage = '0')
            adc_values = Serial_Commands.check_ADC_values()
            print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
            time.sleep(1)
            ADC_max_1 = adc_values[0]
            view.press('Hardware Maximum')
            view.edit_texts[0].press()
            view.keyevent(ADC_max_1)
            view.press('Save')
            self.assertTrue(view.has_text(str(ADC_max_1)), 'AI_1 HW Maximum Assigned' + ': ' + str(ADC_max_1) + '-->' + str(dev_type))
        if AInum == 2:
            view = self.get_view()
            view.press('Settings for analog input 2')
            view.press('Sample interval')
            view.edit_texts[0].press()
            view.keyevent(sample_rate)
            view.press('Save')
            self.assertTrue(view.has_text(str(sample_rate)),'Set Sampling Interval' + ': ' + str(sample_rate) +  '-->' + str(dev_type))
            view.press('Threshold')
            view.edit_texts[0].press()
            view.keyevent(threshold)
            view.press('Save')
            self.assertTrue(view.has_text(str(threshold)),'Set Threshold' + ': ' + str(threshold) +  '-->' + str(dev_type))
            view = self.get_view()
            if dev_type == 'Photocell':
                view.press('Device type')
                view.press('Photocell')
                dev_mux_2(AI_2 = 'Analog')
                time.sleep(1)
                set_voltage(ch1_voltage = '0', ch2_voltage = '10')
            adc_values = Serial_Commands.check_ADC_values()
            print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
            time.sleep(1)
            ADC_max_2 = adc_values[1]
            view.press('Hardware Maximum')
            view.edit_texts[0].press()
            view.keyevent(ADC_max_2)
            view.press('Save')
            self.assertTrue(view.has_text(str(ADC_max_2)), 'AI_2 HW Maximum Assigned' + ': ' + str(ADC_max_2) + '-->' + str(dev_type))

    def switch_test_AI_1(self):
        dev_mux_1(AI_1 = 'Binary')
        switch_state(SW1 = 'on')
        time.sleep(2)
        switch_state(SW1 = 'off')

    def switch_test_AI_2(self):
        dev_mux_2(AI_2 = 'Binary')
        switch_state(SW2 = 'on')
        time.sleep(2)
        switch_state(SW2 = 'off')

    def switch_test_both(self):
        dev_mux_1(AI_1 = 'Binary')
        dev_mux_2(AI_2 = 'Binary')
        both_switches(state = 'on')
        time.sleep(2)
        both_switches(state = 'off')

    def occ_test_AI_1(self):
        dev_mux_1(AI_1 = 'Binary')
        switch_state(SW1 = 'on')
        time.sleep(2)
        switch_state(SW1 = 'off')

    def occ_test_AI_2(self):
        dev_mux_2(AI_2 = 'Binary')
        switch_state(SW2 = 'on')
        time.sleep(2)
        switch_state(SW2 = 'off')

    def occ_test_both(self):
        dev_mux_1(AI_1 = 'Binary')
        dev_mux_2(AI_2 = 'Binary')
        both_switches(state = 'on')
        time.sleep(2)
        both_switches(state = 'off')

    def contact_test_AI_1(self):
        dev_mux_1(AI_1 = 'Binary')
        switch_state(SW1 = 'on')
        time.sleep(2)
        switch_state(SW1 = 'off')

    def contact_test_AI_2(self):
        dev_mux_2(AI_2 = 'Binary')
        switch_state(SW2 = 'on')
        time.sleep(2)
        switch_state(SW2 = 'off')

    def contact_test_both(self):
        dev_mux_1(AI_1 = 'Binary')
        dev_mux_2(AI_2 = 'Binary')
        both_switches(state = 'on')
        time.sleep(2)
        both_switches(state = 'off')

    def photocell_test_AI_1(self):
        dev_mux_1(AI_1 = 'Analog')
        set_voltage(ch1_voltage = '1', ch2_voltage = '0')
        time.sleep(2)
        set_voltage(ch1_voltage = '3', ch2_voltage = '0')
        time.sleep(2)
        set_voltage(ch1_voltage = '5', ch2_voltage = '0')
        time.sleep(2)
        set_voltage(ch1_voltage = '8', ch2_voltage = '0')
        time.sleep(2)
        set_voltage(ch1_voltage = '10', ch2_voltage = '0')

    def photocell_test_AI_2(self):
        dev_mux_2(AI_2 = 'Analog')
        set_voltage(ch1_voltage = '0', ch2_voltage = '1')
        time.sleep(2)
        set_voltage(ch1_voltage = '0', ch2_voltage = '3')
        time.sleep(2)
        set_voltage(ch1_voltage = '0', ch2_voltage = '5')
        time.sleep(2)
        set_voltage(ch1_voltage = '0', ch2_voltage = '8')
        time.sleep(2)
        set_voltage(ch1_voltage = '0', ch2_voltage = '10')

    def photocell_test_both(self):
        dev_mux_1(AI_1 = 'Analog')
        dev_mux_2(AI_2 = 'Analog')
        set_voltage(ch1_voltage = '1', ch2_voltage = '1')
        time.sleep(2)
        set_voltage(ch1_voltage = '3', ch2_voltage = '3')
        time.sleep(2)
        set_voltage(ch1_voltage = '5', ch2_voltage = '5')
        time.sleep(2)
        set_voltage(ch1_voltage = '8', ch2_voltage = '8')
        time.sleep(2)
        set_voltage(ch1_voltage = '10', ch2_voltage = '10')

    def threshold_test_both(self):
        dev_mux_1(AI_1 = 'Analog')
        dev_mux_2(AI_2 = 'Analog')
        set_voltage(ch1_voltage = '1', ch2_voltage = '1')
        time.sleep(2)
        set_voltage(ch1_voltage = '2', ch2_voltage = '2')
        time.sleep(2)
        set_voltage(ch1_voltage = '5', ch2_voltage = '5')
        time.sleep(2)
        set_voltage(ch1_voltage = '7', ch2_voltage = '7')

    def AI_1_photocell_AI_2_switch(self):
        dev_mux_1(AI_1 = 'Analog')
        dev_mux_2(AI_2 = 'Digital')
        set_voltage(ch1_voltage = '1', ch2_voltage = '0')
        switch_state(SW2 = 'on')
        set_voltage(ch1_voltage = '3', ch2_voltage = '0')
        switch_state(SW2 = 'off')
        set_voltage(ch1_voltage = '5', ch2_voltage = '0')
        switch_state(SW2 = 'on')
        set_voltage(ch1_voltage = '8', ch2_voltage = '0')
        switch_state(SW2 = 'off')
        switch_state(SW2 = 'on')
        set_voltage(ch1_voltage = '10', ch2_voltage = '0')
        switch_state(SW2 = 'off')

    def AI_1_switch_AI_2_photocell(self):
        dev_mux_1(AI_1 = 'Digital')
        dev_mux_2(AI_2 = 'Analog')
        set_voltage(ch1_voltage = '0', ch2_voltage = '1')
        switch_state(SW1 = 'on')
        set_voltage(ch1_voltage = '0', ch2_voltage = '3')
        switch_state(SW1 = 'off')
        set_voltage(ch1_voltage = '0', ch2_voltage = '5')
        switch_state(SW1 = 'on')
        set_voltage(ch1_voltage = '0', ch2_voltage = '8')
        switch_state(SW1 = 'off')
        switch_state(SW1 = 'on')
        set_voltage(ch1_voltage = '0', ch2_voltage = '10')
        switch_state(SW1 = 'off')


    def run(self):
        self.print_intro()
        view = self.get_view()
        devnum_1 = 66
        devnum_2 = 65535
        self.homescreen(back_to_settings = False)
        
        print('View Tests-------------------------')

        self.view_test_input_1(devnum_1 = devnum_1)
        self.view_test_input_2(devnum_2 = devnum_2)

        self.default_device_states()
        self.calibrate_digital_device(AInum = 1, dev_type = 'Switch', sample_rate = 100, threshold = 5)
        self.calibrate_digital_device(AInum = 2, dev_type = 'Switch', sample_rate = 100, threshold = 5)

        self.homescreen(back_to_settings = True)
        print('Switch Test-------------------------')
        self.go_to_analog_1_settings(device_type = 'Switch')
        self.go_to_analog_2_settings(device_type = 'Switch')
        self.homescreen(back_to_settings = False)
        with self.messages_buffering():
            #can't run these in parallel. They fight over the serial port
            self.switch_test_AI_1()
            self.switch_test_AI_2()
        self.can_check(button = 'AI_1 Checking Switch On', message_type = 81, input_device_type = 1, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_1 Checking Switch Off', message_type = 81, input_device_type = 1, input_device_number = devnum_1, input_device_status = 0, input_device_state = 0)
        self.can_check(button = 'AI_2 Checking Switch On', message_type = 81, input_device_type = 1, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_2 Checking Switch Off', message_type = 81, input_device_type = 1, input_device_number = devnum_2, input_device_status = 0, input_device_state = 0)

        self.default_device_states()
        self.homescreen(back_to_settings = False)
        print('Both Switch Test-------------------------')
        with self.messages_buffering():
            self.switch_test_both()
        self.can_check(button = 'Both Checking Switch On', message_type = 81, input_device_type = 1, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'Both Checking Switch Off', message_type = 81, input_device_type = 1, input_device_number = devnum_1, input_device_status = 0, input_device_state = 0)
        self.can_check(button = 'Both Checking Switch On', message_type = 81, input_device_type = 1, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'Both Checking Switch Off', message_type = 81, input_device_type = 1, input_device_number = devnum_2, input_device_status = 0, input_device_state = 0)


        self.default_device_states()
        print('Occ Test-------------------------')
        self.go_to_analog_1_settings(device_type = 'Occupancy Sensor')
        self.go_to_analog_2_settings(device_type = 'Occupancy Sensor')
        self.homescreen(back_to_settings = False)
        with self.messages_buffering():
            #can't run these in parallel. They fight over the serial port
            self.occ_test_AI_1()
            self.occ_test_AI_2()
        self.can_check(button = 'AI_1 Checking Occupancy Sensor Occupied', message_type = 81, input_device_type = 2, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_1 Checking Occupancy Sensor Vacant', message_type = 81, input_device_type = 2, input_device_number = devnum_1, input_device_status = 0, input_device_state = 0)
        self.can_check(button = 'AI_2 Checking Occupancy Sensor Occupied', message_type = 81, input_device_type = 2, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_2 Checking Occupancy Sensor Vacant', message_type = 81, input_device_type = 2, input_device_number = devnum_2, input_device_status = 0, input_device_state = 0)

        print('Both Occ Test-------------------------')
        with self.messages_buffering():
            self.occ_test_both()
        self.can_check(button = 'Both Checking Occupancy Sensor Occupied', message_type = 81, input_device_type = 2, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'Both Checking Occupancy Sensor Vacant', message_type = 81, input_device_type = 2, input_device_number = devnum_1, input_device_status = 0, input_device_state = 0)
        self.can_check(button = 'Both Checking Occupancy Sensor Occupied', message_type = 81, input_device_type = 2, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'Both Checking Occupancy Sensor Vacant', message_type = 81, input_device_type = 2, input_device_number = devnum_2, input_device_status = 0, input_device_state = 0)

        self.default_device_states()
        print('Contact Control Test-------------------------')
        self.go_to_analog_1_settings(device_type = 'Contact Control')
        self.go_to_analog_2_settings(device_type = 'Contact Control')
        self.homescreen(back_to_settings = False)
        with self.messages_buffering():
            #can't run these in parallel. They fight over the serial port
            self.contact_test_AI_1()
            self.contact_test_AI_2()
        self.can_check(button = 'AI_1 Checking Contact Control On', message_type = 81, input_device_type = 4, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_1 Checking Contact Control Off', message_type = 81, input_device_type = 4, input_device_number = devnum_1, input_device_status = 0, input_device_state = 0)
        self.can_check(button = 'AI_2 Checking Contact Control On', message_type = 81, input_device_type = 4, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_2 Checking Contact Control Off', message_type = 81, input_device_type = 4, input_device_number = devnum_2, input_device_status = 0, input_device_state = 0)

        print('Both Contact Control Test-------------------------')
        self.homescreen(back_to_settings = False)
        with self.messages_buffering():
            self.contact_test_both()
        self.can_check(button = 'Both Checking Contact Control On', message_type = 81, input_device_type = 4, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'Both Checking Contact Control Off', message_type = 81, input_device_type = 4, input_device_number = devnum_1, input_device_status = 0, input_device_state = 0)
        self.can_check(button = 'Both Checking Contact Control On', message_type = 81, input_device_type = 4, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'Both Checking Contact Control Off', message_type = 81, input_device_type = 4, input_device_number = devnum_2, input_device_status = 0, input_device_state = 0)

        self.default_device_states()
        self.homescreen(back_to_settings = False)
        print('AI_1 PhotoCell Test-------------------------')
        self.calibrate_analog_device(AInum = 1, dev_type = 'Photocell', sample_rate = 200, threshold = 10)
        self.calibrate_analog_device(AInum = 2, dev_type = 'Photocell', sample_rate = 200, threshold = 10)
        with self.messages_buffering():
            self.photocell_test_AI_1()
        self.can_check(button = 'AI_1 Checking Photocell at 1V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 10)
        self.can_check(button = 'AI_1 Checking Photocell at 3V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 30)
        self.can_check(button = 'AI_1 Checking Photocell at 5V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 50)
        self.can_check(button = 'AI_1 Checking Photocell at 8V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 80)
        self.can_check(button = 'AI_1 Checking Photocell at 10V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)

        self.homescreen(back_to_settings = False)
        print('AI_2 PhotoCell Test-------------------------')
        with self.messages_buffering():
            self.photocell_test_AI_2()
        self.can_check(button = 'AI_2 Checking Photocell at 1V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 10)
        self.can_check(button = 'AI_2 Checking Photocell at 3V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 30)
        self.can_check(button = 'AI_2 Checking Photocell at 5V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 50)
        self.can_check(button = 'AI_2 Checking Photocell at 8V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 80)
        self.can_check(button = 'AI_2 Checking Photocell at 10V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)

        """this test does two things, first: it test that both AI's can operate a photocell.
        Second, it tests that Sapphire can see a simultaneous change on both AI's but still output to the CAN bus in a serial format
        """

        print('Both PhotoCell Test-------------------------')
        self.homescreen(back_to_settings = False)
        with self.messages_buffering():
            self.photocell_test_both()
        self.can_check(button = 'Both_AI_1 Checking Photocell at 1V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 10)
        self.can_check(button = 'Both_AI_2 Checking Photocell at 1V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 10)
        self.can_check(button = 'Both_AI_1 Checking Photocell at 3V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 30)
        self.can_check(button = 'Both_AI_2 Checking Photocell at 3V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 30)
        self.can_check(button = 'Both_AI_1 Checking Photocell at 5V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 50)
        self.can_check(button = 'Both_AI_2 Checking Photocell at 5V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 50)
        self.can_check(button = 'Both_AI_1 Checking Photocell at 8V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 80)
        self.can_check(button = 'Both_AI_2 Checking Photocell at 8V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 80)
        self.can_check(button = 'Both_AI_1 Checking Photocell at 10V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'Both_AI_2 Checking Photocell at 10V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)


        self.default_device_states()
        print('AI_1 PC and AI_2 SW Test-------------------------')
        self.calibrate_analog_device(AInum = 1, dev_type = 'Photocell', sample_rate = 100, threshold = 5)
        self.calibrate_digital_device(AInum = 2, dev_type = 'Switch', sample_rate = 50, threshold = 40)
        self.homescreen(back_to_settings = False)
        with self.messages_buffering():
            self.AI_1_photocell_AI_2_switch()
        self.can_check(button = 'AI_1_PC Checking Photocell at 1V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 10)
        self.can_check(button = 'AI_1_PC Checking Photocell at 3V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 30)
        self.can_check(button = 'AI_1_PC Checking Photocell at 5V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 50)
        self.can_check(button = 'AI_1_PC Checking Photocell at 8V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 80)
        self.can_check(button = 'AI_1_PC Checking Photocell at 10V', message_type = 81, input_device_type = 3, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_2_SW Checking Switch On', message_type = 81, input_device_type = 1, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_2_SW Checking Switch Off', message_type = 81, input_device_type = 1, input_device_number = devnum_2, input_device_status = 0, input_device_state = 0)

        self.default_device_states()
        print('AI_1 SW and AI_2 PC Test-------------------------')
        self.calibrate_analog_device(AInum = 2, dev_type = 'Photocell',sample_rate = 100, threshold = 5)
        self.calibrate_digital_device(AInum = 1, dev_type = 'Switch', sample_rate = 50, threshold = 40)
        self.homescreen(back_to_settings = False)
        with self.messages_buffering():
            self.AI_1_switch_AI_2_photocell()
        self.can_check(button = 'AI_2_PC Checking Photocell at 1V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 10)
        self.can_check(button = 'AI_2_PC Checking Photocell at 3V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 30)
        self.can_check(button = 'AI_2_PC Checking Photocell at 5V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 50)
        self.can_check(button = 'AI_2_PC Checking Photocell at 8V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 80)
        self.can_check(button = 'AI_2_PC Checking Photocell at 10V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_1_SW Checking Switch On', message_type = 81, input_device_type = 1, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'AI_1_SW Checking Switch Off', message_type = 81, input_device_type = 1, input_device_number = devnum_1, input_device_status = 0, input_device_state = 0)
        """skipping broadcast message check. If this feature doesn't work, it will become really obvious when testing doesn't see any CAN traffic"""
