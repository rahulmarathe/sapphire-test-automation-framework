import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View




class FaradayTestCase(sapphire.SapphireTestCase):
    '''
    Note: This test will fail certain criteria if the system is not connected to a LC3 Gateway. The test is looking for certain LC messages on the
    wire and wil fail if the gateway doesn't send them. We might eventually simulate a gateway in software to remove the need for a physical device
    '''
    tab = 'Faraday'
    tab_position = 'right'

    def run(self):
        self.print_intro()

        self.view.slide('A1 GWY', 0) #setting slider to 0 before sarting test
        self.view.slide('A3 GWY GMX', 0) #setting slider to 0 before sarting test

        ds_toggle_relay(8)
        time.sleep(3)

        self.press(slider = 'A1 GWY', slider_level = 100, can_read = True) #pressing slider without testing relay result, but still needs CAN check
        #self.view.slide('A1 GWY', 100)
        for cnum,target_level in zip([17, 18, 19], [100, 100, 100]):
            self.can_check(button = 'A1 GWY', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(slider='A2 GMX', slider_level=100, relays=[5, 6, 7], levels=[100, 100, 100])
        for cnum,target_level in zip([5, 6, 7], [100, 100, 100]):
            self.can_check(button = 'A2 GMX', message_type = 45, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = target_level)

        self.test_relay_result(slider='A2 GMX', slider_level=0, relays=[5, 6, 7], levels=[0, 0, 0])
        for cnum,target_level, in zip([5, 6, 7], [0, 0, 0]):
            self.can_check(button = 'A2 GMX', message_type = 45, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = target_level)

        self.test_relay_result(slider='A3 GWY GMX', slider_level=100, relays=[5, 6, 7], levels=[100, 100, 100])
        for cnum,target_level, in zip([5, 6, 7, 18, 19], [100, 100, 100, 100, 100]):
            self.can_check(button = 'A3 GWY GMX', message_type = 45, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = target_level)

        self.test_relay_result(slider='A3 GWY GMX', slider_level=0, relays=[5, 6, 7], levels=[0, 0, 0])
        for cnum,target_level, in zip([5, 6, 7, 18, 19], [0, 0, 0, 0, 0]):
            self.can_check(button = 'A3 GWY GMX', message_type = 45, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = target_level)

        self.press(button = 'B1 GWY', can_read = True)
        print('Waiting 10 Seconds')
        time.sleep(10)
        self.can_check(button = 'B1 GWY', message_type = 45, fade_time_or_rate = 0, fade_rate = 10, channel_number = 17, target_level = 70)

        self.press(button = 'B1 GWY', can_read = True)
        print('Waiting 10 Seconds')
        time.sleep(10)
        self.can_check(button = 'B1 GWY', message_type = 45, fade_time_or_rate = 0, fade_rate = 10, channel_number = 17, target_level = 0)

        self.press(button = 'B2 GWY', can_read = True)
        print('Waiting 10 Seconds')
        time.sleep(5)
        self.can_check(button = 'B2 GWY', message_type = 45, fade_time_or_rate = 0, fade_rate = 5, channel_number = 18, target_level = 55)

        self.press(button = 'B2 GWY', can_read = True)
        print('Waiting 10 Seconds')
        time.sleep(5)
        self.can_check(button = 'B2 GWY', message_type = 45, fade_time_or_rate = 0, fade_rate = 5, channel_number = 18, target_level = 0)

        self.test_relay_result(button='B2 GWY GMX', button_position='right', relays=[1, 2], levels=[1, 1], binary = True)
        for cnum,target_level, in zip([1, 2, 17, 18], [100, 100, 70, 70]):
            self.can_check(button = 'B2 GWY GMX', message_type = 45, fade_time_or_rate = 1, fade_rate = 10, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='B2 GWY GMX', button_position='right', relays=[1, 2], levels=[0, 0], binary = True)
        for cnum,target_level, in zip([1, 2, 17, 18], [0, 0, 0, 0]):
            self.can_check(button = 'B2 GWY GMX', message_type = 45, fade_time_or_rate = 1, fade_rate = 10, channel_number = cnum, target_level = target_level)
