import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View
from leviton_test import Kvaser_Functions
from leviton_test.parser import messages



class BabbageTestCase(sapphire.SapphireTestCase):
    tab = 'Babbage'
    tab_position = 'left'

    def run(self):
        self.print_intro()

        ds_toggle_relay(8)
        self.test_relay_result(slider='Babb 13', slider_level=100, relays=[13], levels=[40])
        self.can_check(button = 'Babb 13', message_type = 45, fade_time_or_rate = 0, fade_rate = 1.0, channel_number = 13, target_level = 40)

        self.test_relay_result(slider='Babb 14', slider_level=100, relays=[14], levels=[50])
        self.can_check(button = 'Babb 14', message_type = 45, fade_time_or_rate = 0, fade_rate = 1.0, channel_number = 14, target_level = 50)

        self.test_relay_result(slider='Babb 15', slider_level=100, relays=[15], levels=[75])
        self.can_check(button = 'Babb 15', message_type = 45, fade_time_or_rate = 0, fade_rate = 1.0, channel_number = 15, target_level = 75)

        self.test_relay_result(slider='Babb 16', slider_level=100, relays=[16], levels=[100])
        self.can_check(button = 'Babb 16', message_type = 45, fade_time_or_rate = 0, fade_rate = 1.0, channel_number = 16, target_level = 100)

        self.test_relay_result(slider='Babb 5-8', slider_level=100, relays=[5, 6, 7, 8], levels=[100, 100, 100, 100], wait_time=5)
        self.can_check(button = 'Babb 5-8', message_type = 65, fade_time_or_rate = 1, fade_rate = 5, group_number = 2)

        print('ds_toggle_relay(8)')
        ds_toggle_relay(8)

        self.test_relay_result(slider='Babb 13-16', slider_level=100, relays=[13, 14, 15, 16], levels=[50, 70, 80, 90], wait_time=10)
        for cnum,target_level in zip([13, 14, 15, 16], [50, 70, 80, 90]):
            self.can_check(button = 'Babb 13-16', message_type = 45, fade_time_or_rate = 0, fade_rate = 10, channel_number = cnum, target_level = target_level)
        time.sleep(2)
