import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View
from leviton_test import Serial_Commands
from leviton_test.utils import sapphire_AI_select
from leviton_test.Voltage_Control import set_voltage
from leviton_test.Input_Device_MUX import dev_mux_1, dev_mux_2, switch_state


class GeneralTest(sapphire.SapphireTestCase):

    tab = 'Gregory'
    tab_position = 'left'

    def run(self):
        self.print_intro()
        view = self.get_view()
        self.press(button='Channel 1', can_read=True)
    #self.test_relay_result(button='Channel 1', button_position='left', relays=[1], levels=[1], binary=True)
        self.can_check(
        button = 'Channel 1',
        message_type = 45,
        fade_time_or_rate = 0,
        fade_rate = 1.0,
        channel_number = 1,
        priority = 7,
        target_level = 255,
        header_priority = 5,
        header_broadcast_or_muticast = 1,
        header_destination_routing = 1,
        header_destination_address = 255,
        header_source_routing = 0,
        header_source_address = 56,
        header_start_linked = 1,
        header_sequence = 15
        )

