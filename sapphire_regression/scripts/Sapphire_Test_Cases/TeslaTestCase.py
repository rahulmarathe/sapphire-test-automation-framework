import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View

class TeslaTestCase(sapphire.SapphireTestCase):
    tab = 'Tesla'
    tab_position = 'left'

    def run(self):
        self.print_intro()
        print('ds_toggle_relay(8)')
        ds_toggle_relay(8)
        time.sleep(5)
        self.test_relay_result(button='A', button_position='left', relays=[1, 2], levels=[1, 1], binary=True)
        for cnum,target_level in zip([1, 2,], [100, 100]):
            self.can_check(button = 'A', message_type = 45, fade_time_or_rate = 1, fade_rate = 3, channel_number = cnum, target_level = target_level)

        time.sleep(2)
        print('Using DS buttons to turn off channels 1 and 2')

        # 1. messages_buffering() opens up the CAN channel, at #2 (comments below) all of the CAN
        #    traffic is stored that occured while the inner block of code was running.
        with self.messages_buffering():
            ds_toggle_relay(1)
        # 2. At this stage, all of the can messages are stored in self (and therefore accessible by can_check())
        #    and the flush_can_bus() is called implicitly so that the next use of the can buffer
        #    is not polluted.
        self.can_check(button = 'GTDS_1', message_type = 48, fade_time_or_rate = 1, fade_rate = 5, channel_number = 1, header_source_address = 33)

        with self.messages_buffering():
            ds_toggle_relay(2)
        self.can_check(button = 'GTDS_2', message_type = 48, fade_time_or_rate = 1, fade_rate = 5, channel_number = 2, header_source_address = 33)

        relays = binary_state()
        self.assertTrue(relays[1] == 0, 'GTDS turned relay 1 off')
        self.assertTrue(relays[2] == 0, 'GTDS turned relay 2 off')
        print('TODO: check that `Feedback A` button darkens, and `No Feedback A` stays lit after ds toggle')

        time.sleep(2)
        self.test_relay_result(button='B', button_position='left', relays=[3], levels=[1], binary=True)
        self.can_check(button = 'B', message_type = 45, fade_time_or_rate = 1, fade_rate = 3, channel_number = 3, target_level = 100)

        print('Using DS button to turn on channel 4')
        with self.messages_buffering():
            ds_toggle_relay(4)
        self.can_check(button = 'GTDS_4', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = 4, target_level = 100, header_source_address = 33)

        relays = binary_state()
        self.assertTrue(relays[4] == 1, 'DS turned relay 4 On')

        time.sleep(2)
        print('TODO: check that `Feedback C` button is lit, and `No Feedback C` darkens after ds toggle')
        print('Using DS buttons to turn off channels 3 and 4')
        with self.messages_buffering():
            ds_toggle_relay(3)
        self.can_check(button = 'GTDS_3', message_type = 48, fade_time_or_rate = 1, fade_rate = 5, channel_number = 3, header_source_address = 33)
        with self.messages_buffering():
            ds_toggle_relay(4)
        self.can_check(button = 'GTDS_4', message_type = 48, fade_time_or_rate = 1, fade_rate = 5, channel_number = 4, header_source_address = 33)
        relays = binary_state()
        self.assertTrue(relays[3] == 0, 'DS turned relay 3 off')
        self.assertTrue(relays[4] == 0, 'DS turned relay 4 off')

        time.sleep(2)
        print('TODO: check that `Feedback B` and `Feedback C` darken, but `No Feedback` B and C stay lit after ds toggle')
        self.test_relay_result(button='D', button_position = 'right', relays=[5, 6, 7, 8], levels=[75, 75, 75, 75], binary=False)
        self.can_check(button = 'D', message_type = 65, fade_time_or_rate = 1, fade_rate = 3, group_number = 2,)
