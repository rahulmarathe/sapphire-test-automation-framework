import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View


class NewtonTestCase(sapphire.SapphireTestCase):
    """Defines the Newton tab test case.

    TODO: Implement screenshot checking for button light up

    """

    tab = 'Newton'
    tab_position = 'left'

    def run(self):
        self.print_intro()
        ds_toggle_relay(8)
        time.sleep(3)
        self.test_relay_result(button='A1', relays=[1], levels=[1], binary=True)
        self.can_check(button = 'A1', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 1, fade_rate = 10, channel_number = 1, target_level = 100)
        self.can_check(button='Next Frame', header_source_address = 56, header_start_linked = 0, header_sequence = 15, channel_number_1=1, target_level_1=100)


        self.test_relay_result(button='A1', relays=[1], levels=[0], binary=True)
        self.can_check(button = 'A1', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 1, fade_rate = 10, channel_number = 1, target_level = 0)

        self.test_relay_result(button='A2', relays=[1], levels=[1], binary=True)
        self.can_check(button = 'A2', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 1, fade_rate = 5, channel_number = 1, target_level = 100)

        self.test_relay_result(button='A2', relays=[1], levels=[0], binary=True)
        self.can_check(button = 'A2', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 1, fade_rate = 5, channel_number = 1, target_level = 0)

        self.test_relay_result(button='B1', relays=[2, 3], levels=[1, 1], binary=True)
        for cnum, target_level in zip([2, 3], [100, 100]):
            self.can_check(button = 'B1', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='B1', relays=[2, 3], levels=[0, 0], binary=True)
        for cnum, target_level in zip([2, 3], [0, 0]):
            self.can_check(button = 'B1', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='B2', relays=[2, 3], levels=[1, 1], binary=True)
        for cnum, target_level in zip([2, 3], [100, 100]):
            self.can_check(button = 'B2', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='B2', relays=[2, 3], levels=[0, 0], binary=True)
        for cnum, target_level in zip([2, 3], [0, 0]):
            self.can_check(button = 'B2', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='C1', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[1, 1, 1, 1, 1, 1, 1, 1], binary=True)
        self.can_check(button = 'C1', message_type = 65, rel_abs_fade = 0, fade_time_or_rate = 1, fade_rate = 5, group_number = 1)

        self.test_relay_result(button='C1', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[0, 0, 0, 0, 0, 0, 0, 0], binary=True)
        self.can_check(button = 'C1', message_type = 65, rel_abs_fade = 0, fade_time_or_rate = 1, fade_rate = 5, group_number = 1)

        self.test_relay_result(button='C2', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[1, 1, 1, 1, 1, 1, 1, 1], binary=True)
        self.can_check(button = 'C2', message_type = 65, rel_abs_fade = 0, fade_time_or_rate = 1, fade_rate = 5, group_number = 1)

        self.test_relay_result(button='C2', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[0, 0, 0, 0, 0, 0, 0, 0], binary=True)
        self.can_check(button = 'C2', message_type = 65, rel_abs_fade = 0, fade_time_or_rate = 1, fade_rate = 5, group_number = 1)
