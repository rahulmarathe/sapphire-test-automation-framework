import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View



class KeplerTestCase(sapphire.SapphireTestCase):
    tab = 'Kepler'
    tab_position = 'right'

    def run(self):
        self.print_intro()
        ds_toggle_relay(8)
        time.sleep(5)

        self.test_relay_result(button='All On', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[1, 1, 1, 1, 1, 1, 1, 1], binary=True)
        for cnum, target_level in zip([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16], [100 for _ in range(16)]):
            self.can_check(button = 'All On', message_type = 45, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = target_level)
        self.test_relay_result(button='All On', relays=[5, 6, 7, 8, 13, 14, 15, 16], levels=[100, 100, 100, 100, 100, 100, 100, 100])

        self.test_relay_result(button='All Off', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[0, 0, 0, 0, 0, 0, 0, 0], binary=True)
        for cnum, target_level in zip([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16], [0 for _ in range(16)]):
            self.can_check(button = 'All On', message_type = 45, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = target_level)
        self.test_relay_result(button='All Off', relays=[5, 6, 7, 8, 13, 14, 15, 16], levels=[0, 0, 0, 0, 0, 0, 0, 0])

        #group tests. G1 is all binary, g2-6 are analog with various fade times. Test checks for both on and off toggles
        #test only checks responses until linked frames are working, then it will check group fade messages

        self.test_relay_result(button='Group 1', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[1, 1, 1, 1, 1, 1, 1, 1], binary=True)
        for cnum, target_level in zip([1, 2, 3, 4, 9, 10, 11, 12], [100, 100, 100, 100, 100, 100, 100, 100]):
            self.can_check(button = 'Group 1', message_type = 50, fade_time_or_rate = 0, channel_number = cnum, hw_level = target_level) # not a channel level with fade message

        self.test_relay_result(button='Group 1', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[0, 0, 0, 0, 0, 0, 0, 0], binary=True)
        for cnum, target_level in zip([1, 2, 3, 4, 9, 10, 11, 12], [0, 0, 0, 0, 0, 0, 0, 0]):
            self.can_check(button = 'Group 1', message_type = 50, fade_time_or_rate = 0, channel_number = cnum, hw_level = target_level) # not a channel level with fade message

        self.test_relay_result(button='Group 2', relays=[5, 6, 7, 8, 13, 14, 15, 16], levels=[100, 100, 100, 100, 100, 100, 100, 100], wait_time=5)
        for cnum, target_level in zip([5, 6, 7, 8, 13, 14, 15, 16], [100, 100, 100, 100, 100, 100, 100, 100]):
            self.can_check(button = 'Group 2', message_type = 50, fade_time_or_rate = 0, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Group 2', relays=[5, 6, 7, 8, 13, 14, 15, 16], levels=[0, 0, 0, 0, 0, 0, 0, 0], wait_time=5)
        for cnum, target_level in zip([5, 6, 7, 8, 13, 14, 15, 16], [0, 0, 0, 0, 0, 0, 0, 0]):
            self.can_check(button = 'Group 2', message_type = 50, fade_time_or_rate = 0, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Group 3', relays=[5, 6,], levels=[100, 100], wait_time=10)
        for cnum, target_level in zip([5, 6], [100, 100]):
            self.can_check(button = 'Group 3', message_type = 50, fade_time_or_rate = 0, fade_rate = 10, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Group 3', relays=[5, 6,], levels=[0, 0], wait_time=10)
        for cnum, target_level in zip([5, 6], [0, 0]):
            self.can_check(button = 'Group 3', message_type = 50, fade_time_or_rate = 0, fade_rate = 10, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Group 4', relays=[7, 8,], levels=[100, 100], wait_time=3)
        for cnum, target_level, in zip([7, 8], [100, 100]):
            self.can_check(button = 'Group 4', message_type = 50, fade_time_or_rate = 0, fade_rate = 3, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Group 4', relays=[7, 8,], levels=[0, 0], wait_time=3)
        for cnum, target_level, in zip([7, 8], [0, 0]):
            self.can_check(button = 'Group 4', message_type = 50, fade_time_or_rate = 0, fade_rate = 3, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Group 5', relays=[13, 14,], levels=[100, 100], wait_time=15)
        for cnum, target_level, in zip([13, 14], [100, 100]):
            self.can_check(button = 'Group 5', message_type = 50, fade_time_or_rate = 0, fade_rate = 15, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Group 5', relays=[13, 14,], levels=[0, 0], wait_time=15)
        for cnum, target_level, in zip([13, 14], [0, 0]):
            self.can_check(button = 'Group 5', message_type = 50, fade_time_or_rate = 0, fade_rate = 15, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Group 6', relays=[14, 15, 16,], levels=[100, 100, 100], wait_time=20)
        for cnum, target_level, in zip([14, 15, 16], [100, 100, 100]):
            self.can_check(button = 'Group 6', message_type = 50, fade_time_or_rate = 0, fade_rate = 20, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Group 6', relays=[14, 15, 16,], levels=[0, 0, 0], wait_time=20)
        for cnum, target_level, in zip([14, 15, 16], [0, 0, 0]):
            self.can_check(button = 'Group 6', message_type = 50, fade_time_or_rate = 0, fade_rate = 20, channel_number = cnum, target_level = target_level)
