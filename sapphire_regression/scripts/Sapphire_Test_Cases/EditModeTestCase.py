import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View




class EditModeTestCase(sapphire.SapphireTestCase):

    def run(self):
        self.print_intro()
        view = self.get_view()
        self.goto_edit_mode_from_settings()
        view.scroll_menu_right()
        view.press('Newton')
        view.press('A1')
        view.slide('C1', 100)
        view.slide('C1', 100)
        view.press('Save')
        self.goto_edit_mode_from_settings()
        view.scroll_menu_right()
        view.press('Newton')
        view.press('A1')
        self.assertTrue(view.has_text('100'), 'Changed button A1 to 100 via Edit Mode')
        view.slide('C1', 0)
        view.slide('C1', 0)
        view.press('Save')

        self.goto_edit_mode_from_settings()
        view.scroll_menu_right()
        view.press('Babbage')
        slider = self.view.sliders[0]
        view.press_xy(slider.mid, (slider.top + slider.bottom) / 2)
        view.slide('C13', 100)
        view.slide('C13', 100)
        view.press('Save')


        self.goto_edit_mode_from_settings()
        view.scroll_menu_right()
        view.press('Babbage')
        slider = self.view.sliders[0]
        view.press_xy(slider.mid, (slider.top + slider.bottom) / 2)
        self.assertTrue(view.has_text('100'), 'Changed button Babb 13 to 100 via Edit Mode')
        view.slide('C13', 45)
        view.slide('C13', 45)
        view.press('Save')



    def goto_edit_mode_from_settings(self):
        view = self.get_view()
        view.refresh()
        view.press_settings()
        if 'Sign out' not in view.text_elements:
            view.press('Sign in')
            view.edit_texts[1].press()
            view.keyevent('5625')
            view.press('OK')
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Edit Mode')
