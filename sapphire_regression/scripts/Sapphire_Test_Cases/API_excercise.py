import threading
from threading import Thread
import multiprocessing
from multiprocessing import Process
import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View
from leviton_test import Serial_Commands
from leviton_test.Voltage_Control import set_voltage
from leviton_test.Input_Device_MUX import dev_mux_1, dev_mux_2, switch_state, both_switches



class APIexcercise(sapphire.SapphireTestCase):

    def runInParallel(*fns): #got this nice parallel process function wrapper from stackoverflow
      proc = []
      for fn in fns:
        p = Process(target=fn)
        p.start()
        proc.append(p)
      for p in proc:
        p.join()

    def device_message(self, command, header):
        canparser.broadcast_msg(self.can_channel, command, header)

    def default_device_states(self):
        print('Setting Default Hardware Values')
        #sets multiplexing relays to 0
        dev_mux_1(AI_1 = 'Reset')
        #setting all DAQ voltages to 0
        set_voltage(ch1_voltage = '0', ch2_voltage = '0')

    def homescreen(self, back_to_settings = False):
        #gets the view back to the homescreen
        view = self.get_view()
        if back_to_settings == False:
            if not view.settings_button:
                while not view.has_text('Exit'):
                    view.press('Done')
                view.press('Exit')
        if back_to_settings == True:
            if not view.settings_button:
                while not view.has_text('Exit'):
                    view.press('Done')
                view.press('Exit')
            view.press_settings()
            if 'Sign out' not in view.text_elements:
                view.press('Sign in')
                view.edit_texts[1].press()
                view.keyevent('5625')
                view.press('OK')

    def go_to_analog_1_settings(self, device_type):
        self.homescreen(back_to_settings = True)
        view = self.get_view()
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        view.press('Settings for analog input 1')
        view.press('Device type')
        view.press(device_type)

    def go_to_analog_2_settings(self, device_type):
        self.homescreen(back_to_settings = True)
        view = self.get_view()
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        view.press('Settings for analog input 2')
        view.press('Device type')
        view.press(device_type)

    def calibrate_digital_device(self, AInum, dev_type, sample_rate, threshold):
        self.homescreen(back_to_settings = True)
        view = self.get_view()
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        if AInum == 1:
            view = self.get_view()
            view.press('Settings for analog input 1')
            view.press('Sample interval')
            view.edit_texts[0].press()
            view.keyevent(sample_rate)
            view.press('Save')
            self.assertTrue(view.has_text(str(sample_rate)),'Set Sampling Interval' + ': ' + str(sample_rate) +  '-->' + str(dev_type))
            view.press('Threshold')
            view.edit_texts[0].press()
            view.keyevent(threshold)
            view.press('Save')
            self.assertTrue(view.has_text(str(threshold)),'Set Threshold' + ': ' + str(threshold) +  '-->' + str(dev_type))
            view = self.get_view()
            if dev_type == 'Switch':
                view.press('Device type')
                view.press('Switch')
                dev_mux_1(AI_1 = 'Binary')
                switch_state(SW1 = 'on')
            adc_values = Serial_Commands.check_ADC_values()
            print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
            time.sleep(1)
            ADC_max_1 = adc_values[0]
            view.press('Hardware Maximum')
            view.edit_texts[0].press()
            view.keyevent(ADC_max_1)
            view.press('Save')
            self.assertTrue(view.has_text(str(ADC_max_1)), 'AI_1 HW Maximum Assigned' + ': ' + str(ADC_max_1) + '-->' + str(dev_type))
        if AInum == 2:
            view = self.get_view()
            view.press('Settings for analog input 2')
            view.press('Sample interval')
            view.edit_texts[0].press()
            view.keyevent(sample_rate)
            view.press('Save')
            self.assertTrue(view.has_text(str(sample_rate)),'Set Sampling Interval' + ': ' + str(sample_rate) +  '-->' + str(dev_type))
            view.press('Threshold')
            view.edit_texts[0].press()
            view.keyevent(threshold)
            view.press('Save')
            self.assertTrue(view.has_text(str(threshold)),'Set Threshold' + ': ' + str(threshold) +  '-->' + str(dev_type))
            view = self.get_view()
            if dev_type == 'Switch':
                view.press('Device type')
                view.press('Switch')
                dev_mux_2(AI_2 = 'Binary')
                switch_state(SW2 = 'on')
            adc_values = Serial_Commands.check_ADC_values()
            print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
            time.sleep(1)
            ADC_max_2 = adc_values[1]
            view.press('Hardware Maximum')
            view.edit_texts[0].press()
            view.keyevent(ADC_max_2)
            view.press('Save')
            self.assertTrue(view.has_text(str(ADC_max_2)), 'AI_2 HW Maximum Assigned' + ': ' + str(ADC_max_2) + '-->' + str(dev_type))

    def calibrate_analog_device(self, AInum, dev_type, sample_rate, threshold):
        self.homescreen(back_to_settings = True)
        view = self.get_view()
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Analog Input Configuration')
        if AInum == 1:
            view = self.get_view()
            view.press('Settings for analog input 1')
            view.press('Sample interval')
            view.edit_texts[0].press()
            view.keyevent(sample_rate)
            view.press('Save')
            self.assertTrue(view.has_text(str(sample_rate)),'Set Sampling Interval' + ': ' + str(sample_rate) +  '-->' + str(dev_type))
            view.press('Threshold')
            view.edit_texts[0].press()
            view.keyevent(threshold)
            view.press('Save')
            self.assertTrue(view.has_text(str(threshold)),'Set Threshold' + ': ' + str(threshold) +  '-->' + str(dev_type))
            view = self.get_view()
            if dev_type == 'Photocell':
                view.press('Device type')
                view.press('Photocell')
                dev_mux_1(AI_1 = 'Analog')
                time.sleep(1)
                set_voltage(ch1_voltage = '10', ch2_voltage = '0')
            adc_values = Serial_Commands.check_ADC_values()
            print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
            time.sleep(1)
            ADC_max_1 = adc_values[0]
            view.press('Hardware Maximum')
            view.edit_texts[0].press()
            view.keyevent(ADC_max_1)
            view.press('Save')
            self.assertTrue(view.has_text(str(ADC_max_1)), 'AI_1 HW Maximum Assigned' + ': ' + str(ADC_max_1) + '-->' + str(dev_type))
        if AInum == 2:
            view = self.get_view()
            view.press('Settings for analog input 2')
            view.press('Sample interval')
            view.edit_texts[0].press()
            view.keyevent(sample_rate)
            view.press('Save')
            self.assertTrue(view.has_text(str(sample_rate)),'Set Sampling Interval' + ': ' + str(sample_rate) +  '-->' + str(dev_type))
            view.press('Threshold')
            view.edit_texts[0].press()
            view.keyevent(threshold)
            view.press('Save')
            self.assertTrue(view.has_text(str(threshold)),'Set Threshold' + ': ' + str(threshold) +  '-->' + str(dev_type))
            view = self.get_view()
            if dev_type == 'Photocell':
                view.press('Device type')
                view.press('Photocell')
                dev_mux_2(AI_2 = 'Analog')
                time.sleep(1)
                set_voltage(ch1_voltage = '0', ch2_voltage = '10')
            adc_values = Serial_Commands.check_ADC_values()
            print(adc_values) #check_ADC_values returns a tuple of values and variables are assigned in sequence
            time.sleep(1)
            ADC_max_2 = adc_values[1]
            view.press('Hardware Maximum')
            view.edit_texts[0].press()
            view.keyevent(ADC_max_2)
            view.press('Save')
            self.assertTrue(view.has_text(str(ADC_max_2)), 'AI_2 HW Maximum Assigned' + ': ' + str(ADC_max_2) + '-->' + str(dev_type))


    def api_test_script_1(self):
        dev_mux_1(AI_1 = 'Digital')
        dev_mux_2(AI_2 = 'Analog')
        set_voltage(ch1_voltage = '0', ch2_voltage = '1')
        time.sleep(5)
        set_voltage(ch1_voltage = '0', ch2_voltage = '2')
        time.sleep(3)
        set_voltage(ch1_voltage = '0', ch2_voltage = '3')
        time.sleep(5)
        set_voltage(ch1_voltage = '0', ch2_voltage = '4')
        time.sleep(10)
        set_voltage(ch1_voltage = '0', ch2_voltage = '5')
        time.sleep(20)
        set_voltage(ch1_voltage = '0', ch2_voltage = '6')
        time.sleep(2)
        set_voltage(ch1_voltage = '0', ch2_voltage = '7')
        time.sleep(2)
        set_voltage(ch1_voltage = '0', ch2_voltage = '8')
        time.sleep(10)
        set_voltage(ch1_voltage = '0', ch2_voltage = '9')
        time.sleep(5)
        set_voltage(ch1_voltage = '0', ch2_voltage = '10')
        time.sleep(10)
        set_voltage(ch1_voltage = '0', ch2_voltage = '0')
        time.sleep(2)
        switch_state(SW1 = 'on')
        time.sleep(2)
        switch_state(SW1 = 'off')



    def occ_behavior_test(self):
        self.device_message([0, 15, 1, 2, 7], 67076191)

    def run(self):
        self.print_intro()
        view = self.get_view()
        devnum_1 = 66
        devnum_2 = 65535
        self.homescreen(back_to_settings = False)
        self.default_device_states()
        #self.calibrate_analog_device(AInum = 1, dev_type = 'Photocell', sample_rate = 150, threshold = 5)
        #self.calibrate_digital_device(AInum = 2, dev_type = 'Switch', sample_rate = 150, threshold = 5)
        with self.messages_buffering():
            self.api_test_script_1()
        self.can_check(button = 'Setting Photocell to 1V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 10)
        self.can_check(button = 'Checking for Channel 1 to 100%', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, channel_number = 1, target_level = 100)
        self.can_check(button = 'Checking for 1V Virtual Input Publish', message_type = 81, input_device_type = 3, input_device_number = 57, input_device_status = 0, input_device_state = 50, input_device_scaler = 2)
        self.can_check(button = 'Checking for Channel Query', message_type = 49, channel_level_with_fade_flag = 0, channel_number = 1)
        self.can_check(button = 'Checking for Channel Reply', message_type = 50, channel_number = 1, hw_level = 100)

        self.can_check(button = 'Setting Photocell to 2V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 20)
        self.can_check(button = 'Checking for Channel 5 to 25%', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 3, channel_number = 5, target_level = 25 )
        self.can_check(button = 'Checking for 2V Virtual Input Publish', message_type = 81, input_device_type = 3, input_device_number = 56, input_device_status = 0, input_device_state = 255, input_device_scaler = 100)
        self.can_check(button = 'Checking for Channel Query', message_type = 49, channel_level_with_fade_flag = 0, channel_number = 5)
        self.can_check(button = 'Checking for Channel Reply', message_type = 50, channel_number = 5, hw_level = 25)

        self.can_check(button = 'Setting Photocell to 3V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 30)
        self.can_check(button = 'Checking for Channel 6 to 50%', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 5, channel_number = 6, target_level = 50 )
        self.can_check(button = 'Checking for 3V Virtual Input Publish', message_type = 81, input_device_type = 1, input_device_number = 42, input_device_status = 0, input_device_state = 0, input_device_scaler = 0)
        self.can_check(button = 'Checking for Channel Query', message_type = 49, channel_level_with_fade_flag = 0, channel_number = 6)
        self.can_check(button = 'Checking for Channel Reply', message_type = 50, channel_number = 6, hw_level = 50)

        self.can_check(button = 'Setting Photocell to 4V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 40)
        self.can_check(button = 'Checking for Channel 7 to 75%', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 10, channel_number = 7, target_level = 75 )
        self.can_check(button = 'Checking for 4V Virtual Input Publish', message_type = 81, input_device_type = 2, input_device_number = 9, input_device_status = 0, input_device_state = 255, input_device_scaler = 0)
        self.can_check(button = 'Checking for Channel Query', message_type = 49, channel_level_with_fade_flag = 0, channel_number = 7)
        self.can_check(button = 'Checking for Channel Reply', message_type = 50, channel_number = 7, hw_level = 75)

        self.can_check(button = 'Setting Photocell to 5V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 50)
        self.can_check(button = 'Checking for Channel 8 to 100%', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 20, channel_number = 8, target_level = 100 )
        self.can_check(button = 'Checking for 5V Virtual Input Publish', message_type = 81, input_device_type = 4, input_device_number = 88, input_device_status = 0, input_device_state = 255, input_device_scaler = 0)
        self.can_check(button = 'Checking for Channel Query', message_type = 49, channel_level_with_fade_flag = 0, channel_number = 8)
        self.can_check(button = 'Checking for Channel Reply', message_type = 50, channel_number = 8, hw_level = 100)

        self.can_check(button = 'Setting Photocell to 6V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 60)
        for cnum in ([1, 2, 3, 4]):
            self.can_check(button = 'Checking for Channel {} to 100%'.format(cnum), message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = 100 )
            self.can_check(button = 'Checking for Channel {} Query'.format(cnum), message_type = 49, channel_level_with_fade_flag = 0, channel_number = cnum)
            self.can_check(button = 'Checking for Channel {} Reply'.format(cnum), message_type = 50, channel_number = cnum, hw_level = 100)
        self.can_check(button = 'Checking for 6V Virtual Input Publish', message_type = 81, input_device_type = 5, input_device_number = 125, input_device_status = 0, input_device_state = 255)

        self.can_check(button = 'Setting Photocell to 7V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 70)
        for cnum in ([1, 2, 3, 4]):
            self.can_check(button = 'Checking for Channel {} to 0%'.format(cnum), message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = 0 )
            self.can_check(button = 'Checking for Channel {} Query'.format(cnum), message_type = 49, channel_level_with_fade_flag = 0, channel_number = cnum)
            self.can_check(button = 'Checking for Channel {} Reply'.format(cnum), message_type = 50, channel_number = cnum, hw_level = 0)

        self.can_check(button = 'Setting Photocell to 8V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 80)
        for cnum, target_level in zip([14, 15, 16], [100, 50, 75]):
            self.can_check(button = 'Checking for Channel {} to 0%'.format(cnum), message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 10, channel_number = cnum, target_level = target_level )
            self.can_check(button = 'Checking for Channel {} Query'.format(cnum), message_type = 49, channel_level_with_fade_flag = 0, channel_number = cnum)
            self.can_check(button = 'Checking for Channel {} Reply'.format(cnum), message_type = 50, channel_number = cnum, hw_level = target_level)

        self.can_check(button = 'Setting Photocell to 9V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 90)
        self.can_check(button = 'Checking for Channel 17 to 100%', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, priority = 3, fade_rate = 1, channel_number = 17, target_level = 100 )

        self.can_check(button = 'Setting Photocell to 10V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 100)
        self.can_check(button = 'Checking for Channel 18 to 100%', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, priority = 14, fade_rate = 1, channel_number = 18, target_level = 100 )

        self.can_check(button = 'Setting Photocell to 0V', message_type = 81, input_device_type = 3, input_device_number = devnum_2, input_device_status = 0, input_device_state = 0)
        for cnum in ([1, 2, 3, 4, 5, 6, 7, 8,9, 10, 11, 12, 13, 14, 15, 16]):
            self.can_check(button = 'Checking for Channel {} to 0%'.format(cnum), message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = 0 )
            self.can_check(button = 'Checking for Channel {} Query'.format(cnum), message_type = 49, channel_level_with_fade_flag = 0, channel_number = cnum)
            self.can_check(button = 'Checking for Channel {} Reply'.format(cnum), message_type = 50, channel_number = cnum, hw_level = 0)

        self.can_check(button = 'Setting Sensor: Occupied', message_type = 81, input_device_type = 2, input_device_number = devnum_1, input_device_status = 0, input_device_state = 100)
        for cnum, target_level in zip([17, 18, 19, 20, 21], [20, 40, 60, 80, 100]):
            self.can_check(button = 'Checking for Channel {} to 0%'.format(cnum), message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = target_level )
            self.can_check(button = 'Checking for Channel {} Query'.format(cnum), message_type = 49, channel_level_with_fade_flag = 0, channel_number = cnum)
            self.can_check(button = 'Checking for Channel {} Reply'.format(cnum), message_type = 50, channel_number = cnum, hw_level = target_level)

        self.can_check(button = 'Setting Sensor: Un-Occupied', message_type = 81, input_device_type = 2, input_device_number = devnum_1, input_device_status = 0, input_device_state = 0)
        for cnum, target_level in zip([17, 18, 19, 20, 21], [0, 0, 0, 0, 0]):
            self.can_check(button = 'Checking for Channel {} to 0%'.format(cnum), message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, channel_number = cnum, target_level = target_level )
            self.can_check(button = 'Checking for Channel {} Query'.format(cnum), message_type = 49, channel_level_with_fade_flag = 0, channel_number = cnum)
            self.can_check(button = 'Checking for Channel {} Reply'.format(cnum), message_type = 50, channel_number = cnum, hw_level = target_level)
