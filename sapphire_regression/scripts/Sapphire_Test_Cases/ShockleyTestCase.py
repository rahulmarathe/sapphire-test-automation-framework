import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View




class ShockleyTestCase(sapphire.SapphireTestCase):
    """Defines the Shockley tab test case.

    Test Section: Sapphire Application Testing (Shockley tab)
    Regression Plan: 2_10 Oct 10 Build Regression Test.xlsx
    Updated: 12/12/2016

    TODO: Implement screenshot checking for button light up

    Except for TODO items, test confirmed in compliance with Regression Plan
    by Bobby Eshleman

    """

    tab = 'Shockley'
    tab_position = 'left'

    def run(self):
        self.print_intro()
        print('ds_toggle_relay(8)')
        ds_toggle_relay(8)
        time.sleep(15)

        self.test_relay_result(button='Shock A', relays=[5, 6], levels=[20, 80], wait_time=5)
        for cnum, target_level in zip([5, 6], [20, 80]):
            self.can_check(button = 'Shock A', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Shock A', relays=[5, 6], levels=[0, 0], wait_time=5)
        for cnum, target_level in zip([5, 6], [0, 0]):
            self.can_check(button = 'Shock A', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Shock B', relays=[7, 8], levels=[30, 100])
        for cnum, target_level in zip([7, 8], [30, 100]):
            self.can_check(button = 'Shock B', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Shock B', relays=[7, 8], levels=[10, 10])
        for cnum, target_level in zip([7, 8], [10, 10]):
            self.can_check(button = 'Shock B', message_type = 45, fade_time_or_rate = 1, fade_rate = 5, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Shock C', relays=[5, 6, 7, 8], levels=[20, 30, 60, 80], wait_time=15)
        for cnum,target_level in zip([5, 6, 7, 8], [20, 30, 60, 80]):
            self.can_check(button = 'Shock C', message_type = 45, fade_time_or_rate = 0, fade_rate = 15, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Shock C', relays=[5, 6, 7, 8], levels=[0, 0, 0, 0], wait_time=15)
        for cnum,target_level in zip([5, 6, 7, 8], [0, 0, 0, 0]):
            self.can_check(button = 'Shock C', message_type = 45, fade_time_or_rate = 0, fade_rate = 15, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Shock D', relays=[1, 2, 3], levels=[1, 1, 1], binary=True)
        for cnum,target_level in zip([1, 2, 3], [100, 100, 100]):
            self.can_check(button = 'Shock D', message_type = 45, fade_time_or_rate = 1, fade_rate = 3, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Shock D', relays=[1, 2, 3], levels=[0, 0, 0], binary=True)
        for cnum,target_level in zip([1, 2, 3], [0, 0, 0]):
            self.can_check(button = 'Shock D', message_type = 45, fade_time_or_rate = 1, fade_rate = 3, channel_number = cnum, target_level = target_level)

        self.test_relay_result(button='Shock E', relays=[5, 6], levels=[100, 100], wait_time=10)
        #self.test_relay_result(button='Shock E', relays=[1, 2, 3], levels=[1, 1, 1], binary=True)
        relays = binary_state()
        self.assertTrue(relays[1] == 1, 'Button Shock E turns relay 1 to state 1')
        self.assertTrue(relays[2] == 1, 'Button Shock E turns relay 2 to state 1')
        self.assertTrue(relays[3] == 1, 'Button Shock E turns relay 3 to state 1')
        for cnum,target_level in zip([1, 2, 3, 5 ,6], [100, 100, 100, 100, 100]):
            self.can_check(button = 'Shock E', message_type = 45, fade_time_or_rate = 0, fade_rate = 10, channel_number = cnum, target_level = target_level)

        time.sleep(5)
        self.test_relay_result(button='Shock E', relays=[5, 6], levels=[0, 0], wait_time=10)
        #self.test_relay_result(button='Shock E', relays=[1, 2, 3], levels=[0, 0, 0], binary=True)
        relays = binary_state()
        self.assertTrue(relays[1] == 0, 'Button Shock E turns relay 1 to state 0')
        self.assertTrue(relays[2] == 0, 'Button Shock E turns relay 2 to state 0')
        self.assertTrue(relays[2] == 0, 'Button Shock E turns relay 3 to state 0')
        for cnum,target_level in zip([1, 2, 3, 5, 6], [0, 0, 0, 0, 0]):
            self.can_check(button = 'Shock E', message_type = 45, fade_time_or_rate = 0, fade_rate = 10, channel_number = cnum, target_level = target_level)
        #time.sleep(10)
