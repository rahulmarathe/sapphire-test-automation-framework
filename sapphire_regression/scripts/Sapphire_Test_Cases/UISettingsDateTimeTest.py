import time
from context import leviton_test
import datetime
import calendar
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View



class UISettingsDateTimeTestCase(sapphire.SapphireTestCase):

    def homescreen(self, back_to_settings = False):
        #gets the view back to the homescreen
        view = self.get_view()
        if back_to_settings == False:
            if not view.settings_button:
                while not view.has_text('Exit'):
                    view.press('Done')
                view.press('Exit')
        if back_to_settings == True:
            if not view.settings_button:
                while not view.has_text('Exit'):
                    view.press('Done')
                view.press('Exit')
            view.press_settings()
            if 'Sign out' not in view.text_elements:
                view.press('Sign in')
                view.edit_texts[1].press()
                view.keyevent('5625')
                view.press('OK')


    def device_message(self, command, header):
        canparser.broadcast_msg(self.can_channel, command, header)
        #canparser.tail_kvaser_channel(channel, 1)

    def run(self):

        self.print_intro()
        self.homescreen(back_to_settings = True)
        view = self.get_view()
        view.press('Date/Time Settings')
        # 18-21
        now = datetime.datetime.now()
        view.press('Set date')
        view.edit_texts[0].press()
        month = now.month
        month_name = calendar.month_name[month]
        view.keyevent(calendar.month_abbr[month])
        view.keyevent('tab')
        view.keyevent(now.day)
        view.keyevent('tab')
        view.keyevent(now.year)
        view.keyevent('enter')
        view.press('Done')
        self.assertTrue(view.has_text(str(month)), '{} is on screen'.format(month_name))
        self.assertTrue(view.has_text(str(now.day)), '{} is on screen'.format(now.day))
        self.assertTrue(view.has_text(str(now.year)), '{} is on screen'.format(now.year))
        view.press('Set time')
        view.edit_texts[0].press()  # The first editable text field on the screen
        view.keyevent(now.hour)
        view.keyevent('tab')
        view.keyevent(now.minute)
        hour = str(now.hour).zfill(2)
        minute = str(now.minute).zfill(2)
        time = str(hour + ':' + minute)
        #print(time)
        d24 = datetime.datetime.strptime(time, "%H:%M")
        d12 = str(d24.strftime("%I:%M  %p"))
        #print(d12)
        view.keyevent('enter')
        view.press('Done')
        #self.assertTrue(view.has_text(time), '{} is on screen'.format(time))
        #view.press('Use 24 hr Format')
        self.assertTrue(view.has_text(d12), '{} is on screen'.format(d12))
        view.press('Use 24 hr Format')
        view.press('Select time zone')
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('US/Pacific')
        self.assertTrue(view.has_text('US/Pacific'), 'US/Pacific Time zone is on screen')
        view.press('Get Sunrise/Sunset Time')
        view.type(45.9, 0)
        view.type(-118.8, 1)
        view.press('Ok')
        srise = sunrise(lat=45.9, lng=-118.8, year=now.year, month=now.month, day=now.day)
        sset = sunset(lat=45.9, lng=-118.8, year=now.year, month=now.month, day=now.day)
        sunrise_correct = view.has_time(srise, deviation=1)  # 1hr deviation
        sunset_correct = view.has_time(sset, deviation=1)  # 1hr deviation
        self.assertTrue(sunrise_correct, 'Has correct sunrise time')
        self.assertTrue(sunset_correct, 'Has correct sunset time')

        view.press('options')
        view.press('Save')


        view.press('Export Logs')

        if view.has_text('USB Drive is unwriteable'):
            self.assertFalse(view.has_text('USB Drive is unwriteable'), 'Export Logs Warning: No USB drive present on system, test will need to be performed manualy')
            view.press('OK')
        else:
            self.assertTrue(True, 'Log export successful')
        view.refresh()
        time.sleep(6)
        view.press('Export Configuration')
        view.press('Ok')

        if view.has_text('USB Drive is unwriteable'):
            self.assertFalse(view.has_text('USB Drive is unwriteable'), 'Export Configuration Warning: No USB drive present on system, test will need to be performed manualy')
            view.press('OK')
        else:
            self.assertTrue(True, 'Configuration export successful')

        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Network Settings')
        self.assertTrue(view.has_text('Connected'), 'Sapphire is connected to a network')
        view.press('IP Address')
        view.keyevent('192.168.2.220')
        view.press('Save')
        self.assertTrue(view.has_text('192.168.2.220'), 'Sapphire has the correct static IP address')
        view.press('Gateway')
        view.keyevent('192.168.2.1')
        view.press('Save')
        self.assertTrue(view.has_text('192.168.2.1'), 'Sapphire has the correct Gatway Address')
        view.press('Network Prefix Length')
        view.keyevent('24')
        view.press('Save')
        self.assertTrue(view.has_text('24 (255.255.255.0)'), 'Sapphire has the correct Subnet Mask')
        view.press('Done')

        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('Set LumaCAN Node ID')
        view.keyevent ('enter')
        view.keyevent ('256')
        view.press('Save')
        self.assertTrue(view.has_text('Node ID:256 is not in the valid range. Please select a Node ID in the range of 1-249'), 'Node ID:256 is not in valid range.')
        view.press('OK')
        view.keyevent ('enter')
        view.keyevent ('56')
        view.press('Save')
        self.device_message([2, 0, 1], 402624031) # sends conflicting LC node ID 56
        view.refresh()
        time.sleep(1)
        self.assertTrue(view.has_text('In Conflict!'), 'Node ID conflict Check')
        self.device_message([2, 0, 1], 402623135) # sends different node ID 42
        view.press('Set LumaCAN Node ID: In Conflict!')
        view.press('Save')
        #print('Waiting 90sec for Node ID conflict to clear')
        #time.sleep(90)
        view.refresh()
        self.assertTrue(not view.has_text('In Conflict!'), 'Node ID no longer in conflict Check')

        view.swipe(300, 400, 300, 100)
        view.press('Project Settings')
        texts = [
            'Project Settings',
            'Project Number:',
            'Project Name:',
            'New Sapphire Studio Project',
            'Contact Name:',
            'Contact Number:',
            'This Station:',
        ]
        for text in texts:
            result = view.has_text(text)
            self.assertTrue(result, text + ' is on screen')

        view.edit_texts[0].press()
        view.keyevent('Foo')
        view.edit_texts[1].press()
        view.keyevent('123-123-1234')
        view.keyevent('enter')
        view.press('options')
        view.press('Save')
        view.swipe(300, 400, 300, 100)
        view.press('Project Settings')
        self.assertTrue(view.has_text('Foo'), 'Foo is on screen')
        self.assertTrue(view.has_text('123-123-1234'), '123-123-1234 is on screen')
        for idx, btn in enumerate(view.buttons):
            if btn.name == 'Initial Page:':
                break
        idx += 1
        initial_page = view.buttons[idx]
        view.press(initial_page)
        view.swipe(450, 300, 450, 104)
        view.press('Newton')
        view.press('options')
        view.press('Save')
        view.refresh()
        # SLeep here because sometimes there is a long lag after pressing "Save"
        time.sleep(1)
        #view.press('options') commented out because new code removes options button and has "Sign Out" and "Exit" as explicit buttons
        view.press('Exit')
        print("Waiting for sapphire to load...")
        time.sleep(4) # takes a long time to load
        view.refresh() # Must refresh after long load, because the autorefresh inside press() happens too early
        texts = ['A1', 'A2', 'B1', 'B2', 'C1', 'C2']#corrected this to look for correct buttons in this check --SM
        self.assertTrue(view.has_texts(texts), 'Texts {} on the screen'.format(', '.join(texts)))

        view.wakeup()
        self.change_brightness(view, minutes=1, level=0)
        self.wait(seconds=65)
        brightness = view.get_brightness()
        self.assertTrue(brightness == 0, 'Backlight timer turned brightness to 0 after 1 minutes')
        view.wakeup()
        self.change_brightness(view, level=20)
        brightness = view.get_brightness()
        self.wait(seconds=65)
        new_brightness = view.get_brightness()
        self.assertTrue(new_brightness <  brightness, 'Brightness lowered after 1 minute')

        # Brightness out of range
        view.wakeup()
        view.press('settings', seconds=2)
        view.press('Backlight/Idle Timer')
        view.edit_texts[1].press()
        view.keyevent('95')
        view.press('Save')
        self.assertTrue(view.has_text('Error'), 'Displayed Error message for out-of-range backlight value 100')
        view.press('OK')

        # Brightness time out of range
        view.edit_texts[0].press()
        view.keyevent('500')
        view.press('Save')
        result = view.has_text('Error')
        self.assertTrue(result, 'Displayed Error message for out-of-range backlight time 500')
        view.press('OK')
        view.press('Save')
        view.press('Exit')

        # Check UI for each tab
        view.press('Newton')
        texts = ['A1', 'A2', 'B1', 'B2', 'C1', 'C2']
        self.assertTrue(view.has_texts(texts), 'Screen has {}'.format(texts))
        view.press('Ohm')
        texts = [
            'D1-FT',
            'E1-FR',
            'D2-FT',
            'F1-FT',
            'E2-FR',
            'G1-Off',
            'F2-FT',
        ]
        self.assertTrue(view.has_texts(texts), 'Screen has {}'.format(texts))

        assert_texts = lambda texts: self.assertTrue(view.has_texts(texts), 'Screen has {}'.format(texts))
        view.press('Tesla')
        texts = ['A', 'B', 'C', 'D']
        assert_texts(texts)
        view.press('Shockley')
        texts = ['Shock {}'.format(x) for x in ['A', 'B', 'C', 'D', 'E']]
        assert_texts(texts)
        view.press('Babbage')
        self.assertTrue(len(view.sliders) == 6, 'Babbage has 6 sliders')
        for s in view.sliders:
            self.assertTrue(s.name is not None, 'Babbage slider ' + s.name + ' is labeled')

        view.scroll_menu_left()
        view.press('Edison')
        texts = [
            'A:On',
            'A:Raise',
            'B:On',
            'A:Lower',
            'B:Raise',
            'A:Off',
            'B:Lower',
            'B:Off',
        ]
        assert_texts(texts)
        view.scroll_menu_left()
        view.press('Faraday')
        self.assertTrue(len(view.sliders) == 3, 'Faraday has 3 sliders')
        texts = ['B1 GWY', 'B2 GWY', 'B2 GWY GMX']
        assert_texts(texts)
        # Change brightness time so that rest of test is unaffected by Sapphire idling
        self.change_brightness(view, minutes=35)

    def change_brightness(self, view, minutes=None, level=None,):
        view.press('settings', seconds=2)
        view.press('Backlight/Idle Timer')
        if minutes != None:
                view.edit_texts[0].press()
                view.keyevent(str(minutes))
        if level != None:
            view.edit_texts[1].press()
            view.keyevent(str(level))
        view.press('Save')
        view.press('Exit')

    def wait(self, seconds, interval=5):
        print('Waiting to check backlight timing...')
        for i in range(0, seconds, interval):
            print('Waiting {} more seconds'.format(seconds-i))
            time.sleep(interval)
