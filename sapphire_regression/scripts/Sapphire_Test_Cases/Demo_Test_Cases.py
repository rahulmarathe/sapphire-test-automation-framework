import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View




class DemoTest1(sapphire.SapphireTestCase):

    def device_message(self, command, header):
        canparser.broadcast_msg(self.can_channel, command, header)
        #canparser.tail_kvaser_channel(channel, 1)

    def run(self):

        self.print_intro()
        view = self.get_view()
        if not view.settings_button:
            view.press('Exit')
        view.press_settings()
        if 'Sign out' not in view.text_elements:
            view.press('Sign in')
            view.edit_texts[1].press()
            view.keyevent('5625')
            view.press('OK')

        view.press('Date/Time Settings')
        # 18-21
        view.press('Set date')
        view.edit_texts[0].press()
        view.keyevent('Oct')
        view.keyevent('tab')
        view.keyevent('20')
        view.keyevent('tab')
        view.keyevent('2015')
        view.keyevent('enter')
        view.press('Done')
        self.assertTrue(view.has_text('Oct'), 'Oct is on screen')
        self.assertTrue(view.has_text('20'), '20 is on screen')
        self.assertTrue(view.has_text('2015'), '2015 is on screen')
        view.press('Set time')
        view.edit_texts[0].press()  # The first editable text field on the screen
        view.keyevent('10')
        view.keyevent('tab')
        view.keyevent('30')
        view.keyevent('tab')
        view.keyevent('AM')
        view.press('Done')
        self.assertTrue(view.has_text('10:30  AM'), '10:30  AM is on screen')
        view.press('Select time zone')
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=False)
        view.swipe(300, 400, 300, 100, refresh=True)
        view.press('US/Pacific')
        self.assertTrue(view.has_text('US/Pacific'), 'US/Pacific Time zone is on screen')
        view.press('options')
        view.press('Save')
        view.press('Exit')

class DemoTest2(sapphire.SapphireTestCase):

    tab = 'Kepler'
    tab_position = 'right'

    def run(self):
        self.print_intro()
        ds_toggle_relay(8)
        self.test_relay_result(button='All On', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[1, 1, 1, 1, 1, 1, 1, 1], binary=True)
        self.test_relay_result(button='All On', relays=[5, 6, 7, 8, 13, 14, 15, 16], levels=[100, 50, 100, 75, 100, 10, 100, 100])
        self.test_relay_result(button='All Off', relays=[1, 2, 3, 4, 9, 10, 11, 12], levels=[0, 0, 0, 0, 0, 0, 0, 0], binary=True)
        self.test_relay_result(button='All Off', relays=[5, 6, 7, 8, 13, 14, 15, 16], levels=[0, 0, 0, 0, 0, 0, 0, 0])


class DemoTest3(sapphire.SapphireTestCase):
    tab = 'Babbage'
    tab_position = 'left'

    def run(self):
        self.print_intro()
        ds_toggle_relay(8)
        self.test_relay_result(slider='Babb 13', slider_level=100, relays=[13], levels=[40])
        self.recent_message_batch_has(msg_type = 'Channel Fade', target_level = 40, channel = 13, description = 'Slider Babb 13, Channel Fade Command, channel 13, target level 40%')
        self.test_relay_result(slider='Babb 14', slider_level=100, relays=[14], levels=[50])
