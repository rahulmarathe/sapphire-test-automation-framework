import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View




class EdisonTestCase(sapphire.SapphireTestCase):
    tab = 'Edison'
    tab_position = 'right'

    def run(self):
        self.print_intro()

        ds_toggle_relay(8)
        self.test_relay_result(button='A:On', relays=[14], levels=[100])
        self.can_check(button = 'A:On', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, channel_number = 14, target_level = 100)

        with self.messages_buffering():
            self.test_relay_increment(button='A:Lower', relays=[14], increments=[-.10])
        #rel fade times are encoded as a 2's compliment on the bus. So the check will be for the non 2's compliment representation of the value. So -10 will be checked as 246
        self.can_check(button = 'A:Lower', message_type = 45, rel_abs_fade = 1, fade_time_or_rate = 0, fade_rate = 1, channel_number = 14, target_level = 96)

        with self.messages_buffering():
            self.test_relay_increment(button='A:Raise', relays=[14], increments=[.05])
        self.can_check(button = 'A:Raise', message_type = 45, rel_abs_fade = 1, fade_time_or_rate = 0, fade_rate = 1, channel_number = 14, target_level = 2)

        self.test_relay_result(button='A:Off', relays=[14], levels=[0])
        self.can_check(button = 'A:Off', message_type = 45, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, channel_number = 14, target_level = 0)

        self.test_relay_result(button='B:On', relays=[14, 15, 16], levels=[100, 100, 100])
        for grp in [3,6]: #need linked fraqmes for target number
            self.can_check(button = 'B:On', message_type = 65, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, group_number = grp)

        self.test_relay_increment(button='B:Lower', relays=[14, 15, 16], increments=[-.10, -.10, -.10])
        for grp in [3,6]: #need linked fraqmes for target number
            self.can_check(button = 'B:On', message_type = 65, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, group_number = grp)

        self.test_relay_increment(button='B:Raise', relays=[14, 15, 16], increments=[.05, .05, .05])
        for grp in [3,6]: #need linked fraqmes for target number
            self.can_check(button = 'B:On', message_type = 65, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, group_number = grp)

        self.test_relay_result(button='B:Off', relays=[14, 15, 16], levels=[0, 0, 0])
        for grp in [3,6]: #need linked fraqmes for target number
            self.can_check(button = 'B:On', message_type = 65, rel_abs_fade = 0, fade_time_or_rate = 0, fade_rate = 1, group_number = grp)
        
