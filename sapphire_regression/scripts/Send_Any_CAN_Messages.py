import time as timer
import argparse
import textwrap

from itertools import cycle, groupby
from collections import OrderedDict
from canlib import canlib
from context import leviton_test
from leviton_test.commands import vector_to_commands_dictionary, button_list
from leviton_test.canparser import (
    str_from_msg,
    set_up_channel,
    tear_down_channel,
    tail_kvaser_channel,
    broadcast_msg,
    command_type,
    channel_number,
    target_level,
)
from leviton_test.view import View


def main():
    print('Fade Command Interface')
    #command = [2, 0, 1]
    #command = [0, 81, 2, 0, 45, 23]
    #command = [0, 81, 2, 0, 255, 245, 50, 0]
    #command = [0, 80, 3, 0, 66, 0] #device satus query for device 66
    #command = [0, 15, 4, 1, 4]#behavior
    #command = [0, 11, 1, 19, 17, 0]
    #command = [0, 1, 0, 0, 0, 0, 0, 0]
    header = 67076191
    command = [0, 45, 1, 112, 10, 157, 255, 158, 125, 159, 255, 160, 255, 161, 255]
    #node ID of 23
    #header = 402622047  #node ID 25
    #header = 402621919 #node ID 23
    #command = [2, 45, 2, 112, 10, 128, 255]
    #header = 402623135	#node ID 42
    #header = 402624031	#node ID 56
    device_message(command, header)


def device_message(command, header):
    channel = set_up_channel(0)
    print(command)
    broadcast_msg(channel, command, header)
    flush_channel(channel, 1)
    tear_down_channel(channel)


def flush_channel(channel, seconds):
    for (_, _, _, _, _) in tail_kvaser_channel(channel, timeout=seconds):
        pass

if __name__ == '__main__':
    main()
