import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View
from Sapphire_Test_Cases.UISettingsDateTimeTest import UISettingsDateTimeTestCase #From thie test case file, Import the class
from Sapphire_Test_Cases.NewtonTestCase import NewtonTestCase
from Sapphire_Test_Cases.KeplerTestCase import KeplerTestCase
from Sapphire_Test_Cases.TeslaTestCase import TeslaTestCase
from Sapphire_Test_Cases.ShockleyTestCase import ShockleyTestCase
from Sapphire_Test_Cases.BabbageTestCase import BabbageTestCase
from Sapphire_Test_Cases.KeplerTestCase import KeplerTestCase
from Sapphire_Test_Cases.FaradayTestCase import FaradayTestCase
from Sapphire_Test_Cases.EditModeTestCase import EditModeTestCase
from Sapphire_Test_Cases.EdisonTestCase import EdisonTestCase
from Sapphire_Test_Cases.CurieTestCase import CurieTestCase
from Sapphire_Test_Cases.Demo_Test_Cases import DemoTest1
from Sapphire_Test_Cases.Demo_Test_Cases import DemoTest2
from Sapphire_Test_Cases.Demo_Test_Cases import DemoTest3
from Sapphire_Test_Cases.AITesting import AITestCase
from Sapphire_Test_Cases.General_Testing import GeneralTest
from Sapphire_Test_Cases.API_excercise import APIexcercise
from Sapphire_Test_Cases.API_Stress import APIstress


if __name__ == '__main__':
    sapphire.main(only=[UISettingsDateTimeTestCase], repeat_failures=0)
    #sapphire.main(only = [CurieTestCase], repeat_failures = 0)
    #sapphire.main(only=[DemoTest2, DemoTest3], repeat_failures=1)
    #sapphire.main(exclude=[DemoTest1, DemoTest2, DemoTest3])
    #sapphire.main(only = [UISettingsDateTimeTestCase], repeat_failures = 0)
    #sapphire.main(only=[UISettingsDateTimeTestCase, NewtonTestCase, TeslaTestCase, ShockleyTestCase, BabbageTestCase, EdisonTestCase, KeplerTestCase, EditModeTestCase], repeat_failures = 1)
    #sapphire.main(only=[AITestCase], repeat_failures = 1)
    #sapphire.main(only=[APIexcercise], repeat_failures = 0)
    #sapphire.main(only=[APIstress], repeat_failures = 0)
    #For single test, put only=testname in the argument
