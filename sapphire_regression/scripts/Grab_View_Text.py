
from context import leviton_test
from leviton_test import sapphire
from leviton_test.view import View
from leviton_test import canparser
from leviton_test.ds import ds_toggle_relay
import time
import os
import subprocess
from subprocess import Popen, CREATE_NEW_CONSOLE


'''
#Start-Process -FilePath "powershell.exe" "& {adb logcat}"
timestr = time.strftime("%Y%m%d")
LOGCAT_FILENAME = os.path.join(os.getcwd(), 'logs', 'logcat.rst')
print(LOGCAT_FILENAME)
save_file = LOGCAT_FILENAME + "_" + timestr
def logcat():
#opens a powershell instance that connects to the android LOGCAT. This is saved to a file by powershell as the test is running
    subprocess.Popen(['powershell.exe', "& {adb logcat -c *:V}"], creationflags=CREATE_NEW_CONSOLE) #clears ADB buffer
    time.sleep(1)
    subprocess.Popen(['powershell.exe', "& {adb logcat -v time *:D}", "| Out-File '{}'".format(save_file)], creationflags=CREATE_NEW_CONSOLE)
'''

def main():
    v = View()
    print(v)
    #logcat()
    #with open(LOGCAT_FILENAME, 'a') as f:
    #print(View.has_texts("Broadcast Message", "Enabled"))
if __name__ == '__main__':
    main()


#Start-Process -FilePath 'powershell.exe' "& {adb logcat -b all -c *:I}"
