# main.py
"""Populate databases with schedules for Sapphire.

Setup:
1. Sapphire configuration .zip in CURRENT DIRECTORY (note: Have only one .zip in the current directory)
2. Have A Sapphire DB in the CURRENT DIRECTORY

Use the --csv argument to point to the CSV file you are using to specify your csv file.


CSV Format
----------

    The CSV format is extremely particular and there are a few 'gotchas' to be aware of:

        1. There can be no empty rows between events, ONLY between event types.

            For example:

                // Bad

                weekly event
                name,command,time,days
                Event 1 - weekly, 1_on, 12:00, all
                                                        <---- This space will cause an error
                Event 2 - weekly, 1_on, 12:00, all


                special weekly event
                name,command,time,days
                event 3 - special weekly,3_on,12:00,all


                // Good

                weekly event
                name,command,time,days
                Event 1 - weekly, 1_on, 12:00, all
                Event 2 - weekly, 1_on, 12:00, all
                                                         <------ This space is okay because it is between event types
                special weekly event
                name,command,time,days
                event 3 - special weekly,3_on,12:00,all


        2. Every event must have every field filled out, no more or no less.
        3. The format must be exactly like the example CSV. (Use --example to output the example csv)
        4. Make sure that your dates are valid.  For example, April 31st will throw an error.

        
"""
import csv
import sqlite3
import argparse
import calendar
import os
from collections import namedtuple

import autoschedule

from context import leviton_test
from leviton_test.commands import command_in_bytes, commands_to_vector_dictionary, get_commands

Date = namedtuple('Date', ['day', 'nth_day_of_month', 'month'])

def format_time(time_str):
    if time_str[0:2] == '00':
        return time_str[1:]
    else:
        return time_str


def time_tuple(name):
    key = ''.join(name.lower().split())
    holidays = {
        'newyear': Date(1, 0, 0),
        'newyears': Date(1, 0, 0),
        'memorialday': Date(1, 5, 4),
        'independenceday': Date(4, 0, 6),
        'laborday': Date(1, 2, 8),
        'thanksgiving': Date(4, 4, 10),
        'christmas': Date(25, 0, 11),
    }
    try:
        day, day_of_week, month = holidays[key]
    except KeyError:
        raise KeyError('Holiday {} not supported.\nCurrently supported holidays: {}'.format(name, str(holidays.keys())))
    HolidayTime = namedtuple('HolidayTime', ['day', 'day_of_week', 'month'])
    return HolidayTime(day=day, day_of_week=day_of_week, month=month)


def offset_from_str(text):
    return {
        'pre-sunrise': 1,
        'post-sunrise': 2,
        'pre-sunset': 3,
        'post-sunset': 4,
    }.get(str(text).lower(), 0)


def time_from_hours(num):
    return '{}:00'.format(num)


def days_in_bytes(days):
    if type(days) is str and days.lower() == 'all':
        days = __alldays()
    if type(days) is str:
        days = [days]
    string = __byte_beginning(days)
    string += __day_bytes(days.pop(0))

    divider = b'~q\x00~\x00\x02t\x00'
    for day in days:
        string += divider + __day_bytes(day)
    return string


def __alldays():
    cal = calendar.Calendar()
    return [calendar.day_name[day] for day in cal.iterweekdays()]


def __byte_beginning(days):
    count = len(days).to_bytes(1, 'big')
    start = b'\xac\xed\x00\x05ur\x007[Lcom.leviton.app.sapphire.scheduler.CalendarEvent$Day;\xa2\x7f\xa0\xdebxsi\x02\x00\x00xp\x00\x00\x00'
    components = []
    components.append(start)
    components.append(count)
    components.append(b'~r')
    middle_part = b'\x004com.leviton.app.sapphire.scheduler.CalendarEvent$Day\x00\x00\x00\x00\x00\x00\x00\x00\x12\x00\x00xr\x00\x0ejava.lang.Enum\x00\x00\x00\x00\x00\x00\x00\x00\x12\x00\x00xpt\x00'
    components.append(middle_part)
    return b''.join(components)


def __day_bytes(day):
    day = day.upper()
    result = {
    	'WEDNESDAY': b'\tWEDNESDAY',
    	'SUNDAY': b'\x06SUNDAY',
    	'MONDAY': b'\x06MONDAY',
    	'THURSDAY': b'\x08THURSDAY',
    	'SATURDAY': b'\x08SATURDAY',
    	'TUESDAY': b'\x07TUESDAY',
    	'FRIDAY': b'\x06FRIDAY',
    }.get(day, None)
    if not result:
        raise Exception("Day not found")
    return result


def requests_from_csv(path_to_csv):
    """Reads in a csv and outputs a collection of request objects.

    A request is a dictionary, with a `request_type`, keys defined by column headers,
    and values from rows beneath those headers.

    For example, with a csv file such as:

    Item A
    name, day, month
    foo, 1, 12

    The request object will be:

    {
        'request_type': 'Item A',
        'name': 'foo',
        'day', '1',
        'month', '12'
    }

    Sections of types will be separated by spaces.
    """

    with open(path_to_csv, 'r', newline='') as f:
        assert os.path.exists(path_to_csv), path_to_csv + ' does not exist.'
        reader = csv.reader(f, dialect='excel')
        current_request_type = ''
        header = []
        request_list = []
        for row in reader:
            if DEBUG:
                print('line number: {} current_request_type {}, header {}, row {}'.format(reader.line_num, current_request_type, header, row))
            # Row is an empty row if all cols are just empty strings, or if the first char
            # in the string in the first column is '#'
            # We do this because header information (the little title in the csv file above the column info)
            # is determined by if a row is empty or not.
            row_is_empty = True
            for col in row:
                if col != '':
                    row_is_empty = False
                    break

            row_has_comment = True
            if row:
                try:
                    row_has_comment = row[0][0] == '#'
                except:
                    pass

            if row_is_empty or row_has_comment:
                row = []

            if not row:
                # If we have any empty row, we're not in a section of valid rows,
                # so reset the header and request type
                current_request_type = ''
                header = []
            # When you see the first column that is not '', save it as the current_request_type
            # This is because the first row that is not empty following an empty current_request_type
            # is the beginning of a new section
            elif row[0] is not '' and not current_request_type:
                current_request_type = row[0]
            # If we have a current_request_type section, but don't have a header, save the current
            # row as a header because headers MUST immediately follow the section name
            elif current_request_type and not header:
                header = row
            # If we have a header and a current_request_type, then start creating a request
            # and add the cols to the request body
            elif current_request_type and header:
                request = {'request_type': current_request_type}
                for idx, col in enumerate(row):
                    # get the key from the header list for the current_request_type column
                    try:
                        key = header[idx]
                    except IndexError as e:
                        message = (
                            "Attempted Index:{} Current Header:{} "
                            "Current Request Type: {} Line number:{}\nRow: {}\n"
                        ).format(idx, header, current_request_type, reader.line_num, row)
                        raise IndexError(message)
                    # Assign the key's value to the field in the column
                    request[key] = col
                request_list.append(request)
        return request_list


def event_constructor(request_type, name, time, command, day_bytes, priority, offset):
    return dict(
        table='event_tbl',
        request_type=request_type,
        event_name=name,
        event_time=time,
        event_command=command,
        event_repeat_days=day_bytes,
        event_priority=priority,
        event_offset=offset
    )


def special_constructor(request_type, name, time, month, day, weekDay, is_weekly, command, day_bytes, offset):
    return dict(
            table='special_tbl',
           request_type=request_type,
            event_name=name,
            event_time=time,
            event_month=month,
            event_day=day,
            event_weekDay=weekDay,
            is_weekly=is_weekly,
            event_command=command,
            event_repeat_days=day_bytes,
            event_offset=offset
        )


def holiday_constructor(request_type, month, day, day_of_week, holiday_type):
    return dict(
        table='date_tbl',
        request_type=request_type,
        event_month=month,
        event_day=day,
        event_weekDay=day_of_week,
        event_priority=holiday_type,
    )


def modified_request(request):
    """Takes requests as built from csv and converts them to what the SQL db will need

    Returns a dictionary where all keys except for :key:`request_type` and :key:`table`
    correlate with a column in the Sapphire table.

    For example, the :table:`date_tbl` has columns `event_month`, `event_day`,
    `event_weekDay`, `event_priority`.  So the request returned will have the keys
    `event_month`, `event_day`, `event_weekDay`, `event_priority`. as well as a
    `table` key, and a `request_type` key.

    """
    # 1. Match request_type
    request_type = request['request_type']

    # 2. Pre-process keys common across request
    if 'hours' in request:
        time = time_from_hours(request['hours'])
    elif 'time' in request:
        time = format_time(request['time'])

    if 'command' in request:
        try:
            command = command_in_bytes(request['command'].lower())
        except KeyError:
            msg = 'Event: {}, command {} is not a valid command'.format(request['name'], request['command'])
            msg += available_commands_string()
            raise Exception(msg)

    if 'days' in request:
        try:
            day_bytes = days_in_bytes(request['days'])
        except Exception as e:
            msg = str(e) + ', found in event: ' + str(request)
            raise Exception(msg)
    else:
        day_bytes = days_in_bytes('all')

    if 'offset' in request:
        offset = offset_from_str(request['offset'])
    else:
        offset = offset_from_str(None)

    if 'name' in request:
        name = request['name']
    else:
        name = None

    if 'holiday_type' in request:
        message = 'Holiday must be of type 1 or 2 not {}'.format(request['holiday_type'])
        assert int(request['holiday_type']) == 2 or int(request['holiday_type']) == 1, message
        priority = request['holiday_type']
        holiday_type = request['holiday_type']

    if 'month' in request:
        month = int(request['month']) - 1
        if not name: name = 'holiday'
        assert month >= 0 and month < 13, 'month {} for event {} not valid'.format(request['month'], name)


    if 'day' in request:
        day = int(request['day'])
        assert day > 0 and day < 32

    # 3. Validate everything and throw an error to the user if invalid input
    assert offset >=0 and offset <5, 'Offset for event name {} was invalid'.format(name)


    # 4. Make `request type`-specific changes and pass into a constructor
    if (request_type == 'weekly event' or
        request_type == 'weekly event with offset'):
        priority = 3
        new_request = event_constructor(request_type, name, time, command, day_bytes, priority, offset)

    elif request_type == 'holiday event':
        new_request = event_constructor(request_type, name, time, command, day_bytes, priority, offset)

    elif request_type == 'holiday event with offset':
        new_request = event_constructor(request_type, name, time, command, day_bytes, priority, offset)

    elif (request_type == 'special weekly event' or
          request_type == 'special weekly event with offset'):
        month = 0
        day = 0
        weekDay = 0
        is_weekly = 1
        new_request = special_constructor(request_type, name, time, month, day, weekDay, is_weekly, command, day_bytes, offset)

    elif (request_type == 'special date event' or
          request_type == 'special date event with offset'):
        is_weekly = 0
        weekDay = 0
        new_request = special_constructor(request_type, name, time, month, day, weekDay, is_weekly, command, day_bytes, offset)

    elif (request_type == 'special holiday event' or
          request_type == 'special holiday event with offset'):
        date = time_tuple(request['holiday'])
        is_weekly = 0
        weekDay = 0
        new_request = special_constructor(request_type, name, time, date.month, date.day, weekDay, is_weekly, command, day_bytes, offset)

    elif request_type == 'holiday date':
        day = request['day']
        new_request = holiday_constructor(request_type=request_type, month=month, day=day, day_of_week=0, holiday_type=priority)

    elif request_type == 'named holiday':
        time = time_tuple(name)
        new_request = holiday_constructor(request_type=request_type, month=time.month, day=time.day, day_of_week=0, holiday_type=priority)

    else:
        raise Exception('Section name {} not valid'.format(request_type))
    return new_request



def create_example_csv():
    rows = [
        ['weekly event'],
        ['name', 'command', 'time', 'days'],
        ['Event 1 - weekly', '1_on', '12:00', 'all'],
        [],

        #['weekly event with offset'],
        #['name', 'command', 'days', 'offset', 'hours'],
        #['event 2 - weekly w/ offset', '2_on', 'all', 'pre-sunrise', '4'],
        #[],



        ['special weekly event'],
        ['name', 'command', 'time', 'days'],
        ['event 3 - special weekly', '3_on', '12:00', 'all'],
        [],

        #['special weekly event with offset'],
        #['name', 'command', 'days', 'offset', 'hours'],
        #['event 4 - special weekly w/ offset', '4_on', 'all', 'pre-sunrise', '4'],
        #[],

        ['special date event'],
        ['name', 'command', 'month', 'day', 'time'],
        ['event 5 - special date ', '9_on', '2', '2', '12:00'],
        [],

        #['special date event with offset'],
        #['name', 'command', 'month', 'day', 'offset', 'hours'],
        #['event 6 - special date w/ offset ', '10_on', '2', '2', 'pre-sunset', '6'],
        #[],


        ['special holiday event'],
        ['name', 'command', 'holiday', 'time'],
        ['event 7 - special holiday event', '11_on', 'christmas', '16:00'],
        [],


        #['special holiday event with offset'],
        #['name', 'command', 'holiday', 'offset', 'hours'],
        #['event 8 - special holiday w/ offset', '12_on', 'new year', 'pre-sunset', '6'],
        #[],


        ['named holiday'],
        ['name', 'holiday_type'],
        ['new year', '1'],
        ['christmas', '2'],
        [],

        ['holiday date'],
        ['month', 'day', 'holiday_type', 'year'],
        ['2', '2', '1', '2016'],
        [],

        ['# This will create an event on new years, see above for holiday 1 definition'],
        ['holiday event'],
        ['name', 'command', 'time', 'holiday_type'],
        ['Holiday event 1', '14_on', '13:00', '1'],
        [],

        #['# This will create an event on christmas, see above for holiday 2 definition'],
        #['holiday event with offset'],
        #['name', 'command', 'holiday_type', 'offset', 'hours'],
        #['Holiday event 2 with offset', '3_on', '2', 'post-sunset', '5'],
        #[],
    ]


    with open(os.path.join(os.getcwd(), 'example.csv'), 'w', newline="") as f:
        writer = csv.writer(f)
        for row in rows:
            writer.writerow(row)
    print('Generated example.csv')


def available_commands_string():
    string += '\nAvailable Commands:\n'
    for cmd in sorted(get_commands(), key=lambda elem: elem[0]):
        string += '\t' + str(cmd) + '\n'

    return string


def process_requests(requests=None, filename=None):
        # We assume that there already exists a Sapphire DB from the scheduler in the current
        # directory called `DB`.  We do this because we want to read the schema from the DB
        # in order to populate it, instead of creating a DB schema naively from scratch.
        assert os.path.exists('DB'), 'Please move a Sapphire database named `DB` into the current directory'
        assert requests or filename, 'Must use request or filename arg in process_requests()'
        conn = sqlite3.connect('DB')
        c = conn.cursor()

        # Delete all data from existing database
        c.execute('''DELETE FROM event_tbl''')
        c.execute('''DELETE FROM date_tbl''')
        c.execute('''DELETE FROM special_tbl''')


        if not requests:
            requests = [modified_request(r) for r in requests_from_csv(filename)]
        # The next three blocks simply take request dictionaries and builds
        # SQL insert statements out of them, and then executes them.
        event_insert_stmt = '''INSERT INTO event_tbl ({}, {}, {}, {}, {}, {}) VALUES (?, ?, ?, ?, ?, ?);'''
        event_tbl_requests = [elem for elem in requests if elem['table'] == 'event_tbl']
        for request in event_tbl_requests:
            sorted_keys = tuple(sorted(elem for elem in request if elem != 'table' and elem != 'request_type'))
            sorted_values = tuple(request[key] for key in sorted_keys)
            try:
                c.execute(event_insert_stmt.format(*sorted_keys), sorted_values)
            except sqlite3.IntegrityError:
                string = "Event: {}, could not find the command in the current directory's .zip configuration file".format(request['event_name'], request['event_command'])
                string += available_commands_string()

                raise RuntimeError(string)

        special_stmt = '''INSERT INTO special_tbl ({}, {}, {}, {}, {}, {}, {}, {}, {})
                          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);'''
        special_requests = [elem for elem in requests if elem.get('table', None) == 'special_tbl']
        for request in special_requests:
            sorted_keys = tuple(sorted(elem for elem in request if elem != 'table' and elem != 'request_type'))
            sorted_values = tuple(request[key] for key in sorted_keys)
            try:
                c.execute(special_stmt.format(*sorted_keys), sorted_values)
            except sqlite3.IntegrityError:
                string = (
                    'Warning! "{}" skipped, probably '
                    'because a vector for the command '
                    'could not be found.'
                )
                print(string.format(request['event_name']))

        date_insert_stmt = '''INSERT INTO date_tbl ({}, {}, {}, {}) VALUES (?, ?, ?, ?)'''
        date_tbl_requests = [elem for elem in requests if elem.get('table', None) == 'date_tbl']
        for request in date_tbl_requests:
            sorted_keys = tuple(sorted(elem for elem in request if elem != 'table' and elem != 'request_type'))
            sorted_values = tuple(request[key] for key in sorted_keys)
            c.execute(date_insert_stmt.format(*sorted_keys), sorted_values)

        conn.commit()
        conn.close()
        print("Finished populating DB")


def main():
    global DEBUG
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--sap-commands', help='Print available Sapphire commands for schedule', action='store_true')
    parser.add_argument('--csv', help='The csv file name to build the DB from')
    parser.add_argument('--debug', help='Print debug information', action='store_true')
    parser.add_argument('--example', action='store_true', help='Generate an example.csv')
    parser.add_argument('--empty', action='store_true', help='Delete all data in DB')
    parser.add_argument('--generate', action='store_true', help='Generate a random DB')
    args = parser.parse_args()
    if args.sap_commands:
        commands = [key for key in commands_to_vector_dictionary()]
        print("All available commands:")
        for command in commands:
            print(command)
    else:
        DEBUG = args.debug

        if args.example:
            create_example_csv()

        if args.empty:
            assert os.path.exists('DB'), 'Please move a Sapphire database named `DB` into the current directory'
            conn = sqlite3.connect('DB')
            c = conn.cursor()
            # Delete all data from existing database
            c.execute('''DELETE FROM event_tbl''')
            c.execute('''DELETE FROM date_tbl''')
            c.execute('''DELETE FROM special_tbl''')
            conn.commit()
            conn.close()
            print("Emptied the database")

        # If there is a csv arg, we use it as the filename, but if not we do nothing because
        # we need a filename for a csv file in order to populate a DB with it.
        elif args.generate:
            print('Generating schedule')
            requests = autoschedule.all_priorities()
            process_requests(requests=requests)
        elif args.csv:
            filename = args.csv
            process_requests(filename=filename)

if __name__ == '__main__':
    main()
