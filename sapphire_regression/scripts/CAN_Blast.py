import time
import argparse
import textwrap

from itertools import cycle, groupby
from collections import OrderedDict
from canlib import canlib
from context import leviton_test
from leviton_test.commands import vector_to_commands_dictionary, button_list
from leviton_test.canparser import (
    str_from_msg,
    set_up_channel,
    tear_down_channel,
    tail_kvaser_channel,
    broadcast_msg,
    command_type,
    channel_number,
    target_level,
)
from leviton_test.view import View


def main():
    channel = set_up_channel(0)
    count = 0
    #header = 67076191
    naptime = .07

    while (count < 1):
        header = 234864539
        command = [0, 45, 1, 112, 10, 157, 0, 0]
        broadcast_msg(channel, command, header)
        print(command)
        time.sleep(0.006)
        header = 134201228
        command = [158, 255, 255, 159, 0, 0, 160, 0]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(0.006)
        header = 134201229

        command = [0, 161, 0, 0, 162, 255, 255, 163]
        print(command)
        broadcast_msg(channel, command, header)
        
        time.sleep(0.006)
        header = 134201230

        command = [0, 0, 164, 255, 255, 165, 0, 255]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(0.006)
        header = 134201231

        command = [168, 0, 255, 169, 0, 0]
        print(command)
        broadcast_msg(channel, command, header)

        count = count + 1

    
    
    '''
    while (count >= 0):
        command = [0, 50, 1, 112, 10, 157, 255, 255]
        broadcast_msg(channel, command, header)
        print(command)
        time.sleep(0.006)
        command = [0, 50, 1, 112, 10, 157, 0, 0]
        print(command)
        broadcast_msg(channel, command, header)
    '''
    '''
    while (count < 150):
        header = 201297439
        command = [2, 45, 0, 112, 10, 144, 25]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(naptime)
        command = [2, 45, 0, 112, 10, 148, 25]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(naptime)
        command = [2, 45, 0, 112, 10, 146, 25]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(naptime)
        command = [2, 45, 0, 112, 10, 147, 25]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(naptime)
        command = [2, 45, 0, 112, 10, 145, 25]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(naptime)
        command = [2, 45, 0, 112, 10, 144, 0]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(naptime)
        command = [2, 45, 0, 112, 10, 148, 0]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(naptime)
        command = [2, 45, 0, 112, 10, 146, 0]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(naptime)
        command = [2, 45, 0, 112, 10, 147, 0]
        print(command)
        broadcast_msg(channel, command, header)
        time.sleep(naptime)
        command = [2, 45, 0, 112, 10, 145, 0]
        print(command)
        broadcast_msg(channel, command, header)
        count = count + 1
    '''
    for (_, _, _, _, _) in tail_kvaser_channel(channel, timeout=1):
        pass
    tear_down_channel(channel)

if __name__ == '__main__':
    main()
