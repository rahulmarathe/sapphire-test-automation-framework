import time
from context import leviton_test
from leviton_test import sapphire
from leviton_test.astronomics import sunrise, sunset
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import Timer
from leviton_test import canparser
from leviton_test.greenmax import binary_state, analog_state
from leviton_test.view import View
from leviton_test import Serial_Commands
from leviton_test.utils import sapphire_AI_select


def main():
    sapphire_AI_select(analog_input = 1, binary_or_analog_device = 'binary')

if __name__ == '__main__':
    main()
