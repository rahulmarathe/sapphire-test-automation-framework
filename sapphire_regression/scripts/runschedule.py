# engine.py
"""Draws a schedule from a Sapphire DB, time skips to events and checks that the correct events occurred.

Features:

    #. Runs and verifies an arbitrary Sapphire schedule
    #. Automatically determines and tests correct behavior based on the exact
       same sqlite3 database and xml configuration files that the Sapphire
       instance itself is using.
    #. Prints test results
    #. Prints real-time test activity

Step 1: Load Schedule onto USB
Step 2: Load from USB into Sapphire
Step 3: Point Scheudler test script to relevant DB file

"""
import time as timer
import argparse
import sqlite3
import textwrap

from itertools import cycle, groupby
from collections import OrderedDict
from datetime import datetime, timedelta
import shutil
from canlib import canlib
from context import leviton_test
from leviton_test.commands import vector_to_commands_dictionary, button_list
from leviton_test.timeminer import get_occurrences, get_dates, day_from_bytes, Occurrence
from leviton_test.canparser import (
    time_msg_from_datetime,
    str_from_msg,
    set_up_channel,
    tear_down_channel,
    tail_kvaser_channel,
    date_msg_from_datetime,
    broadcast_msg,
    command_type,
    channel_number,
    target_level,
)
from leviton_test.view import View
import logging

LOG_FILENAME = 'logs/schedule.log'
logging.basicConfig(
    filename=LOG_FILENAME,
    level=logging.DEBUG,
)

PASS_CNT = 0
FAIL_CNT = 0
DEBUG = False
DB_PATH = None

def main():
    global DEBUG
    global DB_PATH
    parser = argparse.ArgumentParser(description='Run and test a schedule from a DB')
    parser.add_argument('DB_path', help='Folder where the DB is stored (e.g. C:\SAP_Auto_Test_Development\scripts\Test_11_12_23_28\DB)', type=str)
    parser.add_argument('start', help='The date to start the schedule runner, should be in format m/d/y (e.g., 1/10/2017 for Jan 1. 2017)', type=str)
    parser.add_argument('end', help='The date to end, should be in format m/d/y (e.g., 1/10/2017 for Jan 1. 2017)', type=str)
    parser.add_argument('--debug-off', action='store_true', help='Turn off real-time info display, only show results at end')
    args = parser.parse_args()
    DEBUG = False if args.debug_off else True
    DB_PATH = args.DB_path
    try:
        shutil.copy(DB_PATH, './DB')
    except shutil.SameFileError as e:
        print(e, 'Did not copy files, continuing with current DB')
    start = datetime.strptime(args.start, '%m/%d/%Y')
    end = datetime.strptime(args.end, '%m/%d/%Y')
    print_from_db()
    occurrences = get_occurrences(pre=start, post=end)
    schedule = Schedule(start, end, occurrences, debug=DEBUG)
    schedule.run()


def debug_print(*msgs):
    if DEBUG:
        print(*msgs)


def jump_shy(channel, current, target, seconds=5):
    """Returns approximate current datetime on Sapphire.

    Jumps from current datetime to seconds before target datetime.
    """
    return jump_time(channel, current, target - timedelta(seconds=seconds))


def jump_time(channel, current, target):
    """Returns approximate current datetime on Sapphire.

    Jumps from current datetime to seconds before target datetime.
    """
    # If the target time is before the current time, jump to the midnight before
    # the day before the target time, wait for midnight to pass, then jump to
    # the midnight before the event time, wait for midnight to pass,
    # then jump to the target time
    if target < current:
        day_before_target = target - timedelta(days=1)
        current = jump_to_midnight_before(channel, day_before_target)
        current = jump_to_midnight_before(channel, target)
        return send_datetime_message(channel, target)

    # if current time and target time are greater than a day way, jump to midnight,
    # wait for midnight to pass while printing messages
    elif (target - current).days > 1:
        return jump_to_midnight_before(channel, target)

    # if the target time is ahead of the current time, and their difference is less
    # than a day, then just jump directly to the event time
    else:
        return send_datetime_message(channel, target)


def jump_to_midnight_before(channel, event_time):
    wait_seconds = 10
    midnight_before_event = datetime(event_time.year, event_time.month, event_time.day, 0, 0) - timedelta(seconds=wait_seconds/2)
    current = send_datetime_message(channel, midnight_before_event)
    # Gather all messages
    debug_print('Flushing messages while midnight passes ( {} seconds )'.format(wait_seconds))
    flush_channel(channel, wait_seconds)
    debug_print('Midnight passed, continuing with tests...')
    return current + timedelta(seconds=wait_seconds)


def send_datetime_message(channel, time):
    debug_print('Jumping to ', time.strftime("%A, %d, %B %Y %I:%M:%S %p"))
    date_msg = date_msg_from_datetime(time)
    time_msg = time_msg_from_datetime(time)
    broadcast_msg(channel, date_msg)
    broadcast_msg(channel, time_msg)
    flush_channel(channel, 1)
    return time + timedelta(seconds=1)


def flush_channel(channel, seconds):
    for (_, _, _, _, _) in tail_kvaser_channel(channel, timeout=seconds):
        pass


def time_to_event_map(occurrences):
    all_events = dict()
    for event in occurrences:
        time = datetime(event.time.year, event.time.month, event.time.day)
        if not time in all_events:
            all_events[time] = [event]
        else:
            all_events[time].append(event)
    return all_events


def expected_events_per_day(occurrences):
    mapping = time_to_event_map(occurrences)
    expected = dict()
    for key, val in mapping.items():
        not_special = [x for x in val if x.priority != -1]
        if not_special:
            highest_priority = min(not_special, key=lambda x: x.priority)
        else:
            highest_priority = val[0]

        should_occur = []
        for event in val:
            if event.priority == highest_priority or event.priority == -1:
                should_occur.append(event)
        expected[key] = should_occur

    event_by_day = OrderedDict()

    # Group events by day
    for ev in occurrences:
        year_month_day = datetime_strip(ev.time)
        if year_month_day not in event_by_day:
            event_by_day[year_month_day] = []

        event_by_day[year_month_day].append(ev)

    # Reduce events down to only those with highest priority on their given day.
    # The `day` dictionary should simply map a datetime to ONLY the event(s) that is/are
    # supposed to occur.
    for date, events in list(event_by_day.items()):
        # -1 is the priority for special events, so exclude those

        filtered = [e for e in events if e.priority != -1]
        if not filtered:
            filtered = events
        lowest_event = min(filtered, key=lambda x: x.priority)
        event_by_day[date] = [e for e in events if e.priority <= lowest_event.priority]

    # Stifle any priority 3 events if there is a holiday on that day
    holidays = get_dates(DB_PATH)
    for date, events in list(event_by_day.items()):
        new_event_list = []
        for hol in holidays:
            # Doesn't check for multiple holidays on the same day
            if date.month == hol.month and date.day == hol.day:
                event_by_day[date] = [e for e in events if e.priority <= hol.priority]

    return event_by_day


def print_from_db():
    fill = '_' * 20
    logging.info(fill + 'TEST_STARTING' + fill + ':  Time: {}'.format(datetime.now().strftime("%D %T")))
    # Get expected events and times
    commandmap = vector_to_commands_dictionary()
    # Display contents of Sqlite3 DB
    conn = sqlite3.connect(DB_PATH)
    print(DB_PATH)
    c = conn.cursor()
    dates = []
    final_string = "\n\nThe contents of the Sqlite3 DB\n"
    final_string += ('This is ALL the DB contents, and is not restricted to the time range'
          ' arguments.')
    final_string += '\nHoliday Dates:\n'

    for row in c.execute('SELECT event_month, event_day, event_priority FROM date_tbl'):
        month, day, priority = row
        final_string += '{}/{}, priority {}\n'.format(month + 1, day, priority)

    final_string += '\nSpecial Event: '
    for row in c.execute(
            'SELECT event_name, event_month, event_day, event_time,' +
            ' event_weekDay, is_weekly, event_offset, event_command, event_repeat_days FROM special_tbl'
        ):
        name, month, event_day, time, day_of_week, is_weekly, offset, command, repeat_days = row
        string = '{}, \ndate {}/{}, \ntime {}'.format(name, month, event_day, time)
        if is_weekly:
            string += ', weekly=True'
        string += '\ncommand {}, \nrepeat days {}\n\n'
        final_string += string.format(commandmap[command], day_from_bytes(repeat_days))

    holiday1_events = []
    holiday2_events = []
    weekly_events = []
    for row in c.execute('SELECT event_name,event_time, event_repeat_days, event_priority, event_command FROM event_tbl'):
        name, time, days, priority, command = row
        string = '{}, \ntime {}, \ndays {}, \npriority {}, \ncommand {}\n\n'.format(name, time, day_from_bytes(days), priority, commandmap[command])
        if priority == 1:
            holiday1_events.append(string)
        elif priority == 2:
            holiday2_events.append(string)
        elif priority == 3:
            weekly_events.append(string)
        else:
            raise Exception('Event with wrong priority: ' + string)
    conn.close()

    final_string += 'Holiday 1 Events:'
    if not holiday1_events:
        final_string += "\tNone"
    else:
        for string in holiday1_events:
            final_string += string + '\n'

    final_string += 'Holiday 2 Events:'
    if not holiday2_events:
        final_string += "\tNone"
    else:
        for string in holiday2_events:
            final_string += string + '\n'

    final_string += '\nWeekly Events:'
    if not weekly_events:
        final_string += '\tNone'
    else:
        for string in weekly_events:
            final_string += string + '\n'
    final_string = textwrap.dedent(final_string)
    logging.info(final_string)
    debug_print(final_string)


def datetime_strip(time):
    return datetime(time.year, time.month, time.day)


def print_events_by_day(occurrences, expected_by_day):
    """Prints all events organized by day.

    The display will look like so:

    Day: 12/2/1
    Time       Event    Priority
    12:01 AM   12_on    2
    12:03 AM   11_on    1           <---- Expected


    """
    print("All Events in DB")
    day_string = '\nDay: {}'
    spacing = '{:<12} {:<9} {:<9}'
    row = lambda ev: spacing.format(ev.time.strftime('%I:%M %p'), ev.command, ev.priority)
    expected_string = lambda ev: row(ev) + ' <----- Expected'
    heading = spacing.format('Time', 'Event', 'Priority')
    already_printed = set()
    current_time = occurrences[0].time
    for idx, event in enumerate(occurrences):
        datestring = event.time.strftime('%Y/%m/%d')
        if datestring not in already_printed:
            print(day_string.format(datestring))
            print(heading)
            already_printed.add(datestring)

        key = datetime_strip(event.time)
        if event in expected_by_day[key]:
            print(expected_string(event))
        else:
            print(row(event))


class Schedule(object):

    def __init__(self, start, end, occurrences, debug=False):
        assert type(start) is datetime and type(end) is datetime
        self.start = start
        self.end = end
        self.events = occurrences
        self.debug = debug
        self.timelist = sorted(list(set([x.time for x in self.events])))
        self.timeformat = "%A, %d, %B %Y %I:%M:%S %p"

    def run(self):
        '''Runs a schedule and gathers all messages from Kvaser'''
        # Set time to one day before first event
        global PASS_CNT
        global FAIL_CNT
        expected_map = self.map_expected_at_datetime()
        self.print_expected(expected_map)
        expected_map = self.set_events_to_priority_none(expected_map)
        self.evaluate_passfail(expected_map)

        print('Tests Passed: {}'.format(PASS_CNT))
        print('Tests Failed: {}'.format(FAIL_CNT))
        fill = '_' * 20
        logging.info(fill + 'TEST_ENDING' + fill + ':  Time: {}'.format(datetime.now().strftime("%D %T")))

    def set_events_to_priority_none(self, expected_map):
        expected_map = {
            key: [Occurrence(command=event.command, time=event.time, priority=None) for event in val]
            for key,val in expected_map.items()
        }
        return expected_map

    def print_expected(self, expected_map):
        introduction = """

        =======================
        Sapphire Scheduler Test
        =======================

        This is the automated Sapphire Scheduler regression test suite.  Below
        are the schedule events and whether they should or should not fire.  The
        test will run Sapphire to each event and detect if Sapphire is sending
        the correct commands.  The result will be reported in the end.

        Check logs/schedule.log for full database output.

        ===============
        Expected Events
        ==============="""


        introduction = textwrap.dedent(introduction)
        print(introduction)
        logging.info(introduction)
        already_printed = set()
        first = True
        for time in self.timelist:
            day = datetime(time.year, time.month, time.day)
            if day not in already_printed:
                daystring = day.strftime('%A, %d, %B %Y')
                s1 = '\n\n' + daystring
                s2 = '-' * len(daystring)
                print(s1)
                print(s2)
                logging.info(s1)
                logging.info(s2)
                already_printed.add(day)
            timestring = time.strftime('%I:%M:%S %p')
            expected = expected_map[time]
            all_at_time = [event for event in self.events if event.time == time]

            for event in sorted(list(set(expected + all_at_time)), key=lambda x: x.time):
                if event in expected:
                    expstring = '<--- expected'
                else:
                    expstring = ''

                finalstring = '{}, command: {}, priority: {}, {}'.format(
                    timestring,
                    event.command,
                    event.priority,
                    expstring
                )
                print(finalstring)
                logging.info(finalstring)

        priority_keystring = """
        +-----------------------+
        |     Priority Key      |
        +------------+----------+
        | Event Type | Priority |
        +------------+----------+
        | Holiday 1  |    1     |
        | Holiday 2  |    2     |
        | Weekly     |    3     |
        | Special    |   -1     |
        +------------+----------+


        Sapphire Policy Notes
        ---------------------

        Priority -1: should always fire, and don't suppress any other event.
        Priority  1: suppresses priority 2 and 3 events.
        Priority  2: suppresses priority 3 events.
        Priority  3: suppresses no events, and only fires if there are no
                     priority 1 or 2 events.

        * note: Special events in Sapphire implementation have no priority, -1
            is just for convenience in testing



        Test script now running...
        """
        priority_keystring = textwrap.dedent(priority_keystring)
        print(priority_keystring)
        print()
        logging.info(priority_keystring + '\n')

    def evaluate_passfail(self, expected_map):
        global PASS_CNT
        global FAIL_CNT
        actual_map = self.map_actual_at_datetime()
        results = 'Test Results'
        print(results)
        print('-' * len(results))
        for time in list(expected_map):
            expected = expected_map[time]
            actual = actual_map[time]
            for event in expected:
                if event in actual:
                    PASS_CNT += 1
                    string = " + Passed: {}, {}"
                else:
                    string = " - Failed: {}, {}"
                    FAIL_CNT += 1
                result = string.format(event.command, event.time.strftime(self.timeformat))
                print(result)
                logging.info(result)

            not_expected = set(actual) - set(expected)
            for event in not_expected:
                string = " - Failed: {} fired at {}, but wasn't supposed to!".format(event.command, event.time.strftime(self.timeformat))
                print(string)
                logging.info(string)
                FAIL_CNT += 1

    def map_actual_at_datetime(self):
        """Returns a dict of the datetimes that the events occurred, with the values
        being the message that was sent at that datetime.

        Runs tests, executes time shifts, and reads the Kvaser feed.
        This is the function that actually executes tests.
        """
        expected_at_datetime = self.map_expected_at_datetime()
        debug_print('Gathered expectd datetime and messages...\n')
        actual_at_datetime = dict()
        first_event = True
        ch = set_up_channel(0)
        jump_seconds = 5 # The seconds before the event that we want to jump to
        current_jump_time = datetime.now()
        debug_print('Begin time changes to scheduled times...\n')
        for time in self.timelist:
            if first_event:
                self.wakeup_android()
                debug_print('Android woken up via adb...')
                day_before = time - timedelta(days=1)
                current_jump_time = jump_shy(ch, current_jump_time, day_before)
                self.settings_and_back()
                debug_print('Opened up settings and returned...')
                first_event = False

            # gather all expected events for the next datetime
            expected = expected_at_datetime[time]

            # Jump to `seconds` before event
            current_jump_time = jump_shy(ch, current_jump_time, time, seconds=jump_seconds)
            self.print_all_events_for_datetime(time)
            # Gather all messages
            debug_print('Listening...')
            temp_messages = []
            for (msgId, msg, dlc, flg, kvaser_time) in tail_kvaser_channel(ch, timeout=jump_seconds + 10):
                if command_type(msg) == 'Channel Fade':
                    temp_messages.append(msg)
                    messagestring = '  --  Kvaser: {}'.format(str_from_msg(msg))
                    debug_print(messagestring)
                    logging.info(messagestring)
            fade_messages = [msg for msg in temp_messages if command_type(msg) == 'Channel Fade']

            realtime_occurrences = []
            # Construct Occurrence(time, command, priority) from msg
            for msg in fade_messages:
                command = self.command_from_message(msg)
                priority = None
                occurrence = Occurrence(command=command, priority=None, time=time)
                realtime_occurrences.append(occurrence)

            actual_at_datetime[time] = realtime_occurrences

        tear_down_channel(ch)
        return actual_at_datetime

    def map_expected_at_datetime(self):
        '''Returns a dictionary from exact datetimes to a list of events expected
        at that datetime.
        '''
        daymap = self.map_expected_at_day()
        timemap = dict.fromkeys(self.timelist)
        for time in list(timemap):
            day = self.day_from(time)
            events_at_time = [event for event in self.events if event.time == time]
            only_expected = [event for event in events_at_time if event in daymap[day]]
            timemap[time] = only_expected

        return timemap

    def map_expected_at_day(self):
        daymap = dict.fromkeys(self.day_from(t) for t in self.timelist)
        for day in list(daymap):
            events_at_day = [event for event in self.events if self.day_from(event.time) == day]
            only_expected = self.filter_group_by_expectation(events_at_day)
            daymap[day] = only_expected

        return daymap

    def filter_group_by_expectation(self, group):
        non_special_events = [e for e in group if e.priority != -1]
        if non_special_events:
            lowest_priority = min(non_special_events, key=lambda x: x.priority).priority
        else:
            lowest_priority = min(group, key=lambda x: x.priority).priority
        return [event for event in group if event.priority <= lowest_priority]

    def all_events_on_datetime(self, time):
        return [event for event in self.events if event.time == time]

    def day_from(self, time):
        return datetime(time.year, time.month, time.day)

    def wakeup_android(self):
        view = View()
        view.wakeup()
        timer.sleep(2)

    def settings_and_back(self):
        view = View()
        view.press('settings', seconds=2)
        view.back()
        timer.sleep(2)

    def print_all_events_for_datetime(self, time):
        expected_at_datetime = self.map_expected_at_datetime()
        expected = expected_at_datetime[time]
        for event in self.all_events_on_datetime(time):
            if event in expected:
                string = 'Event(command={}, priority={})'.format(event.command, event.priority)
                string += ' <---- Expected'
            else:
                string = 'Event(command={}, priority={})'.format(event.command, event.priority)
            debug_print(string)

    def command_from_message(self, message):
        if target_level(message) == 0.0:
            level = '_off'
        else:
            level = '_on'
        channel = channel_number(message)
        return str(channel) + level

if __name__ == '__main__':
    import time
    before = time.time()
    main()
    elapsed = time.time() - before
    print('\nTest finished in', round(elapsed/60, 2), ' minutes')
