from context import leviton_test
from leviton_test import Kvaser_Functions


class Constants:
    NODE_ID_QUERY = 0
    NODE_ID_REPLY_OR_PUBLISH = 1
    NODE_LOCATE = 2
    ASSIGN_NODE_ID = 3
    NODE_RESET = 4
    TIME_QUERY = 10
    TIME_REPLY_OR_PUBLISH = 11
    DATE_QUERY = 12
    DATE_REPLY_OR_PUBLISH = 13
    BEHAVIOR_TRANSITION_QUERY = 14
    BEHAVIOR_TRANSITION_REPLY_OR_PUBLISH = 15
    APPLICATION_CODE_MEMORY_TRANSFER_REQUEST = 20
    APPLICATION_CODE_MEMORY_TRANSFER_ACKNOWLEDGE = 21
    CONFIGURATION_MEMORY_TRANSFER_REQUEST = 22
    CONFIGURATION_MEMORY_TRANSFER_ACKNOWLEDGE = 23
    FILE_MEMORY_TRANSFER_REQUEST = 24
    FILE_MEMORY_TRANSFER_ACKNOWLEDGE = 25
    LOCAL_DEVICE_UPDATE_REQUEST = 26
    LOCAL_DEVICE_UPDATE = 27
    MEMORY_BLOCK_TRANSFER_SEND = 30
    MEMORY_BLOCK_TRANSFER_ACKNOWLEDGE = 31
    DATABASE_WRITE_PERMISSION_REQUEST_OR_RELINQUISH = 32
    DATABASE_WRITE_PERMISSION_REQUEST_ACKNOWLEDGE = 33
    PROPERTY_WRITE = 34
    PROPERTY_WRITE_ACKNOWLEDGE = 35
    PROPERTY_QUERY = 36
    PROPERTY_REPLY_OR_PUBLISH = 37
    FADE_GO = 40
    CHANNEL_FADE = 45
    CHANNEL_TIMED_OFF = 46
    CHANNEL_BLINK_WARN = 47
    CHANNEL_RELINQUISH = 48
    CHANNEL_STATUS_QUERY = 49
    CHANNEL_STATUS_REPLY_OR_PUBLISH = 50
    CHANNEL_BLINK = 51
    CHANNEL_PRIORITY_ARRAY_REQUEST = 55
    CHANNEL_PRIORITY_ARRAY_REPLY_OR_PUBLISH = 56
    GROUP_FADE = 65
    GROUP_TIMED_OFF = 66
    GROUP_BLINK_WARN = 67
    GROUP_RELINQUISH = 68
    GROUP_STATUS_QUERY = 69
    GROUP_STATUS_REPLY_OR_PUBLISH = 70
    GROUP_BLINK = 71
    ROOM_COMBINE_OR_SEPARATE = 75
    INPUT_DEVICE_QUERY = 80
    INPUT_DEVICE_REPLY_OR_PUBLISH = 81
    PERSONALITY_SET = 101
    PERSONALITY_QUERY = 102
    PERSONALITY_PUBLISH_OR_REPLY = 103
    SOFTLOCK_LEVEL_SET = 107
    SOFTLOCK_LEVEL_QUERY = 108
    SOFTLOCK_LEVEL_PUBLISH_OR_REPLY = 109
    SUBNET_NUMBER_QUERY = 110
    SUBNET_NUMBER_REPLY_OR_PUBLISH = 111
    LOAD_SHED = 120
    METERING_DATA_QUERY = 121
    METERING_DATA_REPLY_PUBLISH = 122
    ALARM_OR_ERROR_QUERY = 130
    ALARM_OR_ERROR_REPLY_PUBLISH = 131
    ALARM_OR_ERROR_ACKNOWLEDGE_AND_FLUSH = 132
    BACNET = 240
    SECTORNET = 243


# -------Long list of Message Type Classes------------------------------
# Most of these definitions are just skeletons and should be filled in as the
# implementation grows.


class NodeIdQuery:
    """Defines the message class for message Node Id Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class NodeIdReplyOrPublish:
    """Defines the message class for message Node Id Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class NodeLocate:
    """Defines the message class for message Node Locate"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class AssignNodeId:
    """Defines the message class for message Assign Node Id"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class NodeReset:
    """Defines the message class for message Node Reset"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class TimeQuery:
    """Defines the message class for message Time Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class TimeReplyOrPublish:
    """Defines the message class for message Time Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class DateQuery:
    """Defines the message class for message Date Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class DateReplyOrPublish:
    """Defines the message class for message Date Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class BehaviorTransitionQuery:
    """Defines the message class for message Behavior Transition Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class BehaviorTransitionReplyOrPublish:
    """Defines the message class for message Behavior Transition Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ApplicationCodeMemoryTransferRequest:
    """Defines the message class for message Application Code Memory Transfer Request"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ApplicationCodeMemoryTransferAcknowledge:
    """Defines the message class for message Application Code Memory Transfer Acknowledge"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ConfigurationMemoryTransferRequest:
    """Defines the message class for message Configuration Memory Transfer Request"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ConfigurationMemoryTransferAcknowledge:
    """Defines the message class for message Configuration Memory Transfer Acknowledge"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class FileMemoryTransferRequest:
    """Defines the message class for message File Memory Transfer Request"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class FileMemoryTransferAcknowledge:
    """Defines the message class for message File Memory Transfer Acknowledge"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class LocalDeviceUpdateRequest:
    """Defines the message class for message Local Device Update Request"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class LocalDeviceUpdate:
    """Defines the message class for message Local Device Update"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class MemoryBlockTransferSend:
    """Defines the message class for message Memory Block Transfer Send"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class MemoryBlockTransferAcknowledge:
    """Defines the message class for message Memory Block Transfer Acknowledge"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class DatabaseWritePermissionRequestOrRelinquish:
    """Defines the message class for message Database Write Permission Request Or Relinquish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class DatabaseWritePermissionRequestAcknowledge:
    """Defines the message class for message Database Write Permission Request Acknowledge"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class PropertyWrite:
    """Defines the message class for message Property Write"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class PropertyWriteAcknowledge:
    """Defines the message class for message Property Write Acknowledge"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class PropertyQuery:
    """Defines the message class for message Property Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class PropertyReplyOrPublish:
    """Defines the message class for message Property Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class FadeGo:
    """Defines the message class for message Fade Go"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ChannelFade:
    """Defines the message class for message Channel Fade"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ChannelTimedOff:
    """Defines the message class for message Channel Timed Off"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ChannelBlinkWarn:
    """Defines the message class for message Channel Blink Warn"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ChannelRelinquish:
    """Defines the message class for message Channel Relinquish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ChannelStatusQuery:
    """Defines the message class for message Channel Status Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ChannelStatusReplyOrPublish:
    """Defines the message class for message Channel Status Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ChannelBlink:
    """Defines the message class for message Channel Blink"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ChannelPriorityArrayRequest:
    """Defines the message class for message Channel Priority Array Request"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class ChannelPriorityArrayReplyOrPublish:
    """Defines the message class for message Channel Priority Array Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class GroupFade:
    """Defines the message class for message Group Fade"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class GroupTimedOff:
    """Defines the message class for message Group Timed Off"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class GroupBlinkWarn:
    """Defines the message class for message Group Blink Warn"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class GroupRelinquish:
    """Defines the message class for message Group Relinquish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class GroupStatusQuery:
    """Defines the message class for message Group Status Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class GroupStatusReplyOrPublish:
    """Defines the message class for message Group Status Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class GroupBlink:
    """Defines the message class for message Group Blink"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class RoomCombineOrSeparate:
    """Defines the message class for message Room Combine Or Separate"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class InputDeviceQuery:
    """Defines the message class for message Input Device Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class InputDeviceReplyOrPublish:
    """Defines the message class for message Input Device Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class PersonalitySet:
    """Defines the message class for message Personality Set"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class PersonalityQuery:
    """Defines the message class for message Personality Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class PersonalityPublishOrReply:
    """Defines the message class for message Personality Publish Or Reply"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class SoftlockLevelSet:
    """Defines the message class for message Softlock Level Set"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class SoftlockLevelQuery:
    """Defines the message class for message Softlock Level Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class SoftlockLevelPublishOrReply:
    """Defines the message class for message Softlock Level Publish Or Reply"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class SubnetNumberQuery:
    """Defines the message class for message Subnet Number Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class SubnetNumberReplyOrPublish:
    """Defines the message class for message Subnet Number Reply Or Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class LoadShed:
    """Defines the message class for message Load Shed"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class MeteringDataQuery:
    """Defines the message class for message Metering Data Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class MeteringDataReplyPublish:
    """Defines the message class for message Metering Data Reply Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class AlarmOrErrorQuery:
    """Defines the message class for message Alarm Or Error Query"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class AlarmOrErrorReplyPublish:
    """Defines the message class for message Alarm Or Error Reply Publish"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class AlarmOrErrorAcknowledgeAndFlush:
    """Defines the message class for message Alarm Or Error Acknowledge And Flush"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class Bacnet:
    """Defines the message class for message Bacnet"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


class Sectornet:
    """Defines the message class for message Sectornet"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


def message_factory(number):
    return {
        0: NodeIdQuery,
        1: NodeIdReplyOrPublish,
        2: NodeLocate,
        3: AssignNodeId,
        4: NodeReset,
        10: TimeQuery,
        11: TimeReplyOrPublish,
        12: DateQuery,
        13: DateReplyOrPublish,
        14: BehaviorTransitionQuery,
        15: BehaviorTransitionReplyOrPublish,
        20: ApplicationCodeMemoryTransferRequest,
        21: ApplicationCodeMemoryTransferAcknowledge,
        22: ConfigurationMemoryTransferRequest,
        23: ConfigurationMemoryTransferAcknowledge,
        24: FileMemoryTransferRequest,
        25: FileMemoryTransferAcknowledge,
        26: LocalDeviceUpdateRequest,
        27: LocalDeviceUpdate,
        30: MemoryBlockTransferSend,
        31: MemoryBlockTransferAcknowledge,
        32: DatabaseWritePermissionRequestOrRelinquish,
        33: DatabaseWritePermissionRequestAcknowledge,
        34: PropertyWrite,
        35: PropertyWriteAcknowledge,
        36: PropertyQuery,
        37: PropertyReplyOrPublish,
        40: FadeGo,
        45: ChannelFade,
        46: ChannelTimedOff,
        47: ChannelBlinkWarn,
        48: ChannelRelinquish,
        49: ChannelStatusQuery,
        50: ChannelStatusReplyOrPublish,
        51: ChannelBlink,
        55: ChannelPriorityArrayRequest,
        56: ChannelPriorityArrayReplyOrPublish,
        65: GroupFade,
        66: GroupTimedOff,
        67: GroupBlinkWarn,
        68: GroupRelinquish,
        69: GroupStatusQuery,
        70: GroupStatusReplyOrPublish,
        71: GroupBlink,
        75: RoomCombineOrSeparate,
        80: InputDeviceQuery,
        81: InputDeviceReplyOrPublish,
        101: PersonalitySet,
        102: PersonalityQuery,
        103: PersonalityPublishOrReply,
        107: SoftlockLevelSet,
        108: SoftlockLevelQuery,
        109: SoftlockLevelPublishOrReply,
        110: SubnetNumberQuery,
        111: SubnetNumberReplyOrPublish,
        120: LoadShed,
        121: MeteringDataQuery,
        122: MeteringDataReplyPublish,
        130: AlarmOrErrorQuery,
        131: AlarmOrErrorReplyPublish,
        132: AlarmOrErrorAcknowledgeAndFlush,
        240: Bacnet,
        243: Sectornet,
    }.get(number)


class MessageType:
    message_type_octet = 1
    def grab_message_type(self):
        octet = self.CAN_message[self.message_type_octet]


class ChannelNumber:
    def grab_channel_number(self):
        octet = self.get_channel_number_octet()
        start, end = self.get_bit_index_range()
        return int(octet[start:end], base=2)


class CANmessage(MessageType, ChannelNumber):
    def __init__(self, CAN_message):
        self.CAN_message = ['{0:b}'.format(num).zfill(8) for num in CAN_message]
    def __str__(self):
        return str(self.CAN_message)

    def get_channel_number_octet(self):
        pass

    def get_bit_index_range(self):
        pass


def main():
    ch1 = Kvaser_Functions.set_up_channel(0)
    for msg in Kvaser_Functions.messages(ch1):
        message = list(msg)
        if len(message) > 1:
            Message = message_factory(message[1])
            if Message:
                message = Message(message)
        print(message)

if __name__ == '__main__':
    main()
