import context
from context.leviton_test.view import View

v = View()
v.press_settings()
if 'Sign out' not in v.text_elements:
    v.press('Sign in')
    v.edit_texts[1].press()
    v.keyevent('5625')
    v.press('OK')
