# example.py
"""This file demonstrates how to use the regression suite as a Python module."""
import os

from regression import SapphireTestCase
from greenmax import binary_state, analog_state
from mocks import MockView


class PurePythonTestCase(SapphireTestCase):
    """The primary means of test creation is by subclassing :class:`SapphireTestCase`
    and overriding the the run() method.  Refer to view.py for the View API.

    """

    def run(self):
        self.view.scroll_menu_left()
        self.view.press('Newton')
        self.test_toggle('A1', [1])
        self.test_toggle('A2', [1])
        self.test_toggle('B1', [2, 3])
        self.test_toggle('B2', [2, 3])
        self.test_toggle('C1', [1, 2, 3, 4])
        self.test_toggle('C1', [1, 2, 3, 4])

    def test_toggle(self, button, relays):
        """This function is to reduce repetition in testing button toggling.

        Arguments:
            button: str
                The button to be pressed twice, once to test toggling on, once to test toggling off.
            relays: list of int's
                The relays that :arg:button are expected to toggle on and off.  Example: [1, 4, 5] indicates relays 1, 4, and 5.

        Note that there are two ways to get hardware state.  One is by the function
        binary_state(), and the other is analog_state().  Both return a dictionary
        of channel-state pairs.

            For example, if you wanted the binary state of relay 1:
                >>> state = binary_state()
                >>> state[1] == 1 or state[1] == 0   # This will either be 1 or 0, depending on if relay 1 is on or off (respectively)
                True

        Note: assertTrue takes a boolean expression and a message string.  The message string is displayed as the test result method.

        """

        # We expect each button to make their respect relays toggle on to 1, and then off to 0
        for expected in [1, 0]:
            self.view.press(button)
            state = binary_state()
            for relay in relays:
                self.assertTrue(state[relay] == expected, '{} turns relay {} to state {}'.format(button, relay, expected))


class SimpleTestCase(SapphireTestCase):
    """This class defines a straight-forward, run-of-the-mill test case.  It uses no mock and no special functions.

    """

    def run(self):
        """This shows one of the simplest implementations of run.

        We simply press A1, get the binary state of the relays, and assert that
        relay 1 was turned on.  We include a message string to the assertTrue that
        will be displayed in the test results.
        """
        self.view.press('A1')
        state = binary_state()
        self.assertTrue(state[1] == 1, 'Button A1 turned on relay 1')


# Example Usage
# =============

# Since SimpleTestCase requires an adb connection (it is not mocked), this will throw a ConnectionAbortedError if
# a connection to the device can not be made

next_test = SimpleTestCase()
test.run_and_report()

# Mocking
# =======

# For PurePythonTestCase, we use a xml file to mock out Sapphire.  This allows us to run test code without an actual device, but still
# with a realistic view hierarchy.  The xml file referenced here is one that was pulled from the device using ADB.  The command
# to do so is in the README or in the :file:`adb.py` source code.

# You simply instantiate the class you defined above and call its :method:`test_and_report`.
# Note that you can change the test cases's ViewClass, which is View by default.  This is for dependency injection.
# MockView is used here so that we are not using a real view, but instead the xml file referred to by :arg:`path_to_xml`.

# Note: Mocking is not required, see above for SimpleTestCase that does not use Mocking

path_to_xml = os.path.join("C:\\", "LabView Development", "Phython Sheduler", "regression-create-mock-device", "views", "newton.xml") # platform-independent path building
test = PurePythonTestCase(ViewClass=MockView, path_to_xml=path_to_xml)
test.run_and_report()

import doctest
doctest.testmod()
