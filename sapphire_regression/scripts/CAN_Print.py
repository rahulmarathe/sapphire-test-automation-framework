import math
from context import leviton_test
from leviton_test import Kvaser_Functions
from leviton_test.parser import messages
from leviton_test.parser import generics


'''
def main():
    ch1 = Kvaser_Functions.set_up_channel(0)
    for msg in Kvaser_Functions.messages(ch1):
        message = Kvaser_Functions.bits_from_msg(list(msg))
        if len(message) > 1:
            message_type = messages.message_factory(message)
        print(message_type)
        #if hasattr(message_type):
            #print('channel_number', message_type.channel_number)
'''
def main():
    can_check(
    message_type = 65,
    fade_time_or_rate = 1,
    fade_rate = 1.0,
    group_number = 2,
    priority = 7,
    header_priority = 5,
    header_broadcast_or_muticast = 1,
    header_destination_routing = 1,
    header_destination_address = 255,
    header_source_routing = 0,
    header_source_address = 56,
    header_start_linked = 1,
    header_sequence = 15
    )
'''
def main():
    can_check(
    message_type = 45,
    fade_time_or_rate = 0,
    fade_rate = 1.0,
    channel_number = 1,
    priority = 7,
    target_level = 50,
    header_priority = 5,
    header_broadcast_or_muticast = 1,
    header_destination_routing = 1,
    header_destination_address = 255,
    header_source_routing = 0,
    header_source_address = 56,
    header_start_linked = 1,
    header_sequence = 15
    )
'''


def can_check(message_type, button = None, *args, **kwargs):
    truth = False
    ch1 = Kvaser_Functions.set_up_channel(0)
    print('\n')
    for msg in Kvaser_Functions.messages(ch1):
        message = Kvaser_Functions.bits_from_msg(list(msg))
        header = Kvaser_Functions.bits_from_header(list(msg))
        #print(list(msg)) #prints in CANking format
        #print(message) #prints in raw octets
        if len(message) > 1:
            message_inst = messages.message_factory(message)
            header_inst = messages.header_factory(header)
            checked_message = message_inst.__class__.__name__ #creates new string with the CAN command type name as the first element. This will be built into a checked string
            checked_header = ''
            if message_type == int(message[1], base = 2): #checks that the message type specified exists. It can't check message parameters until it finds a specified message type

                for key, value in kwargs.items():
                    for attr_name, attr_val in header_inst.__class__.__dict__.items(): #runs until there are no more attribute keyword pairs in the class
                        if key == attr_name:
                            attribute = getattr(header_inst, attr_name)
                            try:
                                value_check = (attribute == value)
                            except ValueError:
                                print('attribute', type(attribute), 'type', type(value))
                            if value_check:
                                checked_header = checked_header + ', ' + str(attribute) + '-(' + str(value) + ')' + ('(P)')
                            else:
                                checked_header = checked_header + ', ' + str(attribute) + '-(' + str(value) + ')' + ('(F)')
                                anyfail = True

                    for attr_name, attr_val in message_inst.__class__.__dict__.items(): #runs until there are no more attribute keyword pairs in the class
                        if key == attr_name:
                            attribute = getattr(message_inst, attr_name)
                            try:

                                '''if attr_name is "fade_rate":
                                    as_float = float(attribute)
                                    value_check = math.isclose(as_float, value, rel_tol=1e-2, abs_tol=0.0)
                                else:
                                    value_check = (attribute == value)'''
                                if attr_name is "target_level": #this is to take in a user specified target level as a percent, convert and compare to bus value
                                    value = int((value/100)*255)
                                    value_check = (attribute == value)
                                else:
                                    value_check = (attribute == value)
                            except ValueError:
                                print('attribute', type(attribute), 'type', type(value))
                            if value_check:
                                checked_message = checked_message + ', ' + str(attribute) + '-(' + str(value) + ')' + ('(P)')
                            else:
                                checked_message = checked_message + ', ' + str(attribute) + '-(' + str(value) + ')' + ('(F)')
                                anyfail = True

                print('+++++++++++++', '\n', 'Message---', checked_message, '\n', 'Header---', checked_header)




if __name__ == '__main__':
    main()
    #for message in Kvaser_Functions.bits_messages_reversed_octets():
        #print(message)
