import subprocess
import os
import shutil

for root, _, files in os.walk('.\data\csv_schedules'):
    paths = []
    for file_ in files:
        if 'Sheet' not in file_ and 'Template' not in file_:
            path = os.path.join(root, file_)
            paths.append(path)

    cmd = lambda path: ['python', 'builder.py', '--csv', path]
    for p in paths:
        foldername, filename = os.path.split(p)
        filename = filename.strip('.csv')
        if not os.path.exists(filename):
            os.makedirs(filename)
        print('Creating folder for ' + filename)
        #print(foldername, filename)
        print('\n\nRunning', p)
        subprocess.run(cmd(p))
        shutil.copy('./DB', filename + '/DB')
