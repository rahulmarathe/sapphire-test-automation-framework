import context
from context import leviton_test
from leviton_test.view import View

v = View()
v.press_settings()
if 'Sign out' not in v.text_elements:
    v.press('Sign in')
    v.edit_texts[1].press()
    v.keyevent('5625')
    v.press('OK')

v.swipe(300, 400, 300, 100, refresh=False)
v.swipe(300, 400, 300, 100, refresh=True)
v.press('Scheduler')
v.press('options')
v.press('Import/Export')
v.press('Import Schedule from USB')
v.back()
v.back()
