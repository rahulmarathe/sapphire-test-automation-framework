import time as timer
import argparse
import sqlite3
import textwrap

from itertools import cycle, groupby
from collections import OrderedDict
from datetime import datetime, timedelta
from canlib import canlib
from context import leviton_test
from leviton_test.commands import vector_to_commands_dictionary, button_list
from leviton_test.timeminer import get_occurrences, get_dates, day_from_bytes, Occurrence
from leviton_test.canparser import (
    time_msg_from_datetime,
    str_from_msg,
    set_up_channel,
    tear_down_channel,
    tail_kvaser_channel,
    date_msg_from_datetime,
    broadcast_msg,
    command_type,
    channel_number,
    target_level,
)
from leviton_test.view import View



def main():
    parser = argparse.ArgumentParser(description='Enter Time in M/YD/YYYY h:m:s format')
    parser.add_argument('date', help='The date to start the schedule runner, should be in format mm/dd/yy (e.g., 1/10/2017 for Jan 1. 2017)', type=str)
    parser.add_argument('time', help='The time to start the schedule runner, should be in format HH/mm/ss (e.g., 23:30:00)', type=str)
    parser.add_argument('--debug-off', action='store_true', help='Turn off real-time info display, only show results at end')
    args = parser.parse_args()
    time = datetime.strptime(args.date + " " + args.time, '%m/%d/%Y %H:%M:%S' )
    send_datetime_message(time)


def send_datetime_message(time):
    channel = set_up_channel(0)
    print('Jumping to ', time.strftime("%A, %d, %B %Y %I:%M:%S %p"))
    date_msg = date_msg_from_datetime(time)
    time_msg = time_msg_from_datetime(time)
    broadcast_msg(channel, date_msg)
    broadcast_msg(channel, time_msg)
    flush_channel(channel, 1)
    tear_down_channel(channel)
    return time + timedelta(seconds=1)

def flush_channel(channel, seconds):
    for (_, _, _, _, _) in tail_kvaser_channel(channel, timeout=seconds):
        pass

if __name__ == '__main__':
    main()
