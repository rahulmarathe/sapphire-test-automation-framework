"""This script was used to generate the Advanced Can parser classes for each LC3 feature"""

from CAN_Parser_Advanced import Constants

line_template = '''    {number}: {classname},\n'''
filename = 'class_mapping.py'
with open(filename, 'w') as f:
    f.write('\nmessage_class = {\n')
    attrs = Constants.__dict__.items()
    attrs = [attr for attr in attrs if type(attr[1]) is int]
    attrs = sorted(attrs, key=lambda x: x[1])
    for attr in attrs:
        attr, val = attr
        attr = attr.title().replace('_', '')
        line = line_template.format(number=val, classname=attr)
        f.write(line)
    f.write('}')



class_template = '''\
class {}:
    """Defines the message class for message {}"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.__class__.__name__ + ' : ' + str(self.msg)


'''

filename = 'special_classes.py'
with open(filename, 'w') as f:
    attrs = Constants.__dict__.items()
    attrs = [attr for attr in attrs if type(attr[1]) is int]
    attrs = sorted(attrs, key=lambda x: x[1])
    for attr in attrs:
        attr, val = attr
        non_replaced = attr.title().replace('_', ' ')
        attr = attr.title().replace('_', '')
        line = class_template.format(attr, non_replaced)
        f.write(line)
