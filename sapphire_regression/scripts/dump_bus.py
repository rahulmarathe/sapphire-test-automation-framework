from context import leviton_test
from leviton_test.canparser import set_up_channel, tear_down_channel, messages, str_from_msg, bits_from_msg

ch = set_up_channel(0)
for msg in messages(ch):
    m = str_from_msg(msg)
    ints = [int(b, base=2) for b in bits_from_msg(msg)]
    print(ints)

tear_down_channel(ch)
