from context import leviton_test
from leviton_test import Kvaser_Functions
from leviton_test.parser import messages



def main():
    ch1 = Kvaser_Functions.set_up_channel(0)
    for msg in Kvaser_Functions.messages(ch1):
        message = Kvaser_Functions.bits_from_msg(list(msg))
        header = Kvaser_Functions.bits_from_header(list(msg))
        #print(list(msg)) #prints in CANking format
        #print(message)
        print(header)
        if len(message) > 1:
            message_type = messages.message_factory(message)
            header_type = messages.header_factory(header)
            #print(message_type)
            print(header_type)
        #if hasattr(message_type):
            #print('channel_number', message_type.channel_number)

if __name__ == '__main__':
    main()
