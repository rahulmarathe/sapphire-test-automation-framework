# autoschedule.py
"""This module is for generating schedules for use by Sapphire."""
import calendar as cal
from itertools import combinations, cycle
from functools import reduce
from random import randrange
from datetime import datetime, timedelta

import builder

from context import leviton_test
from leviton_test.commands import command_in_bytes, on_command_key_generator, on_commands


command_keys = ['{}_{}'.format(num, toggle) for num in range(1, 16) for toggle in ['on', 'off']]
USED_COMMANDS = []


def combinations_range(start, end):
    return reduce(lambda a, b: a + b, map(list, [combinations(range(start, end), i) for i in range(1, len(range(start, end)) + 1)]))


def priority_combos():
    return combinations_range(1, 4)


def day_cycle():
    """Cycles ['FRIDAY', 'SATURDAY', 'SUNDAY', ...].  Starts on Friday because
    Jan 1, 2016 was a Friday, which is assumed to be the first date of generated schedules"""
    days = [cal.day_name[i].upper() for i in [4, 5, 6, 1, 2, 3]]
    yield from cycle(days)


def proliferate(max=4):
    """Returns a list of lists of numbers 1, 2, or 3, repeated from 1 to 4 times.

    Example:

        >>> proliferate()
        [[1, 1, 1], [2], [3, 3, 3], [1, 2], [1, 1, 3, 3], [2, 2, 2, 3, 3, 3], [1, 1, 1, 2, 2, 2, 3, 3, 3]]

    """
    return [sorted(y for _ in range(randrange(1, max)) for y in x) for x in priority_combos()]


def create_holiday_requests(priorities):
    return [
        builder.holiday_constructor(
            request_type='holiday_date',
            month=1,
            day=idx+1,
            day_of_week=0,
            holiday_type=holiday_type
        )
        for idx, day in enumerate(priorities)
        for holiday_type in day
        if (holiday_type == 1 or holiday_type == 2)

    ]


def create_weekly_requests(priorities):
    global USED_COMMANDS
    noon = datetime(2016, 1, 1, 12, 2) # 12:02
    minute = timedelta(minutes=1)
    cnt = 0
    weekday = day_cycle()
    keygen = on_command_key_generator()
    result = []
    for idx, day in enumerate(priorities):
        current_day = next(weekday)
        for event_cnt, holiday_type in enumerate(day):
            if holiday_type == 3:
                cnt = (cnt + 1) % 17
                key = next(keygen)
                USED_COMMANDS.append(key)
                command = command_in_bytes(key)
                time = (noon + minute*event_cnt).strftime('%H:%M')
                name = 'weekly_day{}_{}'.format(idx+1, time)
                day_bytes = builder.days_in_bytes(current_day)
                result.append(
                    builder.event_constructor(
                        request_type='weekly event',
                        name=name,
                        time=time,
                        command=command,
                        day_bytes=day_bytes,
                        priority=3,
                        offset=0
                    )
                )
    return result


def all_priorities():
    global USED_COMMANDS
    priorities = proliferate()
    weeklys = create_weekly_requests(priorities)
    holidays = create_holiday_requests(priorities)
    # Iterate through keys so not to use the same command as a weekly event
    keygen = on_command_key_generator()
    for i in range(len(weeklys)):
        next(keygen)

    key1 = next(keygen)
    key2 = next(keygen)
    USED_COMMANDS.append(key1)
    USED_COMMANDS.append(key2)
    holiday_events = [
        builder.event_constructor(
            request_type='holiday_event',
            name='holiday1',
            time='12:00',
            command=command_in_bytes(key1),
            day_bytes=builder.days_in_bytes('all'),
            priority=1,
            offset=0
        ),
        builder.event_constructor(
            request_type='holiday_event',
            name='holiday2',
            time='12:01',
            command=command_in_bytes(key2),
            day_bytes=builder.days_in_bytes('all'),
            priority=2,
            offset=0
        ),
    ]
    return weeklys + holidays + holiday_events
