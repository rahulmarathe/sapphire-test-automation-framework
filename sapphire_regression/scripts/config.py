# config.py
"""This file defines config-related globals, such as LabView exe paths and devices IP's."""

import os


mocks_path = os.path.join(
     'C:\\',
     'Users',
     'BEK',
     'sapphire_regression',
     'branches',
     'pre_code_review',
     'sapphire_regression',
     'greenmax_mocks'
)

if os.getenv('Is_Development_Machine') == 'True':
    SAPPHIRE_IP = '192.168.2.220'
    GM_BINARY_CSV_PATH = 'C:\\SAP_Auto_Test_Development\\LabVIEW Programs for Phython\\ReadGMXBinary\\Binary.csv'
    GM_BINARY_EXE_PATH = 'C:\\SAP_Auto_Test_Development\\LabVIEW Programs for Phython\\ReadGMXBinary\\Read GMX Binary.exe'
    SAPPHIRE_WAIT_TIME = 3
    SAPPHIRE_VERBOSE = True
    GM_ANALOG_EXE_PATH = 'C:\\SAP_Auto_Test_Development\\LabVIEW Programs for Phython\\ReadGMXAnalog\\Read GMX Analog.exe'
    GM_ANALOG_CSV_PATH = 'C:\\SAP_Auto_Test_Development\\LabVIEW Programs for Phython\\ReadGMXAnalog\\Analog.csv'
    GM_DS_EXE_PATH = 'C:\\SAP_Auto_Test_Development\\LabVIEW Programs for Phython\\GMX GTDS Toggle\\GMX GTDS Toggle.exe'
    SET_DAQ_VOLTAGE = 'C:\\SAP_Auto_Test_Development\\LabVIEW Programs for Phython\\DAQ Set Voltage\\DAQ Set Voltage.exe'
    SAPPHIRE_SERIAL_COM_PORT = 'COM8'
    NUMATO_MUX_RELAY_COM_PORT = 'COM24'

else:
    SAPPHIRE_IP = '192.168.2.220'
    GM_BINARY_CSV_PATH = os.path.join(mocks_path, 'binary.csv')
    GM_BINARY_EXE_PATH = os.path.join(mocks_path, 'binary_mock.py')
    SAPPHIRE_WAIT_TIME = 3
    SAPPHIRE_VERBOSE = True
    GM_ANALOG_EXE_PATH = os.path.join(mocks_path, 'analog_mock.py')
    GM_ANALOG_CSV_PATH = os.path.join(mocks_path, 'analog.csv')
    GM_DS_EXE_PATH = os.path.join(mocks_path, 'ds_mock.py')
    SAPPHIRE_SERIAL_COM_PORT = 'COM8'
    NUMATO_MUX_RELAY_COM_PORT = 'COM24'
