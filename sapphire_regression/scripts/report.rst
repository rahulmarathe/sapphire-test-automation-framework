

===========================================
Sapphire Regression Test: 2017-06-29 13:59
===========================================


AITestCase
==========

= ==========

AITestCase
==========
= ==========
Out of range Analog ID not retained Passed
Device ID Assigned Passed
Check for Switch Device type Passed
Check for Occupancy Sensor Device type Passed
Check for Photocell Device type Passed
Check for Contact Control Device type Passed
Check for Potentiometer Device type Passed
Cancel selection changes nothing Passed
Set Sampling Interval Passed
Disable Input Device Passed
Enable Input Device Passed
Out of range Analog ID not retained Passed
Device ID Assigned Passed
Check for Switch Device type Passed
Check for Occupancy Sensor Device type Passed
Check for Photocell Device type Passed
Check for Contact Control Device type Passed
Check for Potentiometer Device type Passed
Cancel selection changes nothing Passed
Set Sampling Interval Passed
Disable Input Device Passed
Enable Input Device Passed
Set Sampling Interval: 100-->Switch Passed
Set Threshold: 5-->Switch Passed
AI_1 HW Maximum Assigned: 969-->Switch Passed
Set Sampling Interval: 100-->Switch Passed
Set Threshold: 5-->Switch Passed
AI_2 HW Maximum Assigned: 964-->Switch Passed
Passed: Button: AI_1 Checking Switch On -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-255: Active-(255)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: AI_1 Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
		>Button: AI_1 Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: AI_2 Checking Switch On -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-255: Active-(255)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
		>Button: AI_2 Checking Switch On -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-255: Active-(255)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: AI_2 Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
		>Button: AI_2 Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: Both Checking Switch On -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-255: Active-(255)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: Both Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: Both Checking Switch On -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-255: Active-(255)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: Both Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: AI_1 Checking Occupancy Sensor Occupied -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-255: Occupied-(255)(P), Status Change-(0)(P), Input Device Type-2: Occ Sensor-(2)(P), 
 Passed
Passed: Button: AI_1 Checking Occupancy Sensor Vacant -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Un-Occupied-(0)(P), Status Change-(0)(P), Input Device Type-2: Occ Sensor-(2)(P), 
 Passed
Passed: Button: AI_2 Checking Occupancy Sensor Occupied -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-255: Occupied-(255)(P), Status Change-(0)(P), Input Device Type-2: Occ Sensor-(2)(P), 
 Passed
Passed: Button: AI_2 Checking Occupancy Sensor Vacant -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Un-Occupied-(0)(P), Status Change-(0)(P), Input Device Type-2: Occ Sensor-(2)(P), 
 Passed
Passed: Button: Both Checking Occupancy Sensor Occupied -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-255: Occupied-(255)(P), Status Change-(0)(P), Input Device Type-2: Occ Sensor-(2)(P), 
 Passed
Passed: Button: Both Checking Occupancy Sensor Vacant -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Un-Occupied-(0)(P), Status Change-(0)(P), Input Device Type-2: Occ Sensor-(2)(P), 
 Passed
Passed: Button: Both Checking Occupancy Sensor Occupied -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-255: Occupied-(255)(P), Status Change-(0)(P), Input Device Type-2: Occ Sensor-(2)(P), 
 Passed
Passed: Button: Both Checking Occupancy Sensor Vacant -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Un-Occupied-(0)(P), Status Change-(0)(P), Input Device Type-2: Occ Sensor-(2)(P), 
 Passed
Passed: Button: AI_1 Checking Contact Control On -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-255: Closed-(255)(P), Status Change-(0)(P), Input Device Type-4: Contact Closure-(4)(P), 
 Passed
Passed: Button: AI_1 Checking Contact Control Off -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Open-(0)(P), Status Change-(0)(P), Input Device Type-4: Contact Closure-(4)(P), 
 Passed
Passed: Button: AI_2 Checking Contact Control On -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-255: Closed-(255)(P), Status Change-(0)(P), Input Device Type-4: Contact Closure-(4)(P), 
 Passed
Passed: Button: AI_2 Checking Contact Control Off -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Open-(0)(P), Status Change-(0)(P), Input Device Type-4: Contact Closure-(4)(P), 
 Passed
Passed: Button: Both Checking Contact Control On -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-255: Closed-(255)(P), Status Change-(0)(P), Input Device Type-4: Contact Closure-(4)(P), 
 Passed
Passed: Button: Both Checking Contact Control Off -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Open-(0)(P), Status Change-(0)(P), Input Device Type-4: Contact Closure-(4)(P), 
 Passed
Passed: Button: Both Checking Contact Control On -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-255: Closed-(255)(P), Status Change-(0)(P), Input Device Type-4: Contact Closure-(4)(P), 
 Passed
Passed: Button: Both Checking Contact Control Off -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Open-(0)(P), Status Change-(0)(P), Input Device Type-4: Contact Closure-(4)(P), 
 Passed
Set Sampling Interval: 200-->Photocell Passed
Set Threshold: 10-->Photocell Passed
AI_1 HW Maximum Assigned: 368-->Photocell Passed
Set Sampling Interval: 200-->Photocell Passed
Set Threshold: 10-->Photocell Passed
AI_2 HW Maximum Assigned: 367-->Photocell Passed
Passed: Button: AI_1 Checking Photocell at 1V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 24-(25)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_1 Checking Photocell at 3V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 76-(76)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_1 Checking Photocell at 5V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 127-(127)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_1 Checking Photocell at 8V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 204-(204)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_1 Checking Photocell at 10V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 255-(255)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2 Checking Photocell at 1V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 25-(25)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2 Checking Photocell at 3V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 75-(76)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2 Checking Photocell at 5V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 127-(127)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2 Checking Photocell at 8V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 203-(204)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2 Checking Photocell at 10V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 255-(255)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_1 Checking Photocell at 1V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 24-(25)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_2 Checking Photocell at 1V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 25-(25)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_1 Checking Photocell at 3V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 76-(76)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_2 Checking Photocell at 3V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 75-(76)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_1 Checking Photocell at 5V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 127-(127)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_2 Checking Photocell at 5V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 127-(127)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_1 Checking Photocell at 8V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 204-(204)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_2 Checking Photocell at 8V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 203-(204)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_1 Checking Photocell at 10V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 255-(255)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: Both_AI_2 Checking Photocell at 10V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 255-(255)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Set Sampling Interval: 100-->Photocell Passed
Set Threshold: 5-->Photocell Passed
AI_1 HW Maximum Assigned: 368-->Photocell Passed
Set Sampling Interval: 50-->Switch Passed
Set Threshold: 40-->Switch Passed
AI_2 HW Maximum Assigned: 962-->Switch Passed
Passed: Button: AI_1_PC Checking Photocell at 1V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 25-(25)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_1_PC Checking Photocell at 3V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 76-(76)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_1_PC Checking Photocell at 5V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 127-(127)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_1_PC Checking Photocell at 8V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 204-(204)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_1_PC Checking Photocell at 10V -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State: 255-(255)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2_SW Checking Switch On -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-255: Active-(255)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
		>Button: AI_2_SW Checking Switch On -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-255: Active-(255)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: AI_2_SW Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
		>Button: AI_2_SW Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
		>Button: AI_2_SW Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Set Sampling Interval: 100-->Photocell Passed
Set Threshold: 5-->Photocell Passed
AI_2 HW Maximum Assigned: 367-->Photocell Passed
Set Sampling Interval: 50-->Switch Passed
Set Threshold: 40-->Switch Passed
AI_1 HW Maximum Assigned: 969-->Switch Passed
Passed: Button: AI_2_PC Checking Photocell at 1V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 25-(25)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2_PC Checking Photocell at 3V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 76-(76)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2_PC Checking Photocell at 5V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 127-(127)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2_PC Checking Photocell at 8V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 203-(204)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_2_PC Checking Photocell at 10V -> InputDeviceReplyOrPublish, Input Device Number: 65535-(65535)(P), State: 255-(255)(P), Status Change-(0)(P), Input Device Type-3: Photocell-(3)(P), 
 Passed
Passed: Button: AI_1_SW Checking Switch On -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-255: Active-(255)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
		>Button: AI_1_SW Checking Switch On -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-255: Active-(255)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
Passed: Button: AI_1_SW Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
		>Button: AI_1_SW Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
		>Button: AI_1_SW Checking Switch Off -> InputDeviceReplyOrPublish, Input Device Number: 66-(66)(P), State-0: Inactive-(0)(P), Status Change-(0)(P), Input Device Type-1: Switch-(1)(P), 
 Passed
= ==========


===========================================
Sapphire Regression Test: 2017-06-29 14:14
===========================================


UISettingsDateTimeTestCase
==========================

= ==========

UISettingsDateTimeTestCase
==========================
= ==========
Oct is on screen Passed
20 is on screen Passed
2015 is on screen Passed
10:30  AM is on screen Passed
US/Pacific Time zone is on screen Passed
Has correct sunrise time Passed
Has correct sunset time Passed
Oct is on screen Passed
30 is on screen Passed
PM is on screen Passed
22:30 on screen Passed
10:30 on screen Passed
Log export successful Passed
Configuration export successful Passed
Sapphire is connected to a network Passed
Sapphire has the correct static IP address Passed
Sapphire has the correct Gatway Address Passed
Sapphire has the correct Subnet Mask Passed
Node ID:256 is not in valid range. Passed
Node ID conflict Check Passed
Node ID no longer in conflict Check Passed
Project Settings is on screen Passed
Project Number: is on screen Passed
Project Name: is on screen Passed
New Sapphire Studio Project is on screen Passed
Contact Name: is on screen Passed
Contact Number: is on screen Passed
This Station: is on screen Passed
Foo is on screen Passed
123-123-1234 is on screen Passed
Texts A1, A2, B1, B2, C1, C2 on the screen Passed
Backlight timer turned brightness to 0 after 1 minutes Passed
Brightness lowered after 1 minute Passed
Displayed Error message for out-of-range backlight value 100 Passed
Displayed Error message for out-of-range backlight time 500 Passed
Screen has ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'] Passed
Screen has ['D1-FT', 'E1-FR', 'D2-FT', 'F1-FT', 'E2-FR', 'G1-Off', 'F2-FT'] Passed
Screen has ['A', 'B', 'C', 'D'] Passed
Screen has ['Shock A', 'Shock B', 'Shock C', 'Shock D', 'Shock E'] Passed
Babbage has 6 sliders Passed
Babbage slider Babb 13 is labeled Passed
Babbage slider Babb 14 is labeled Passed
Babbage slider Babb 15 is labeled Passed
Babbage slider Babb 16 is labeled Passed
Babbage slider Babb 5-8 is labeled Passed
Babbage slider Babb 13-16 is labeled Passed
Screen has ['A:On', 'A:Raise', 'B:On', 'A:Lower', 'B:Raise', 'A:Off', 'B:Lower', 'B:Off'] Passed
Faraday has 3 sliders Passed
Screen has ['B1 GWY', 'B2 GWY', 'B2 GWY GMX'] Passed
= ==========


===========================================
Sapphire Regression Test: 2017-06-29 14:17
===========================================


NewtonTestCase
==============

=================================================================================================================================================================== ==========

NewtonTestCase
==============
=================================================================================================================================================================== ==========
Button A1 turns relay 1 to state 1                                                                                                                                  Failed
Failed:Button: A1 -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Rate-(0)(F), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(255)(F), Channel Number: 1-(1)(P), 
 Failed
Failed:Button: A1 -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Rate-(0)(F), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(0)(P), Channel Number: 1-(1)(P), 
   Failed
Button A1 turns relay 1 to state 0                                                                                                                                  Passed
Button A2 turns relay 1 to state 1                                                                                                                                  Passed
Passed: Button: A2 -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 1-(1)(P), 
 Passed
Button A2 turns relay 1 to state 0                                                                                                                                  Passed
Passed: Button: A2 -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 1-(1)(P), 
    Passed
Button B1 turns relay 2 to state 1                                                                                                                                  Passed
Button B1 turns relay 3 to state 1                                                                                                                                  Passed
Passed: Button: B1 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 2-(2)(P), 
                      Passed
Passed: Button: B1 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 3-(3)(P), 
                      Passed
Button B1 turns relay 2 to state 0                                                                                                                                  Passed
Button B1 turns relay 3 to state 0                                                                                                                                  Passed
Passed: Button: B1 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 2-(2)(P), 
                          Passed
Passed: Button: B1 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 3-(3)(P), 
                          Passed
Button B2 turns relay 2 to state 1                                                                                                                                  Passed
Button B2 turns relay 3 to state 1                                                                                                                                  Passed
Passed: Button: B2 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 2-(2)(P), 
                      Passed
Passed: Button: B2 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 3-(3)(P), 
                      Passed
Button B2 turns relay 2 to state 0                                                                                                                                  Passed
Button B2 turns relay 3 to state 0                                                                                                                                  Passed
Passed: Button: B2 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 2-(2)(P), 
                          Passed
Passed: Button: B2 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 3-(3)(P), 
                          Passed
Button C1 turns relay 1 to state 1                                                                                                                                  Passed
Button C1 turns relay 2 to state 1                                                                                                                                  Passed
Button C1 turns relay 3 to state 1                                                                                                                                  Passed
Button C1 turns relay 4 to state 1                                                                                                                                  Passed
Button C1 turns relay 9 to state 1                                                                                                                                  Passed
Button C1 turns relay 10 to state 1                                                                                                                                 Passed
Button C1 turns relay 11 to state 1                                                                                                                                 Passed
Button C1 turns relay 12 to state 1                                                                                                                                 Passed
Passed: Button: C1 -> GroupFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Group Number: 1-(1)(P), Fade Time/Rate: 5.0-(5)(P), 
                                Passed
Button C1 turns relay 1 to state 0                                                                                                                                  Passed
Button C1 turns relay 2 to state 0                                                                                                                                  Passed
Button C1 turns relay 3 to state 0                                                                                                                                  Passed
Button C1 turns relay 4 to state 0                                                                                                                                  Passed
Button C1 turns relay 9 to state 0                                                                                                                                  Passed
Button C1 turns relay 10 to state 0                                                                                                                                 Passed
Button C1 turns relay 11 to state 0                                                                                                                                 Passed
Button C1 turns relay 12 to state 0                                                                                                                                 Passed
Passed: Button: C1 -> GroupFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Group Number: 1-(1)(P), Fade Time/Rate: 5.0-(5)(P), 
                                Passed
Button C2 turns relay 1 to state 1                                                                                                                                  Passed
Button C2 turns relay 2 to state 1                                                                                                                                  Passed
Button C2 turns relay 3 to state 1                                                                                                                                  Passed
Button C2 turns relay 4 to state 1                                                                                                                                  Passed
Button C2 turns relay 9 to state 1                                                                                                                                  Passed
Button C2 turns relay 10 to state 1                                                                                                                                 Passed
Button C2 turns relay 11 to state 1                                                                                                                                 Passed
Button C2 turns relay 12 to state 1                                                                                                                                 Passed
Passed: Button: C2 -> GroupFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Group Number: 1-(1)(P), Fade Time/Rate: 5.0-(5)(P), 
                                Passed
Button C2 turns relay 1 to state 0                                                                                                                                  Passed
Button C2 turns relay 2 to state 0                                                                                                                                  Passed
Button C2 turns relay 3 to state 0                                                                                                                                  Passed
Button C2 turns relay 4 to state 0                                                                                                                                  Passed
Button C2 turns relay 9 to state 0                                                                                                                                  Passed
Button C2 turns relay 10 to state 0                                                                                                                                 Passed
Button C2 turns relay 11 to state 0                                                                                                                                 Passed
Button C2 turns relay 12 to state 0                                                                                                                                 Passed
Passed: Button: C2 -> GroupFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Group Number: 1-(1)(P), Fade Time/Rate: 5.0-(5)(P), 
                                Passed
=================================================================================================================================================================== ==========


===========================================
Sapphire Regression Test: 2017-06-29 14:20
===========================================


TeslaTestCase
=============

= ==========

TeslaTestCase
=============
= ==========
Button A turns relay 1 to state 1 Passed
Button A turns relay 2 to state 1 Passed
Passed: Button: A -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: A -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 2-(2)(P), 
 Passed
Passed: Button: GTDS_1 -> ChannelRelinquish, Fade Type: Rate-(1)(P), Source Node: 33-(33)(P), Fade Time/Rate: 5.0-(5)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: GTDS_2 -> ChannelRelinquish, Fade Type: Rate-(1)(P), Source Node: 33-(33)(P), Fade Time/Rate: 5.0-(5)(P), Channel Number: 2-(2)(P), 
 Passed
GTDS turned relay 1 off Passed
GTDS turned relay 2 off Passed
Button B turns relay 3 to state 1 Passed
Passed: Button: B -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 3-(3)(P), 
 Passed
Passed: Button: GTDS_4 -> ChannelFade, Fade Type: Rate-(1)(P), Source Node: 33-(33)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 4-(4)(P), 
 Passed
DS turned relay 4 On Passed
Passed: Button: GTDS_3 -> ChannelRelinquish, Fade Type: Rate-(1)(P), Source Node: 33-(33)(P), Fade Time/Rate: 5.0-(5)(P), Channel Number: 3-(3)(P), 
 Passed
Passed: Button: GTDS_4 -> ChannelRelinquish, Fade Type: Rate-(1)(P), Source Node: 33-(33)(P), Fade Time/Rate: 5.0-(5)(P), Channel Number: 4-(4)(P), 
 Passed
DS turned relay 3 off Passed
DS turned relay 4 off Passed
D turns relay 5 to level between 6.77 and 8.17, Real Voltage: 7.39 Passed
D turns relay 6 to level between 6.77 and 8.17, Real Voltage: 7.46 Passed
D turns relay 7 to level between 6.77 and 8.17, Real Voltage: 7.49 Passed
D turns relay 8 to level between 6.77 and 8.17, Real Voltage: 7.44 Passed
Passed: Button: D -> GroupFade, Fade Type: Rate-(1)(P), Group Number: 2-(2)(P), Fade Time/Rate: 3.0-(3)(P), 
 Passed
= ==========


===========================================
Sapphire Regression Test: 2017-06-29 14:25
===========================================


ShockleyTestCase
================

= ==========

ShockleyTestCase
================
= ==========
Shock A turns relay 5 to level between 0.71 and 2.11, Real Voltage: 1.24 Passed
Shock A turns relay 6 to level between 7.22 and 8.62, Real Voltage: 7.98 Passed
Passed: Button: Shock A -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 51-(51)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Shock A -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 204-(204)(P), Channel Number: 6-(6)(P), 
 Passed
Shock A turns relay 5 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.02 Passed
Shock A turns relay 6 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 13.90 Passed
Passed: Button: Shock A -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Shock A -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 6-(6)(P), 
 Passed
Shock B turns relay 7 to level between 1.8 and 3.2, Real Voltage: 2.58 Passed
Shock B turns relay 8 to level between 9.44 and 10.84, Real Voltage: 9.85 Passed
Passed: Button: Shock B -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 77-(76)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: Shock B -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 8-(8)(P), 
 Passed
Shock B turns relay 7 to level between 0.01 and 1.41, Real Voltage: 0.80 Passed
Shock B turns relay 8 to level between 0.01 and 1.41, Real Voltage: 0.77 Passed
Passed: Button: Shock B -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 26-(25)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: Shock B -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 26-(25)(P), Channel Number: 8-(8)(P), 
 Passed
Shock C turns relay 5 to level between 0.71 and 2.11, Real Voltage: 1.24 Passed
Shock C turns relay 6 to level between 1.8 and 3.2, Real Voltage: 2.66 Passed
Shock C turns relay 7 to level between 5.35 and 6.75, Real Voltage: 5.98 Passed
Shock C turns relay 8 to level between 7.22 and 8.62, Real Voltage: 7.96 Passed
Passed: Button: Shock C -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 51-(51)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Shock C -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 77-(76)(P), Channel Number: 6-(6)(P), 
 Passed
Passed: Button: Shock C -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 153-(153)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: Shock C -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 204-(204)(P), Channel Number: 8-(8)(P), 
 Passed
Shock C turns relay 5 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.01 Passed
Shock C turns relay 6 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 13.89 Passed
Shock C turns relay 7 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.13 Passed
Shock C turns relay 8 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.37 Passed
Passed: Button: Shock C -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 0-(0)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Shock C -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 0-(0)(P), Channel Number: 6-(6)(P), 
 Passed
Passed: Button: Shock C -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 0-(0)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: Shock C -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 0-(0)(P), Channel Number: 8-(8)(P), 
 Passed
Button Shock D turns relay 1 to state 1 Passed
Button Shock D turns relay 2 to state 1 Passed
Button Shock D turns relay 3 to state 1 Passed
Passed: Button: Shock D -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: Shock D -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 2-(2)(P), 
 Passed
Passed: Button: Shock D -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 3-(3)(P), 
 Passed
Button Shock D turns relay 1 to state 0 Passed
Button Shock D turns relay 2 to state 0 Passed
Button Shock D turns relay 3 to state 0 Passed
Passed: Button: Shock D -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 0-(0)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: Shock D -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 0-(0)(P), Channel Number: 2-(2)(P), 
 Passed
Passed: Button: Shock D -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 0-(0)(P), Channel Number: 3-(3)(P), 
 Passed
Shock E turns relay 5 to level between 9.44 and 10.84, Real Voltage: 9.83 Passed
Shock E turns relay 6 to level between 9.44 and 10.84, Real Voltage: 9.91 Passed
Button Shock E turns relay 1 to state 1 Passed
Button Shock E turns relay 2 to state 1 Passed
Button Shock E turns relay 3 to state 1 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 255-(255)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 255-(255)(P), Channel Number: 2-(2)(P), 
 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 255-(255)(P), Channel Number: 3-(3)(P), 
 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 255-(255)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 255-(255)(P), Channel Number: 6-(6)(P), 
 Passed
Shock E turns relay 5 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.01 Passed
Shock E turns relay 6 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 13.89 Passed
Button Shock E turns relay 1 to state 0 Passed
Button Shock E turns relay 2 to state 0 Passed
Button Shock E turns relay 3 to state 0 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(0)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(0)(P), Channel Number: 2-(2)(P), 
 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(0)(P), Channel Number: 3-(3)(P), 
 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(0)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Shock E -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(0)(P), Channel Number: 6-(6)(P), 
 Passed
= ==========


===========================================
Sapphire Regression Test: 2017-06-29 14:27
===========================================


BabbageTestCase
===============

= ==========

BabbageTestCase
===============
= ==========
Sliding Babb 13 to level 100 turns relay 13 to level between 3.03 and 4.43, Real Voltage: 3.51 Passed
Passed: Button: Babb 13 -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1.0)(P), Target Level: 102-(102)(P), Channel Number: 13-(13)(P), 
 Passed
Sliding Babb 14 to level 100 turns relay 14 to level between 4.25 and 5.65, Real Voltage: 4.72 Passed
Passed: Button: Babb 14 -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1.0)(P), Target Level: 127-(127)(P), Channel Number: 14-(14)(P), 
 Passed
Sliding Babb 15 to level 100 turns relay 15 to level between 6.77 and 8.17, Real Voltage: 7.27 Passed
Passed: Button: Babb 15 -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1.0)(P), Target Level: 191-(191)(P), Channel Number: 15-(15)(P), 
 Passed
Sliding Babb 16 to level 100 turns relay 16 to level between 9.44 and 10.84, Real Voltage: 9.82 Passed
Passed: Button: Babb 16 -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1.0)(P), Target Level: 255-(255)(P), Channel Number: 16-(16)(P), 
 Passed
Sliding Babb 5-8 to level 100 turns relay 5 to level between 9.44 and 10.84, Real Voltage: 9.84 Passed
Sliding Babb 5-8 to level 100 turns relay 6 to level between 9.44 and 10.84, Real Voltage: 9.92 Passed
Sliding Babb 5-8 to level 100 turns relay 7 to level between 9.44 and 10.84, Real Voltage: 9.97 Passed
Sliding Babb 5-8 to level 100 turns relay 8 to level between 9.44 and 10.84, Real Voltage: 9.87 Passed
Passed: Button: Babb 5-8 -> GroupFade, Fade Type: Rate-(1)(P), Group Number: 2-(2)(P), Fade Time/Rate: 5.0-(5)(P), 
 Passed
Sliding Babb 13-16 to level 100 turns relay 13 to level between 4.25 and 5.65, Real Voltage: 4.58 Passed
Sliding Babb 13-16 to level 100 turns relay 14 to level between 6.32 and 7.72, Real Voltage: 6.84 Passed
Sliding Babb 13-16 to level 100 turns relay 15 to level between 7.22 and 8.62, Real Voltage: 7.73 Passed
Sliding Babb 13-16 to level 100 turns relay 16 to level between 8.19 and 9.59, Real Voltage: 8.84 Passed
Passed: Button: Babb 13-16 -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 127-(127)(P), Channel Number: 13-(13)(P), 
 Passed
Passed: Button: Babb 13-16 -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 179-(178)(P), Channel Number: 14-(14)(P), 
 Passed
Passed: Button: Babb 13-16 -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 204-(204)(P), Channel Number: 15-(15)(P), 
 Passed
Passed: Button: Babb 13-16 -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 229-(229)(P), Channel Number: 16-(16)(P), 
 Passed
= ==========


===========================================
Sapphire Regression Test: 2017-06-29 14:32
===========================================


EdisonTestCase
==============

= ==========

EdisonTestCase
==============
= ==========
A:On turns relay 14 to level between 9.44 and 10.84, Real Voltage: 9.82 Passed
Passed: Button: A:On -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 14-(14)(P), 
 Passed
A:Lower lowers channel 14 between -1.2v and -0.8v, real difference: -0.91v Passed
A:Lower lowers channel 14 between -1.2v and -0.8v, real difference: -0.99v Passed
Passed: Button: A:Lower -> ChannelFade, Relative Fade-(1)(P), Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 246-(244)(P), Channel Number: 14-(14)(P), 
		>Button: A:Lower -> ChannelFade, Relative Fade-(1)(P), Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 246-(244)(P), Channel Number: 14-(14)(P), 
 Passed
A:Raise raises channel 14 between 0.3v and 0.7v, real difference: 0.46v Passed
A:Raise raises channel 14 between 0.3v and 0.7v, real difference: 0.46v Passed
Passed: Button: A:Raise -> ChannelFade, Relative Fade-(1)(P), Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 5-(5)(P), Channel Number: 14-(14)(P), 
		>Button: A:Raise -> ChannelFade, Relative Fade-(1)(P), Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 5-(5)(P), Channel Number: 14-(14)(P), 
 Passed
A:Off turns relay 14 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.13 Passed
Passed: Button: A:Off -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 14-(14)(P), 
 Passed
B:On turns relay 14 to level between 9.44 and 10.84, Real Voltage: 9.82 Passed
B:On turns relay 15 to level between 9.44 and 10.84, Real Voltage: 9.65 Passed
B:On turns relay 16 to level between 9.44 and 10.84, Real Voltage: 9.80 Passed
Passed: Button: B:On -> GroupFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Group Number: 3-(3)(P), Fade Time/Rate: 1.0-(1)(P), 
 Passed
Passed: Button: B:On -> GroupFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Group Number: 6-(6)(P), Fade Time/Rate: 1.0-(1)(P), 
 Passed
B:Lower lowers channel 14 between -1.2v and -0.8v, real difference: -0.91v Passed
B:Lower lowers channel 14 between -1.2v and -0.8v, real difference: -0.99v Passed
B:Lower lowers channel 15 between -1.2v and -0.8v, real difference: -0.98v Passed
B:Lower lowers channel 15 between -1.2v and -0.8v, real difference: -0.98v Passed
B:Lower lowers channel 16 between -1.2v and -0.8v, real difference: -1.02v Passed
B:Lower lowers channel 16 between -1.2v and -0.8v, real difference: -1.03v Passed
Passed: Button: B:On -> GroupFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Group Number: 3-(3)(P), Fade Time/Rate: 1.0-(1)(P), 
 Passed
Passed: Button: B:On -> GroupFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Group Number: 6-(6)(P), Fade Time/Rate: 1.0-(1)(P), 
 Passed
B:Raise raises channel 14 between 0.3v and 0.7v, real difference: 0.47v Passed
B:Raise raises channel 14 between 0.3v and 0.7v, real difference: 0.47v Passed
B:Raise raises channel 15 between 0.3v and 0.7v, real difference: 0.46v Passed
B:Raise raises channel 15 between 0.3v and 0.7v, real difference: 0.46v Passed
B:Raise raises channel 16 between 0.3v and 0.7v, real difference: 0.47v Passed
B:Raise raises channel 16 between 0.3v and 0.7v, real difference: 0.46v Passed
Passed: Button: B:On -> GroupFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Group Number: 3-(3)(P), Fade Time/Rate: 1.0-(1)(P), 
 Passed
Passed: Button: B:On -> GroupFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Group Number: 6-(6)(P), Fade Time/Rate: 1.0-(1)(P), 
 Passed
B:Off turns relay 14 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.13 Passed
B:Off turns relay 15 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.02 Passed
B:Off turns relay 16 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.19 Passed
Passed: Button: B:On -> GroupFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Group Number: 3-(3)(P), Fade Time/Rate: 1.0-(1)(P), 
 Passed
Passed: Button: B:On -> GroupFade, Absolute Fade-(0)(P), Fade Type: Time-(0)(P), Group Number: 6-(6)(P), Fade Time/Rate: 1.0-(1)(P), 
 Passed
= ==========


===========================================
Sapphire Regression Test: 2017-06-29 14:38
===========================================


KeplerTestCase
==============

= ==========

KeplerTestCase
==============
= ==========
Button All On turns relay 1 to state 1 Passed
Button All On turns relay 2 to state 1 Passed
Button All On turns relay 3 to state 1 Passed
Button All On turns relay 4 to state 1 Passed
Button All On turns relay 9 to state 1 Passed
Button All On turns relay 10 to state 1 Passed
Button All On turns relay 11 to state 1 Passed
Button All On turns relay 12 to state 1 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 2-(2)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 3-(3)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 4-(4)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 6-(6)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 8-(8)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 9-(9)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 10-(10)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 11-(11)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 12-(12)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 13-(13)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 14-(14)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 15-(15)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 255-(255)(P), Channel Number: 16-(16)(P), 
 Passed
All On turns relay 5 to level between 9.44 and 10.84, Real Voltage: 9.84 Passed
All On turns relay 6 to level between 9.44 and 10.84, Real Voltage: 9.91 Passed
All On turns relay 7 to level between 9.44 and 10.84, Real Voltage: 9.98 Passed
All On turns relay 8 to level between 9.44 and 10.84, Real Voltage: 9.87 Passed
All On turns relay 13 to level between 9.44 and 10.84, Real Voltage: 9.75 Passed
All On turns relay 14 to level between 9.44 and 10.84, Real Voltage: 9.82 Passed
All On turns relay 15 to level between 9.44 and 10.84, Real Voltage: 9.66 Passed
All On turns relay 16 to level between 9.44 and 10.84, Real Voltage: 9.82 Passed
Button All Off turns relay 1 to state 0 Passed
Button All Off turns relay 2 to state 0 Passed
Button All Off turns relay 3 to state 0 Passed
Button All Off turns relay 4 to state 0 Passed
Button All Off turns relay 9 to state 0 Passed
Button All Off turns relay 10 to state 0 Passed
Button All Off turns relay 11 to state 0 Passed
Button All Off turns relay 12 to state 0 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 2-(2)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 3-(3)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 4-(4)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 6-(6)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 8-(8)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 9-(9)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 10-(10)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 11-(11)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 12-(12)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 13-(13)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 14-(14)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 15-(15)(P), 
 Passed
Passed: Button: All On -> ChannelFade, Fade Type: Time-(0)(P), Fade Time/Rate: 1.0-(1)(P), Target Level: 0-(0)(P), Channel Number: 16-(16)(P), 
 Passed
All Off turns relay 5 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.02 Passed
All Off turns relay 6 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 13.90 Passed
All Off turns relay 7 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.13 Passed
All Off turns relay 8 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.37 Passed
All Off turns relay 13 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.04 Passed
All Off turns relay 14 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.14 Passed
All Off turns relay 15 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.02 Passed
All Off turns relay 16 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.20 Passed
Button Group 1 turns relay 1 to state 1 Passed
Button Group 1 turns relay 2 to state 1 Passed
Button Group 1 turns relay 3 to state 1 Passed
Button Group 1 turns relay 4 to state 1 Passed
Button Group 1 turns relay 9 to state 1 Passed
Button Group 1 turns relay 10 to state 1 Passed
Button Group 1 turns relay 11 to state 1 Passed
Button Group 1 turns relay 12 to state 1 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 255-(255)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 255-(255)(P), Channel Number: 2-(2)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 255-(255)(P), Channel Number: 3-(3)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 255-(255)(P), Channel Number: 4-(4)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 255-(255)(P), Channel Number: 9-(9)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 255-(255)(P), Channel Number: 10-(10)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 255-(255)(P), Channel Number: 11-(11)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 255-(255)(P), Channel Number: 12-(12)(P), 
 Passed
Button Group 1 turns relay 1 to state 0 Passed
Button Group 1 turns relay 2 to state 0 Passed
Button Group 1 turns relay 3 to state 0 Passed
Button Group 1 turns relay 4 to state 0 Passed
Button Group 1 turns relay 9 to state 0 Passed
Button Group 1 turns relay 10 to state 0 Passed
Button Group 1 turns relay 11 to state 0 Passed
Button Group 1 turns relay 12 to state 0 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 0-(0)(P), Channel Number: 1-(1)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 0-(0)(P), Channel Number: 2-(2)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 0-(0)(P), Channel Number: 3-(3)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 0-(0)(P), Channel Number: 4-(4)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 0-(0)(P), Channel Number: 9-(9)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 0-(0)(P), Channel Number: 10-(10)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 0-(0)(P), Channel Number: 11-(11)(P), 
 Passed
Passed: Button: Group 1 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Level: 0-(0)(P), Channel Number: 12-(12)(P), 
 Passed
Group 2 turns relay 5 to level between 9.44 and 10.84, Real Voltage: 9.83 Passed
Group 2 turns relay 6 to level between 9.44 and 10.84, Real Voltage: 9.91 Passed
Group 2 turns relay 7 to level between 9.44 and 10.84, Real Voltage: 9.96 Passed
Group 2 turns relay 8 to level between 9.44 and 10.84, Real Voltage: 9.86 Passed
Group 2 turns relay 13 to level between 9.44 and 10.84, Real Voltage: 9.75 Passed
Group 2 turns relay 14 to level between 9.44 and 10.84, Real Voltage: 9.82 Passed
Group 2 turns relay 15 to level between 9.44 and 10.84, Real Voltage: 9.65 Passed
Group 2 turns relay 16 to level between 9.44 and 10.84, Real Voltage: 9.81 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 6-(6)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 8-(8)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 13-(13)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 14-(14)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 15-(15)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 16-(16)(P), 
 Passed
Group 2 turns relay 5 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.01 Passed
Group 2 turns relay 6 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 13.89 Passed
Group 2 turns relay 7 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.13 Passed
Group 2 turns relay 8 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.37 Passed
Group 2 turns relay 13 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.03 Passed
Group 2 turns relay 14 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.13 Passed
Group 2 turns relay 15 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.02 Passed
Group 2 turns relay 16 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.19 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 6-(6)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 8-(8)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 13-(13)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 14-(14)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 15-(15)(P), 
 Passed
Passed: Button: Group 2 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 16-(16)(P), 
 Passed
Group 3 turns relay 5 to level between 9.44 and 10.84, Real Voltage: 9.84 Passed
Group 3 turns relay 6 to level between 9.44 and 10.84, Real Voltage: 9.92 Passed
Passed: Button: Group 3 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 255-(255)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Group 3 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 255-(255)(P), Channel Number: 6-(6)(P), 
 Passed
Group 3 turns relay 5 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.01 Passed
Group 3 turns relay 6 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 13.89 Passed
Passed: Button: Group 3 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(0)(P), Channel Number: 5-(5)(P), 
 Passed
Passed: Button: Group 3 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(0)(P), Channel Number: 6-(6)(P), 
 Passed
Group 4 turns relay 7 to level between 9.44 and 10.84, Real Voltage: 9.96 Passed
Group 4 turns relay 8 to level between 9.44 and 10.84, Real Voltage: 9.86 Passed
Passed: Button: Group 4 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 7-(7)(P), 
		>Button: Group 4 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: Group 4 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 8-(8)(P), 
		>Button: Group 4 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 255-(255)(P), Channel Number: 8-(8)(P), 
 Passed
Group 4 turns relay 7 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.13 Passed
Group 4 turns relay 8 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.37 Passed
Passed: Button: Group 4 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 0-(0)(P), Channel Number: 7-(7)(P), 
		>Button: Group 4 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 0-(0)(P), Channel Number: 7-(7)(P), 
 Passed
Passed: Button: Group 4 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 0-(0)(P), Channel Number: 8-(8)(P), 
		>Button: Group 4 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 3.0-(3)(P), Target Level: 0-(0)(P), Channel Number: 8-(8)(P), 
 Passed
Group 5 turns relay 13 to level between 9.44 and 10.84, Real Voltage: 9.76 Passed
Group 5 turns relay 14 to level between 9.44 and 10.84, Real Voltage: 9.82 Passed
Passed: Button: Group 5 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 255-(255)(P), Channel Number: 13-(13)(P), 
 Passed
Passed: Button: Group 5 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 255-(255)(P), Channel Number: 14-(14)(P), 
 Passed
Group 5 turns relay 13 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.03 Passed
Group 5 turns relay 14 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.13 Passed
Passed: Button: Group 5 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 0-(0)(P), Channel Number: 13-(13)(P), 
 Passed
Passed: Button: Group 5 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 15.0-(15)(P), Target Level: 0-(0)(P), Channel Number: 14-(14)(P), 
 Passed
Group 6 turns relay 14 to level between 9.44 and 10.84, Real Voltage: 9.83 Passed
Group 6 turns relay 15 to level between 9.44 and 10.84, Real Voltage: 9.66 Passed
Group 6 turns relay 16 to level between 9.44 and 10.84, Real Voltage: 9.82 Passed
Passed: Button: Group 6 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 20.0-(20)(P), Target Level: 255-(255)(P), Channel Number: 14-(14)(P), 
 Passed
Passed: Button: Group 6 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 20.0-(20)(P), Target Level: 255-(255)(P), Channel Number: 15-(15)(P), 
 Passed
Passed: Button: Group 6 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 20.0-(20)(P), Target Level: 255-(255)(P), Channel Number: 16-(16)(P), 
 Passed
Group 6 turns relay 14 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.13 Passed
Group 6 turns relay 15 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.01 Passed
Group 6 turns relay 16 to level between 13.3 and 14.7 or between 0.02 and 1.42, Real Voltage: 14.19 Passed
Passed: Button: Group 6 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 20.0-(20)(P), Target Level: 0-(0)(P), Channel Number: 14-(14)(P), 
 Passed
Passed: Button: Group 6 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 20.0-(20)(P), Target Level: 0-(0)(P), Channel Number: 15-(15)(P), 
 Passed
Passed: Button: Group 6 -> ChannelStatusReplyOrPublish, Fade Type: Time-(0)(P), Fade Time/Rate: 20.0-(20)(P), Target Level: 0-(0)(P), Channel Number: 16-(16)(P), 
 Passed
= ==========


===========================================
Sapphire Regression Test: 2017-06-29 14:41
===========================================


EditModeTestCase
================

= ==========

EditModeTestCase
================
= ==========
Changed button A1 to 100 via Edit Mode Passed
Changed button Babb 13 to 100 via Edit Mode Passed
= ==========


===========================================
Sapphire Regression Test: 2017-06-29 14:44
===========================================


NewtonTestCase
==============

=================================================================================================================================================================== ==========

NewtonTestCase
==============
=================================================================================================================================================================== ==========
Button A1 turns relay 1 to state 1                                                                                                                                  Failed
Failed:Button: A1 -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Rate-(0)(F), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(255)(F), Channel Number: 1-(1)(P), 
 Failed
Failed:Button: A1 -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Rate-(0)(F), Fade Time/Rate: 10.0-(10)(P), Target Level: 0-(0)(P), Channel Number: 1-(1)(P), 
   Failed
Button A1 turns relay 1 to state 0                                                                                                                                  Passed
Button A2 turns relay 1 to state 1                                                                                                                                  Passed
Passed: Button: A2 -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 1-(1)(P), 
 Passed
Button A2 turns relay 1 to state 0                                                                                                                                  Passed
Passed: Button: A2 -> ChannelFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 1-(1)(P), 
    Passed
Button B1 turns relay 2 to state 1                                                                                                                                  Passed
Button B1 turns relay 3 to state 1                                                                                                                                  Passed
Passed: Button: B1 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 2-(2)(P), 
                      Passed
Passed: Button: B1 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 3-(3)(P), 
                      Passed
Button B1 turns relay 2 to state 0                                                                                                                                  Passed
Button B1 turns relay 3 to state 0                                                                                                                                  Passed
Passed: Button: B1 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 2-(2)(P), 
                          Passed
Passed: Button: B1 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 3-(3)(P), 
                          Passed
Button B2 turns relay 2 to state 1                                                                                                                                  Passed
Button B2 turns relay 3 to state 1                                                                                                                                  Passed
Passed: Button: B2 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 2-(2)(P), 
                      Passed
Passed: Button: B2 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 255-(255)(P), Channel Number: 3-(3)(P), 
                      Passed
Button B2 turns relay 2 to state 0                                                                                                                                  Passed
Button B2 turns relay 3 to state 0                                                                                                                                  Passed
Passed: Button: B2 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 2-(2)(P), 
                          Passed
Passed: Button: B2 -> ChannelFade, Fade Type: Rate-(1)(P), Fade Time/Rate: 5.0-(5)(P), Target Level: 0-(0)(P), Channel Number: 3-(3)(P), 
                          Passed
Button C1 turns relay 1 to state 1                                                                                                                                  Passed
Button C1 turns relay 2 to state 1                                                                                                                                  Passed
Button C1 turns relay 3 to state 1                                                                                                                                  Passed
Button C1 turns relay 4 to state 1                                                                                                                                  Passed
Button C1 turns relay 9 to state 1                                                                                                                                  Passed
Button C1 turns relay 10 to state 1                                                                                                                                 Passed
Button C1 turns relay 11 to state 1                                                                                                                                 Passed
Button C1 turns relay 12 to state 1                                                                                                                                 Passed
Passed: Button: C1 -> GroupFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Group Number: 1-(1)(P), Fade Time/Rate: 5.0-(5)(P), 
                                Passed
Button C1 turns relay 1 to state 0                                                                                                                                  Passed
Button C1 turns relay 2 to state 0                                                                                                                                  Passed
Button C1 turns relay 3 to state 0                                                                                                                                  Passed
Button C1 turns relay 4 to state 0                                                                                                                                  Passed
Button C1 turns relay 9 to state 0                                                                                                                                  Passed
Button C1 turns relay 10 to state 0                                                                                                                                 Passed
Button C1 turns relay 11 to state 0                                                                                                                                 Passed
Button C1 turns relay 12 to state 0                                                                                                                                 Passed
Passed: Button: C1 -> GroupFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Group Number: 1-(1)(P), Fade Time/Rate: 5.0-(5)(P), 
                                Passed
Button C2 turns relay 1 to state 1                                                                                                                                  Passed
Button C2 turns relay 2 to state 1                                                                                                                                  Passed
Button C2 turns relay 3 to state 1                                                                                                                                  Passed
Button C2 turns relay 4 to state 1                                                                                                                                  Passed
Button C2 turns relay 9 to state 1                                                                                                                                  Passed
Button C2 turns relay 10 to state 1                                                                                                                                 Passed
Button C2 turns relay 11 to state 1                                                                                                                                 Passed
Button C2 turns relay 12 to state 1                                                                                                                                 Passed
Passed: Button: C2 -> GroupFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Group Number: 1-(1)(P), Fade Time/Rate: 5.0-(5)(P), 
                                Passed
Button C2 turns relay 1 to state 0                                                                                                                                  Passed
Button C2 turns relay 2 to state 0                                                                                                                                  Passed
Button C2 turns relay 3 to state 0                                                                                                                                  Passed
Button C2 turns relay 4 to state 0                                                                                                                                  Passed
Button C2 turns relay 9 to state 0                                                                                                                                  Passed
Button C2 turns relay 10 to state 0                                                                                                                                 Passed
Button C2 turns relay 11 to state 0                                                                                                                                 Passed
Button C2 turns relay 12 to state 0                                                                                                                                 Passed
Passed: Button: C2 -> GroupFade, Absolute Fade-(0)(P), Fade Type: Rate-(1)(P), Group Number: 1-(1)(P), Fade Time/Rate: 5.0-(5)(P), 
                                Passed
=================================================================================================================================================================== ==========


===========================================
Sapphire Regression Test: 2017-07-07 07:53
===========================================


CurieTestCase
=============

========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========

CurieTestCase
=============
========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[3300:F], Fade Time/Rate: 22036.0-[22036:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: Ch 32454 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[32454:F], Fade Time/Rate: 3552.0-[3552:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: Ch 13176 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[13176:F], Fade Time/Rate: 90.0-[90:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: Ch 3621 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[3621:F], Fade Time/Rate: 43700.0-[44916:F], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: Ch 4605 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[4605:F], Fade Time/Rate: 9220.0-[9220:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: Ch 1400 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[1400:F], Fade Time/Rate: 255.0-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: Ch 26583 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[26583:F], Fade Time/Rate: 44916.0-[44916:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           Failed
Failed:Button: Ch 500 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[500:F], Fade Time/Rate: 65012.0-[65012:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: Ch 4362 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[4362:F], Fade Time/Rate: 58612.0-[58612:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: Ch 21818 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[21818:F], Fade Time/Rate: 15316.0-[15316:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[3300:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[32454:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[13176:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[3621:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[4605:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[1400:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[26583:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[500:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[4362:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[21818:F], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[82:P], Fade Time/Rate: 1700.0-[5:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[3300:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[32454:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[13176:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[3621:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[4605:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[1400:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[26583:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[500:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[4362:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[21818:F], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[0:F], Channel Number: 82-[82:P], Fade Time/Rate: 1700.0-[10:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 13176-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3621-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4605-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 1400-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 26583-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 500-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 4362-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 21818-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3300-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 32454-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 13176-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 3621-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4605-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 1400-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 26583-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 500-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 4362-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 227-[191:F], Channel Number: 21818-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[3300:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[32454:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[13176:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[3621:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[4605:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[1400:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[26583:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[500:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                                         Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[4362:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[21818:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3300-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 32454-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 13176-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 3621-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4605-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 1400-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 26583-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 500-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 4362-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Target Level: 56-[63:F], Channel Number: 21818-[82:F], Fade Time/Rate: 1.0-[0:F], Fade Type: Time-[0:P], 
                                                   Failed
Passed: Button: Ch 82 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 82-[82:P], Fade Time/Rate: 1700.0-[1700:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Passed
========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 07:59
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Fade Time/Rate: 22036.0-[22036:P], Channel Number: 3300-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Fade Time/Rate: 3552.0-[3552:P], Channel Number: 32454-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 08:01
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 3300-[3300:F], Fade Time/Rate: 22036.0-[22036:P], Fade Type: Time-[0:P], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Target Level: 255-[255:P], Channel Number: 32454-[32454:F], Fade Time/Rate: 3552.0-[3552:P], Fade Type: Time-[0:P], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 08:28
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Target Level: 255-[255:P], Fade Type: Time-[0:P], Channel Number: 3300-[3300:F], Fade Time/Rate: 22036.0-[22036:P], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Target Level: 255-[255:P], Fade Type: Time-[0:P], Channel Number: 32454-[32454:F], Fade Time/Rate: 3552.0-[3552:P], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 08:30
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Target Level: 255-[100:F], Fade Time/Rate: 22036.0-[22036:P], Fade Type: Time-[0:P], Channel Number: 3300-[3300:F], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Target Level: 255-[100:F], Fade Time/Rate: 3552.0-[3552:P], Fade Type: Time-[0:P], Channel Number: 32454-[32454:F], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 08:32
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Target Level: 255-[100:F], Channel Number: 3300-[3300:F], Fade Time/Rate: 22036.0-[22036:P], Fade Type: Time-[0:P], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Target Level: 255-[100:F], Channel Number: 32454-[32454:F], Fade Time/Rate: 3552.0-[3552:P], Fade Type: Time-[0:P], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 08:34
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Target Level: 255-[100:F], Channel Number: 3300-[3300:F], Fade Type: Time-[0:P], Fade Time/Rate: 22036.0-[22036:P], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Target Level: 255-[100:F], Channel Number: 32454-[32454:F], Fade Type: Time-[0:P], Fade Time/Rate: 3552.0-[3552:P], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 08:38
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Fade Type: Time-[0:P], Target Level: 255-[255:P], Fade Time/Rate: 22036.0-[22036:P], Channel Number: 3300-[3300:F], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Fade Type: Time-[0:P], Target Level: 255-[255:P], Fade Time/Rate: 3552.0-[3552:P], Channel Number: 32454-[32454:F], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 08:39
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3300-[3300:F], Fade Time/Rate: 22036.0-[22036:P], Target Level: 255-[255:P], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 32454-[32454:F], Fade Time/Rate: 3552.0-[3552:P], Target Level: 255-[255:P], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 08:42
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Fade Time/Rate: 22036.0-[22036:P], Target Level: 255-[255:P], Channel Number: 3300-[3300:F], Fade Type: Time-[0:P], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Fade Time/Rate: 3552.0-[3552:P], Target Level: 255-[255:P], Channel Number: 32454-[32454:F], Fade Type: Time-[0:P], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 08:55
===========================================


CurieTestCase
=============

============================================================================================================================================================= ==========

CurieTestCase
=============
============================================================================================================================================================= ==========
Failed:Button: Ch 3300 -> ChannelFade, Fade Time/Rate: 22036.0-[22036:P], Channel Number: 3300-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
  Failed
Failed:Button: Ch 32454 -> ChannelFade, Fade Time/Rate: 3552.0-[3552:P], Channel Number: 32454-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
 Failed
============================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 09:45
===========================================


CurieTestCase
=============

============================================================================================================================================================ ==========

CurieTestCase
=============
============================================================================================================================================================ ==========
Failed:Button: Ch 3300 -> ChannelFade, Fade Type: Time-[0:P], Fade Time/Rate: 22036.0-[22036:P], Channel Number: 3300-[3300:F], Target Level: 255-[255:P], 
 Failed
Failed:
                                                                                                                                                     Failed
============================================================================================================================================================ ==========


===========================================
Sapphire Regression Test: 2017-07-07 10:28
===========================================


CurieTestCase
=============

= ==========

CurieTestCase
=============
= ==========
Passed: Button: Ch 3300 -> ChannelFade, Channel Number: 3300-[3300:P], Fade Time/Rate: 22036.0-[22036:P], Fade Type: Time-[0:P], Target Level: 255-[255:P], 
 Passed
Passed: Button: Ch 32454 -> ChannelFade, Channel Number: 32454-[32454:P], Fade Time/Rate: 3552.0-[3552:P], Fade Type: Time-[0:P], Target Level: 255-[255:P], 
 Passed
= ==========


===========================================
Sapphire Regression Test: 2017-07-07 10:37
===========================================


CurieTestCase
=============

========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========

CurieTestCase
=============
========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========
Failed:Button: Ch 3621 -> ChannelFade, Channel Number: 3621-[3621:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 43700.0-[44916:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[500:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[82:P], Target Level: 255-[255:P], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[5:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[3300:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[32454:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[13176:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[3621:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[4605:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[1400:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[26583:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[500:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[4362:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[21818:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[82:P], Target Level: 255-[0:F], Fade Type: Time-[1:F], Fade Time/Rate: 1700.0-[10:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[3300:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[3300:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[32454:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[32454:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[13176:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[13176:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[3621:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[3621:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[4605:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[4605:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[1400:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[1400:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[26583:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[26583:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[500:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[500:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[500:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[500:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[500:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[500:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[500:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[500:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[500:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[500:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[4362:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[4362:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[21818:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[21818:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[82:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[3300:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[3300:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[3300:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[3300:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[3300:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[3300:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[3300:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[3300:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[3300:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[3300:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[32454:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[32454:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[32454:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[32454:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[32454:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[32454:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[32454:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[32454:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[32454:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[32454:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[13176:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[13176:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[13176:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[13176:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[13176:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[13176:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[13176:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[13176:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[13176:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[13176:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[3621:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[3621:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[3621:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[3621:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[3621:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[3621:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[3621:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[3621:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[3621:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[3621:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[4605:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[4605:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[4605:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[4605:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[4605:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[4605:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[4605:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[4605:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[4605:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[4605:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[1400:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[1400:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[1400:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[1400:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[1400:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[1400:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[1400:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[1400:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[1400:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[1400:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[26583:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[26583:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[26583:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[26583:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[26583:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[26583:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[26583:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[26583:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[26583:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[26583:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[500:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[500:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[500:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[500:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[500:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[500:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[500:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[500:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[500:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[500:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[4362:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[4362:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[4362:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[4362:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[4362:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[4362:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[4362:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[4362:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[4362:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[4362:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[21818:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[21818:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[21818:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[21818:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[21818:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[21818:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[21818:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[21818:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[21818:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[21818:P], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[82:F], Target Level: 229-[191:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[3300:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[3300:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[3300:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[3300:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[3300:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[3300:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[3300:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[3300:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[3300:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[3300:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[32454:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[32454:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[32454:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[32454:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[32454:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[32454:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[32454:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[32454:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[32454:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[32454:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[13176:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[13176:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[13176:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[13176:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[13176:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[13176:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[13176:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[13176:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[13176:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[13176:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[3621:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[3621:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[3621:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[3621:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[3621:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[3621:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[3621:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[3621:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[3621:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[3621:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[4605:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[4605:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[4605:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[4605:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[4605:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[4605:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[4605:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[4605:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[4605:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[4605:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[1400:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[1400:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[1400:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[1400:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[1400:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[1400:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[1400:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[1400:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[1400:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[1400:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[26583:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[26583:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[26583:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[26583:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[26583:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[26583:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[26583:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[26583:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[26583:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[26583:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[500:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[500:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[500:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[500:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[500:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[500:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[500:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[500:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[500:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[500:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                                         Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[4362:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[4362:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[4362:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[4362:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[4362:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[4362:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[4362:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[4362:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[4362:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[4362:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[21818:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[21818:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[21818:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[21818:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[21818:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[21818:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[21818:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[21818:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[21818:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[21818:P], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[82:F], Target Level: 56-[63:F], Fade Type: Time-[0:P], Fade Time/Rate: 1.0-[0:F], 
                                                   Failed
Passed: Button: Ch 3300 -> ChannelFade, Channel Number: 3300-[3300:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 22036.0-[22036:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Passed
Passed: Button: Ch 32454 -> ChannelFade, Channel Number: 32454-[32454:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 3552.0-[3552:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
Passed: Button: Ch 13176 -> ChannelFade, Channel Number: 13176-[13176:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 90.0-[90:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: Ch 4605 -> ChannelFade, Channel Number: 4605-[4605:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 9220.0-[9220:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Passed
Passed: Button: Ch 1400 -> ChannelFade, Channel Number: 1400-[1400:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 255.0-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Passed
Passed: Button: Ch 26583 -> ChannelFade, Channel Number: 26583-[26583:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 44916.0-[44916:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Passed
Passed: Button: Ch 500 -> ChannelFade, Channel Number: 500-[500:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 65012.0-[65012:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: Ch 4362 -> ChannelFade, Channel Number: 4362-[4362:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 58612.0-[58612:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Passed
Passed: Button: Ch 21818 -> ChannelFade, Channel Number: 21818-[21818:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 15316.0-[15316:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Passed
Passed: Button: Ch 82 -> ChannelFade, Channel Number: 82-[82:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], Fade Time/Rate: 1700.0-[1700:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Passed
========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 10:40
===========================================


CurieTestCase
=============

========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========

CurieTestCase
=============
========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========
Failed:Button: Ch 3621 -> ChannelFade, Fade Time/Rate: 43700.0-[44916:F], Target Level: 255-[255:P], Channel Number: 3621-[3621:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: Ch 26583 -> ChannelFade, Fade Time/Rate: 44916.0-[44916:P], Target Level: 0-[255:F], Channel Number: 26583-[26583:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: Ch 500 -> ChannelFade, Fade Time/Rate: 65012.0-[65012:P], Target Level: 0-[255:F], Channel Number: 500-[500:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Failed
Failed:Button: Ch 4362 -> ChannelFade, Fade Time/Rate: 58612.0-[58612:P], Target Level: 0-[255:F], Channel Number: 4362-[4362:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: Ch 21818 -> ChannelFade, Fade Time/Rate: 15316.0-[15316:P], Target Level: 0-[255:F], Channel Number: 21818-[21818:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: Ch 82 -> ChannelFade, Fade Time/Rate: 1700.0-[1700:P], Target Level: 0-[255:F], Channel Number: 82-[82:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[3300:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[32454:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[13176:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[3621:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[4605:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[1400:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[26583:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[500:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[4362:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[21818:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], Channel Number: 82-[82:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[3300:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[32454:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[13176:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[3621:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[4605:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[1400:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[26583:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[500:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[4362:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[21818:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], Channel Number: 82-[82:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 3300-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 32454-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 13176-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 3621-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 4605-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 1400-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 26583-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 500-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 4362-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 21818-[82:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[3300:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[3300:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[32454:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[32454:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[13176:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[13176:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[3621:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[3621:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[4605:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[4605:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[1400:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[1400:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[26583:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[26583:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[500:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[500:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[4362:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[4362:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[21818:P], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3300-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 32454-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 13176-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 3621-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4605-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 1400-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 26583-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 500-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 4362-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 229-[191:F], Channel Number: 21818-[82:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[3300:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[3300:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[3300:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[32454:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[32454:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[32454:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[13176:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[13176:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[13176:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[3621:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[3621:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[3621:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[4605:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[4605:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[4605:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[1400:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[1400:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[1400:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[26583:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[26583:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[26583:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[500:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[500:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[500:F], Fade Type: Time-[0:P], 
                                         Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[4362:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[4362:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[4362:F], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[21818:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[21818:P], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3300-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 32454-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 13176-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 3621-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4605-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 1400-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 26583-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 500-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 4362-[82:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 56-[63:F], Channel Number: 21818-[82:F], Fade Type: Time-[0:P], 
                                                   Failed
Passed: Button: Ch 3300 -> ChannelFade, Fade Time/Rate: 22036.0-[22036:P], Target Level: 255-[255:P], Channel Number: 3300-[3300:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Passed
Passed: Button: Ch 32454 -> ChannelFade, Fade Time/Rate: 3552.0-[3552:P], Target Level: 255-[255:P], Channel Number: 32454-[32454:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
Passed: Button: Ch 13176 -> ChannelFade, Fade Time/Rate: 90.0-[90:P], Target Level: 255-[255:P], Channel Number: 13176-[13176:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: Ch 4605 -> ChannelFade, Fade Time/Rate: 9220.0-[9220:P], Target Level: 255-[255:P], Channel Number: 4605-[4605:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Passed
Passed: Button: Ch 1400 -> ChannelFade, Fade Time/Rate: 255.0-[255:P], Target Level: 255-[255:P], Channel Number: 1400-[1400:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 3300-[3300:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 32454-[32454:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 13176-[13176:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 3621-[3621:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 4605-[4605:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 1400-[1400:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 26583-[26583:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 500-[500:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 4362-[4362:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Channel Number: 21818-[21818:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 11:26
===========================================


CurieTestCase
=============

========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========

CurieTestCase
=============
========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========
Failed:Button: Ch 3621 -> ChannelFade, Channel Number: 3621-[3621:P], Fade Time/Rate: 43700.0-[44916:F], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[3300:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[32454:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[13176:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[3621:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[4605:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[1400:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[26583:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[500:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[4362:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[21818:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[82:P], Fade Time/Rate: 1700.0-[5:F], Target Level: 255-[255:P], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[3300:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[32454:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[13176:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[3621:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[4605:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[1400:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[26583:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[500:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[4362:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[21818:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 82-[82:P], Fade Time/Rate: 1700.0-[10:F], Target Level: 255-[0:F], Fade Type: Time-[1:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                               Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[3300:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[3300:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[3300:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[3300:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[3300:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[3300:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[3300:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[3300:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[3300:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[3300:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[32454:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[32454:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[32454:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[32454:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[32454:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[32454:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[32454:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[32454:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[32454:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[32454:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[13176:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[13176:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[13176:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[13176:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[13176:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[13176:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[13176:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[13176:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[13176:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[13176:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[3621:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[3621:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[3621:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[3621:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[3621:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[3621:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[3621:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[3621:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[3621:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[3621:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[4605:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[4605:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[4605:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[4605:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[4605:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[4605:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[4605:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[4605:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[4605:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[4605:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[1400:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[1400:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[1400:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[1400:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[1400:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[1400:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[1400:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[1400:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[1400:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[1400:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[26583:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[26583:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[26583:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[26583:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[26583:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[26583:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[26583:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[26583:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[26583:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[26583:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[500:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[500:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[500:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[500:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[500:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[500:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[500:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[500:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[500:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[500:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[4362:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[4362:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[4362:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[4362:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[4362:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[4362:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[4362:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[4362:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[4362:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[4362:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
           Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[21818:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[21818:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[21818:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[21818:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[21818:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[21818:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[21818:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[21818:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[21818:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[21818:P], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 500-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 138-[127:F], Fade Type: Time-[0:P], 
                               Failed
Passed: Button: Ch 3300 -> ChannelFade, Channel Number: 3300-[3300:P], Fade Time/Rate: 22036.0-[22036:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Passed
Passed: Button: Ch 32454 -> ChannelFade, Channel Number: 32454-[32454:P], Fade Time/Rate: 3552.0-[3552:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
Passed: Button: Ch 13176 -> ChannelFade, Channel Number: 13176-[13176:P], Fade Time/Rate: 90.0-[90:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: Ch 4605 -> ChannelFade, Channel Number: 4605-[4605:P], Fade Time/Rate: 9220.0-[9220:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Passed
Passed: Button: Ch 1400 -> ChannelFade, Channel Number: 1400-[1400:P], Fade Time/Rate: 255.0-[255:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Passed
Passed: Button: Ch 26583 -> ChannelFade, Channel Number: 26583-[26583:P], Fade Time/Rate: 44916.0-[44916:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Passed
Passed: Button: Ch 500 -> ChannelFade, Channel Number: 500-[500:P], Fade Time/Rate: 65012.0-[65012:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: Ch 4362 -> ChannelFade, Channel Number: 4362-[4362:P], Fade Time/Rate: 58612.0-[58612:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Passed
Passed: Button: Ch 21818 -> ChannelFade, Channel Number: 21818-[21818:P], Fade Time/Rate: 15316.0-[15316:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Passed
Passed: Button: Ch 82 -> ChannelFade, Channel Number: 82-[82:P], Fade Time/Rate: 1700.0-[1700:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 3300-[3300:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 32454-[32454:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 13176-[13176:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 3621-[3621:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 4605-[4605:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 1400-[1400:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 26583-[26583:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 500-[500:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 4362-[4362:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Channel Number: 21818-[21818:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], Fade Type: Time-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Passed
========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================= ==========


===========================================
Sapphire Regression Test: 2017-07-07 11:32
===========================================


CurieTestCase
=============

=========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================== ==========

CurieTestCase
=============
=========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================== ==========
Failed:Button: Ch 3621 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3621-[3621:P], Fade Time/Rate: 43700.0-[44916:F], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Failed
Failed:Button: Ch 26583 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 26583-[26583:P], Fade Time/Rate: 44916.0-[44916:P], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: Ch 500 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 500-[500:P], Fade Time/Rate: 65012.0-[65012:P], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Failed
Failed:Button: Ch 4362 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4362-[4362:P], Fade Time/Rate: 58612.0-[58612:P], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: Ch 21818 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 21818-[21818:P], Fade Time/Rate: 15316.0-[15316:P], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Failed
Failed:Button: Ch 82 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 82-[82:P], Fade Time/Rate: 1700.0-[1700:P], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[3300:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[32454:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[13176:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[3621:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[4605:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[1400:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[26583:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[500:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[4362:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[21818:F], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[82:P], Fade Time/Rate: 1700.0-[5:F], Target Level: 0-[255:F], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[3300:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[32454:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[13176:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[3621:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[4605:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[1400:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[26583:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[500:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[4362:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[21818:F], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[1:F], Channel Number: 82-[82:P], Fade Time/Rate: 1700.0-[10:F], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3300-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 32454-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 13176-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3621-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4605-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 1400-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 26583-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 500-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4362-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 21818-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
 Failed
Failed:Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3300-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 32454-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 13176-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3621-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4605-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 1400-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 26583-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 500-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4362-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
	---
Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 21818-[82:F], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                         Failed
Passed: Button: Ch 3300 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3300-[3300:P], Fade Time/Rate: 22036.0-[22036:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Passed
Passed: Button: Ch 32454 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 32454-[32454:P], Fade Time/Rate: 3552.0-[3552:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: Ch 13176 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 13176-[13176:P], Fade Time/Rate: 90.0-[90:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Passed
Passed: Button: Ch 4605 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4605-[4605:P], Fade Time/Rate: 9220.0-[9220:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Passed
Passed: Button: Ch 1400 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 1400-[1400:P], Fade Time/Rate: 255.0-[255:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3300-[3300:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 32454-[32454:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 13176-[13176:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3621-[3621:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4605-[4605:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 1400-[1400:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 26583-[26583:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 500-[500:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4362-[4362:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 21818-[21818:P], Fade Time/Rate: 1.0-[1:P], Target Level: 255-[255:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3300-[3300:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 32454-[32454:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 13176-[13176:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 3621-[3621:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4605-[4605:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 1400-[1400:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 26583-[26583:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 500-[500:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 4362-[4362:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    Passed
Passed: Button: All Channels 1 -> ChannelFade, Fade Type: Time-[0:P], Channel Number: 21818-[21818:P], Fade Time/Rate: 1.0-[1:P], Target Level: 0-[0:P], 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  Passed
=========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================== ==========


===========================================
Sapphire Regression Test: 2017-07-10 12:46
===========================================


TeslaTestCase
=============

=================================================================== ==========

TeslaTestCase
=============
=================================================================== ==========
Failed:
                                                            Failed
DS turned relay 3 off                                               Failed
D turns relay 5 to level between 6.77 and 8.17, Real Voltage: -0.00 Failed
D turns relay 6 to level between 6.77 and 8.17, Real Voltage: -0.00 Failed
D turns relay 7 to level between 6.77 and 8.17, Real Voltage: -0.00 Failed
D turns relay 8 to level between 6.77 and 8.17, Real Voltage: -0.00 Failed
Button A turns relay 1 to state 1                                   Passed
Button A turns relay 2 to state 1                                   Passed
Passed: Button: A -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Fade Type: Rate-[1:P], Target Level: 255-[255:P], Target Level: 255-[1:P], 
		>Button: A -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Fade Type: Rate-[1:P], Target Level: 255-[255:P], Target Level: 255-[1:P], 
 Passed
Passed: Button: A -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Fade Type: Rate-[1:P], Target Level: 255-[255:P], Target Level: 255-[2:P], 
		>Button: A -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Fade Type: Rate-[1:P], Target Level: 255-[255:P], Target Level: 255-[2:P], 
 Passed
Passed: Button: GTDS_1 -> ChannelRelinquish, Fade Time/Rate: 5.0-[5:P], Fade Type: Rate-[1:P], Source Node: 33-[33:P], Channel Number: 1-[1:P], 
 Passed
Passed: Button: GTDS_2 -> ChannelRelinquish, Fade Time/Rate: 5.0-[5:P], Fade Type: Rate-[1:P], Source Node: 33-[33:P], Channel Number: 2-[2:P], 
 Passed
GTDS turned relay 1 off                                             Passed
GTDS turned relay 2 off                                             Passed
Button B turns relay 3 to state 1                                   Passed
Passed: Button: B -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Fade Type: Rate-[1:P], Target Level: 255-[255:P], Target Level: 255-[3:P], 
 Passed
Passed: Button: GTDS_4 -> ChannelFade, Fade Time/Rate: 5.0-[5:P], Fade Type: Rate-[1:P], Source Node: 33-[33:P], Target Level: 255-[255:P], Target Level: 255-[4:P], 
 Passed
DS turned relay 4 On                                                Passed
Passed: Button: GTDS_4 -> ChannelRelinquish, Fade Time/Rate: 5.0-[5:P], Fade Type: Rate-[1:P], Source Node: 33-[33:P], Channel Number: 4-[4:P], 
 Passed
DS turned relay 4 off                                               Passed
Passed: Button: D -> GroupFade, Fade Time/Rate: 3.0-[3:P], Fade Type: Rate-[1:P], Group Number: 2-[2:P], 
 Passed
=================================================================== ==========


===========================================
Sapphire Regression Test: 2017-07-10 12:52
===========================================


TeslaTestCase
=============

=================================================================== ==========

TeslaTestCase
=============
=================================================================== ==========
Failed:
                                                            Failed
Failed:
                                                            Failed
DS turned relay 3 off                                               Failed
DS turned relay 4 off                                               Failed
D turns relay 5 to level between 6.77 and 8.17, Real Voltage: -0.00 Failed
D turns relay 6 to level between 6.77 and 8.17, Real Voltage: -0.00 Failed
D turns relay 7 to level between 6.77 and 8.17, Real Voltage: -0.00 Failed
D turns relay 8 to level between 6.77 and 8.17, Real Voltage: -0.00 Failed
Button A turns relay 1 to state 1                                   Passed
Button A turns relay 2 to state 1                                   Passed
Passed: Button: A -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Target Level: 255-[255:P], Fade Type: Rate-[1:P], Fade Type: Rate-[1:P], 
		>Button: A -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Target Level: 255-[255:P], Fade Type: Rate-[1:P], Fade Type: Rate-[1:P], 
 Passed
Passed: Button: A -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Target Level: 255-[255:P], Fade Type: Rate-[1:P], Fade Type: Rate-[2:P], 
		>Button: A -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Target Level: 255-[255:P], Fade Type: Rate-[1:P], Fade Type: Rate-[2:P], 
 Passed
Passed: Button: GTDS_1 -> ChannelRelinquish, Fade Time/Rate: 5.0-[5:P], Source Node: 33-[33:P], Fade Type: Rate-[1:P], Channel Number: 1-[1:P], 
 Passed
Passed: Button: GTDS_2 -> ChannelRelinquish, Fade Time/Rate: 5.0-[5:P], Source Node: 33-[33:P], Fade Type: Rate-[1:P], Channel Number: 2-[2:P], 
 Passed
GTDS turned relay 1 off                                             Passed
GTDS turned relay 2 off                                             Passed
Button B turns relay 3 to state 1                                   Passed
Passed: Button: B -> ChannelFade, Fade Time/Rate: 3.0-[3:P], Target Level: 255-[255:P], Fade Type: Rate-[1:P], Fade Type: Rate-[3:P], 
 Passed
Passed: Button: GTDS_4 -> ChannelFade, Fade Time/Rate: 5.0-[5:P], Target Level: 255-[255:P], Source Node: 33-[33:P], Fade Type: Rate-[1:P], Fade Type: Rate-[4:P], 
 Passed
DS turned relay 4 On                                                Passed
Passed: Button: D -> GroupFade, Fade Time/Rate: 3.0-[3:P], Group Number: 2-[2:P], Fade Type: Rate-[1:P], 
 Passed
=================================================================== ==========


===========================================
Sapphire Regression Test: 2017-07-17 08:21
===========================================


UISettingsDateTimeTestCase
==========================

= ==========

UISettingsDateTimeTestCase
==========================
= ==========
= ==========


===========================================
Sapphire Regression Test: 2017-07-17 12:49
===========================================


UISettingsDateTimeTestCase
==========================

= ==========

UISettingsDateTimeTestCase
==========================
= ==========
7 is on screen Passed
17 is on screen Passed
2017 is on screen Passed
= ==========


===========================================
Sapphire Regression Test: 2017-07-17 12:51
===========================================


UISettingsDateTimeTestCase
==========================

= ==========

UISettingsDateTimeTestCase
==========================
= ==========
7 is on screen Passed
17 is on screen Passed
2017 is on screen Passed
= ==========


===========================================
Sapphire Regression Test: 2017-07-17 14:08
===========================================


UISettingsDateTimeTestCase
==========================

===================== ==========

UISettingsDateTimeTestCase
==========================
===================== ==========
14:07 is on screen    Failed
02:07 PM is on screen Failed
July is on screen     Passed
17 is on screen       Passed
2017 is on screen     Passed
US/Pacific Time zone is on screen Passed
Has correct sunrise time Passed
Has correct sunset time Passed
===================== ==========


===========================================
Sapphire Regression Test: 2017-07-17 14:11
===========================================


UISettingsDateTimeTestCase
==========================

===================== ==========

UISettingsDateTimeTestCase
==========================
===================== ==========
14:10 is on screen    Failed
02:10 PM is on screen Failed
July is on screen     Passed
17 is on screen       Passed
2017 is on screen     Passed
US/Pacific Time zone is on screen Passed
Has correct sunrise time Passed
Has correct sunset time Passed
===================== ==========


===========================================
Sapphire Regression Test: 2017-07-17 14:14
===========================================


UISettingsDateTimeTestCase
==========================

===================== ==========

UISettingsDateTimeTestCase
==========================
===================== ==========
02:13 PM is on screen Failed
July is on screen     Passed
17 is on screen       Passed
2017 is on screen     Passed
US/Pacific Time zone is on screen Passed
Has correct sunrise time Passed
Has correct sunset time Passed
===================== ==========


===========================================
Sapphire Regression Test: 2017-07-17 14:16
===========================================


UISettingsDateTimeTestCase
==========================

====================== ==========

UISettingsDateTimeTestCase
==========================
====================== ==========
02:15  PM is on screen Failed
July is on screen      Passed
17 is on screen        Passed
2017 is on screen      Passed
US/Pacific Time zone is on screen Passed
Has correct sunrise time Passed
Has correct sunset time Passed
====================== ==========


===========================================
Sapphire Regression Test: 2017-07-17 14:20
===========================================


UISettingsDateTimeTestCase
==========================

= ==========

UISettingsDateTimeTestCase
==========================
= ==========
July is on screen Passed
17 is on screen Passed
2017 is on screen Passed
02:19  PM is on screen Passed
US/Pacific Time zone is on screen Passed
Has correct sunrise time Passed
Has correct sunset time Passed
= ==========


===========================================
Sapphire Regression Test: 2017-07-20 13:51
===========================================


GeneralTest
===========

================================================================================================================================================================================================================================================================================================================================================================================================== ==========

GeneralTest
===========
================================================================================================================================================================================================================================================================================================================================================================================================== ==========
Failed:Button: Channel 1 -> Header , Source Not Routed-[0:P], Fade Time/Rate: 1.0-[1.0:P], Priority: 7-[7:P], Destination Node: 255-[255:P], Destination Node: 255-[1:P], Destination Routed-[1:P], Sequence Number: 0-[15:F], Target Level: 0-[650:F], Source Node: 56-[56:P], Fade Type: Time-[0:P], Priority-5: High Priority Load Control Message-[5:P], Broadcast-[1:P], Start Frame-[1:P], 
 Failed
================================================================================================================================================================================================================================================================================================================================================================================================== ==========


===========================================
Sapphire Regression Test: 2017-07-20 13:59
===========================================


GeneralTest
===========

======== ==========

GeneralTest
===========
======== ==========
Failed:
 Failed
======== ==========


===========================================
Sapphire Regression Test: 2017-07-20 14:09
===========================================


GeneralTest
===========

======== ==========

GeneralTest
===========
======== ==========
Failed:
 Failed
======== ==========


===========================================
Sapphire Regression Test: 2017-07-20 14:14
===========================================


GeneralTest
===========

======== ==========

GeneralTest
===========
======== ==========
Failed:
 Failed
======== ==========
