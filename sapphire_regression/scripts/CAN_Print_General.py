from context import leviton_test
from leviton_test import Kvaser_Functions
from leviton_test.parser.parser_class import Parser
from leviton_test.parser import messages
from leviton_test.parser import linked_messages
import time
from operator import itemgetter



def main():
    ch1 = Kvaser_Functions.set_up_channel(0)
    node_frame_seq = {} #dictionary to store the lists of values needed to keep track of linked frame sources
    #node_frame_seq = [] #list for stroing source address, sequence#, msg type
    for msg in Kvaser_Functions.messages(ch1):
        header = Kvaser_Functions.bits_from_header(list(msg))
        message = Kvaser_Functions.bits_from_msg(list(msg))
        if len(message) >= 1:
            header_message_contents = messages.header_factory(header)
            if header_message_contents.header_start_linked == 1 and not header_message_contents.header_sequence == 0 and not header_message_contents.header_priority == 3: #start frame of a linked sequence
                    message_contents = messages.message_factory(message)
                    #message_contents = sort_message_contents(message_contents)
                    # caches the starting frame information for susequent frames to reference, also caches the last used protocol feature as a reference for linked feature logic
                    node_frame_seq[str(header_message_contents.header_source_address)] = [int(header_message_contents.header_source_address),int(header_message_contents.header_sequence), int(Kvaser_Functions.bits_from_msg(msg)[1], base=2)]
                    print('==============Start Frame===============', '\n', header_message_contents, '\n', message_contents,'\n')
                    last_feature = str(message_contents).rsplit(',', 1)[-1]
                    #print(last_feature)
            if header_message_contents.header_priority == 3: # a linked frame always has this priority
                if header_message_contents.header_sequence == 0: #last frame in a sequence will have this
                    linked_message_type = node_frame_seq[str(header_message_contents.header_source_address)][2] #grabs the message type out of a list in the cahe based on source address
                    linked_message = linked_messages.linked_message_factory(message, linked_message_type, last_feature)
                    linked_message_output = 'Linked Frame From {}'.format(header_message_contents.header_source_address) + "--> " + str(linked_message)
                    print('============Last Frame===============', '\n', header_message_contents, '\n',linked_message_output, '\n')
                    #linked_message = sort_message_contents(linked_message)
                    last_feature = str(linked_message).rsplit(',', 1)[-1]  # grabs the last feature in the previous frame to pass to the linked factory for conditional logic based on feature order
                    del node_frame_seq[str(header_message_contents.header_source_address)] # deletes the cahed entry for a given source ID when the last frame is done
                else:
                    linked_message_type = node_frame_seq[str(header_message_contents.header_source_address)][2]
                    linked_message = linked_messages.linked_message_factory(message, linked_message_type, last_feature)
                    linked_message_output = 'Linked Frame From {}'.format(header_message_contents.header_source_address) + "--" + str(linked_message)
                    print('============Linked Frame===============', '\n', header_message_contents, '\n',linked_message_output, '\n')
                    #linked_message = sort_message_contents(linked_message)
                    last_feature = str(linked_message).rsplit(',', 1)[-1] # grabs the last feature in the previous frame to pass to the linked factory for conditional logic based on feature order
                    #print(last_feature)
                    #print(node_frame_seq)
            elif header_message_contents.header_start_linked == 1 and header_message_contents.header_sequence == 0 and not header_message_contents.header_priority == 3:
                message_contents = messages.message_factory(message)
                #message_contents = sort_message_contents(message_contents)
                print('=====================================', '\n', header_message_contents, '\n', message_contents,'\n')
        #print(list(msg)) #prints in CANking format
        #print(message)

'''
def sort_message_contents(message_contents):
    message_contents = str(message_contents)
    split_contents = message_contents.split(",")
    split_contents_stripped = []
    for string in split_contents:
        split_contents_stripped.append(string.strip())
    strings_to_sort = []
    for string in split_contents_stripped:
        if "Level" in string[:5] or "Offset Channel Number" in string or "No Feature" in string or "Target Level" in string[0:12] or "Absolute Channel Number" in string:
            strings_to_sort.append(string)
            split_contents_stripped.remove(string)
    if strings_to_sort:
        for string in strings_to_sort:
            if "Offset Channel Number" in string or "Absolute Channel Number" in string:
                split_contents_stripped.append(string)
                strings_to_sort.remove(string)
                break
    if strings_to_sort:
        for string in strings_to_sort:
            if "Level" in string and "Target Level" not in string:
                split_contents_stripped.append(string)
                strings_to_sort.remove(string)
                break
    if strings_to_sort:
        for string in strings_to_sort:
            if "Target Level" in string:
                split_contents_stripped.append(string)
                strings_to_sort.remove(string)
                break
    output_string = ""
    i = 0
    for string in split_contents_stripped:
        if not i == len(split_contents_stripped) - 1:
            output_string = output_string + string + ", "
        else:
            output_string = output_string + string
        i = i + 1
    return output_string
'''



'''
def main():
    ch1 = Kvaser_Functions.set_up_channel(0)
    for msg in Kvaser_Functions.messages(ch1):
        message = Kvaser_Functions.bits_from_msg(list(msg))
        header = Kvaser_Functions.bits_from_header(list(msg))
        #print(list(msg)) #prints in CANking format
        #print(message)
        if len(message) > 1:
            message_type = messages.message_factory(message)
            header_type = messages.header_factory(header)
            print('+++++++++++++', '\n',header_type, '\n', message_type)
'''

'''
def main():
    start = time.time()
    ch1 = Kvaser_Functions.set_up_channel(0)
    parser = Parser()

    while True:
        messages = []
        for hdr, msg in Kvaser_Functions.messages(ch1, with_header=True):
            messages.append((hdr, msg))
            elapsed = time.time() - start
            if elapsed > 1.5:
                break

        parser.store_bulk_messages(messages)
        for frame in parser.frames:
            print(frame)
'''

if __name__ == '__main__':
    main()
