import time as timer
import argparse
import sqlite3
import textwrap

from itertools import cycle, groupby
from collections import OrderedDict
from canlib import canlib
from context import leviton_test
from leviton_test.commands import vector_to_commands_dictionary, button_list
from leviton_test.canparser import (
    str_from_msg,
    set_up_channel,
    tear_down_channel,
    tail_kvaser_channel,
    broadcast_msg,
    command_type,
    channel_number,
    target_level,
)
from leviton_test.view import View



def main():
    print('Fade Command Interface')
    relative_abs_flag = -1
    fade_rate_time_flag = -1
    fade_rate = -1
    start_time_bits = 0
    increment_values = 0
    channel = -1
    level = -1
    while not int(relative_abs_flag) in range(0,2):
        try:
            relative_abs_flag = int(input('Enter 1 for Absolute Fade, 0 for Relative Fade:   '))
        except ValueError:
            print("Please enter a valid number")

    while not int(fade_rate_time_flag) in range(0,2):
        try:
            fade_rate_time_flag = int(input('Enter 1 for Fade Rate type, 0 for Fade Time type:   '))
        except ValueError:
            print("Please enter a valid number")
    flag_bits = int(str(relative_abs_flag) + str(fade_rate_time_flag), 2)

    '''BMS can change this, GMX assumes it is 7'''
    priority = 7

    '''Kind of a hack, I want the user to enter fade times, but have to assume a start time of 0
    and an increment of .1 otherwise the bit parsing gets complicated and most users only care
    about the time in seconds froma 0 start point anyway. Deconstructing a LumaCAN message in bits
    from a given fade time is much more complicated that constructing a fade time from bits
    '''
    while not int(fade_rate) in range(0,25):
        try:
            fade_rate = int(input('Enter fade rate in seconds (25sec max):   '))
        except ValueError:
            print("Please enter a valid number")
    if fade_rate <=25:
        start_time_bits = int('0000',2)
        increment_values = .1
        fade_rate_bits = int(fade_rate *10)

    while not int(channel) in range(1,255):
        try:
            channel = int(input("Enter the channel number (1-255):   "))
        except ValueError:
            print("Please enter a valid number")
    channel_bits = (channel + 127)

    while not int(level) in range(0,101):
        try:
            level = int(input("Enter the Target Level (0-100%):   "))
        except ValueError:
            print("Please enter a valid number")
    level_bits = int(255 * (level / 100))

    command = [2, 45, flag_bits, 112, fade_rate_bits, channel_bits, level_bits]
    header = None
    #command = [2, 45, 2, 112, 10, 128, 255]
    #header = 201295967
    send_input_device_message(command, header)


def send_input_device_message(command, header):
    channel = set_up_channel(0)
    print(command)
    broadcast_msg(channel, command, header)
    flush_channel(channel, 1)
    tear_down_channel(channel)

def flush_channel(channel, seconds):
    for (_, _, _, _, _) in tail_kvaser_channel(channel, timeout=seconds):
        pass

if __name__ == '__main__':
    main()
