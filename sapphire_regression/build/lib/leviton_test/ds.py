# ds.py
"""This module defines an interface for the DS for turning turning relays on and off."""

import subprocess
from leviton_test.config import Configuration
from leviton_test.greenmax import binary_state


config = Configuration()


def ds_toggle_relay(num):
    """Toggles relay number :arg:`num` using the exe or py file found at DS_EXE_PATH"""
    try:
        if '.py' in config.GM_DS_EXE_PATH:
            cmdline_args = ['python',  config.GM_DS_EXE_PATH, str(num)]
        else:
            cmdline_args = [config.GM_DS_EXE_PATH, str(num)]

        subprocess.run(cmdline_args, timeout=config.SAPPHIRE_WAIT_TIME)
    except subprocess.TimeoutExpired:
        pass


def ds_all_relays_off():
    state = binary_state()
    # assumes relays 1-18
    currently_on = [i for i in range(1,17) if state['channel {}'.format(i)] == 1]
    for relay in currently_on:
        ds_toggle_relay(relay)

if __name__ == '__main__':
    print('before', binary_state())
    ds_all_relays_off()
    print('after', binary_state())
