# view.py
"""View operations for controlling an Android device.

This module defines a View class that is used to control an Android device.  The
View class is used whenever you want to control an Android device, such as pressing
a button, swiping across the screen, or checking if the current screen contains
a particular UI element.

Examples:

    >>> view = View()
    >>> view.press('A1') # presses button A1 on screen
    >>> print(view) # prints all UI elements contained in the view
    >>> view.swipe(100, 150, 200, 300) # swipes from coordinates x,y-coordinates (100, 150) to (200, 300)
    >>> view.back() # Uses Android's `back` functionality to return to the previous screen

The View class also contains Sapphire-specific functions.

For example:

    >>> view = View()
    >>> view.scroll_menu_left() # scroll Sapphire's tab menu to the left
    >>> view.scroll_menu_right() # scroll Sapphire's tab menu to the right


"""
import subprocess
import time
import os
import xml.etree.ElementTree as etree
import logging
import pickle
import re
from datetime import datetime, timedelta
from itertools import chain
from numpy import polyfit, poly1d, RankWarning
from leviton_test.adb import ADB
from leviton_test.utils import center, coordinates_from_bounds_string


class View(ADB):
    """Represents an Android view and methods of using that view to control the device.

    This class is used to control an Android device.

    Attributes:
        buttons: list of Button
            A list containing all the buttons on the current screen
        name: str
            The name of the current view, defaults to the empty string
        sliders: list of Slider
            All the sliders on the current view
        text_elements: list of str
            Contains all the text elements on the current screen
        edit_texts: list of EditText
            Contains a list of all `android.widget.EditText` elements
        settings_button: Button
            Contains the settings button if there is one, else None
    """

    menu_y_coordinate = 35

    def __init__(self, name='', delay=0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        time.sleep(delay)
        self.wakeup()
        self.name = name
        before = time.time()
        self.tree = super().get_parsed_tree()
        self.sliders = self.get_sliders(self.tree)

        self.text_elements = self.get_text_elements()
        self.edit_texts = self.get_edit_texts()
        self.buttons = self.get_buttons() + self.edit_texts # TODO: Refactor view functionality to not require self.edit_texts
        try:
            self.settings_button = [Button.from_xml_elem(elem, handler=super(View, self).command) for elem in self.nodes if Button.xml_is_button(elem) and Button.xml_is_settings(elem)][0]
        except IndexError:
            self.settings_button = None

    """@property
    def tree(self):
        if hasattr(self, '_tree'):
            return self._tree
        else:
            self._tree = super().get_parsed_tree()
            return self._tree
    """
    @property
    def nodes(self):
        if hasattr(self, '_nodes'):
            return self._nodes
        else:
            self._nodes = self.tree.findall('.//node[@text]')
            return self._nodes

    def get_text_elements(self):
        return [elem.attrib['text'] for elem in self.nodes]

    def get_edit_texts(self):
        return [EditText.from_xml_elem(elem, handler=super(View, self).command) for elem in self.nodes if EditText.xml_is_edit_text(elem)]

    def get_buttons(self):
        return [
            Button.from_xml_elem(elem, handler=super(View, self).command)
            for elem in self.nodes
            if Button.xml_is_button(elem) and not Button.xml_is_settings(elem)
        ]

    def press(self, button, seconds=0, button_position=None, refresh=True):
        """Presses the button on the view with text matching :arg:`button`."""
        if type(button) is Button:
            button.press()
        elif button.lower() == 'settings':
            if not self.settings_button:
                raise RuntimeError("No settings button no screen")
            self.settings_button.press(seconds=seconds)
        else:
            matching = [x for x in self.buttons if x.name == button]

            if len(matching) > 1:
                button = None
                if button_position:
                    sorted_left_to_right = sorted(matching, key=lambda btn: btn.x)
                    if button_position == 'left':
                        button = sorted_left_to_right[0]
                    elif button_position == 'right':
                        button = sorted_left_to_right[-1]
                    else:
                        raise RuntimeError('Invalid button_position {}'.format(button_position))
                    button.press(seconds=seconds)
                else:
                    raise RuntimeError('Multiple buttons {} found, please use a button_position like `left` or `right`'.format(button))
            elif len(matching) == 1:
                button = matching[0]
                button.press(seconds=seconds)
                logging.info('Pressed {}'.format(button))
            else:
                msg = "Button {} not found on screen.".format(button)
                msg += "  A previously pressed button may not have been enabled and so didn't"
                msg += " change the screen."
                logging.error(msg)
                raise RuntimeError(msg)
        if refresh:
            self.refresh()

    def refresh(self, delay=1):
        new = self.__class__()
        self.name = new.name
        self.sliders = new.sliders
        self.text_elements = new.text_elements
        self.edit_texts = new.edit_texts
        self.buttons = new.buttons
        self.settings_button = new.settings_button

    def slide(self, name, level):
        """Slides slider 'name' to the specified level."""
        slider = self.get_slider_with_name(name)
        return slider.slide(level=level)

    def get_slider_with_name(self, name):
        matching_list = list(filter(lambda x: x.name == name, self.sliders))
        assert len(matching_list) < 2, 'Multiple sliders {} found'.format(name)
        assert matching_list, 'No slider {} found'.format(name)
        slider = matching_list[0]
        return slider

    def press_settings(self, refresh=True):
        try:
            self.settings_button.press(2)
            logging.info("Pressed settings")
        except AttributeError:
            logging.error("Settings button not found on screen")
            raise RuntimeError("Settings button not found on screen")
        if refresh:
            self.refresh()

    def keyevent(self, key):
        code = {
            'tab': 61,
            'enter': 66,
            'del': 67,
        }.get(str(key).lower(), key)

        if key == code:
            super().command(['adb', 'shell', 'input', 'text', str(code)])
        else:
            super().command(['adb', 'shell', 'input', 'keyevent', str(code)])

    def scroll_menu_left(self):
        source_x, destination_x = 720, 30
        self.swipe(source_x=source_x, source_y=self.menu_y_coordinate, destination_x=destination_x, destination_y=self.menu_y_coordinate)
        self.refresh()

    def scroll_menu_right(self):
        source_x, destination_x= 30, 720
        self.swipe(source_x=source_x, source_y=self.menu_y_coordinate, destination_x=destination_x, destination_y=self.menu_y_coordinate)
        self.refresh()

    def swipe(self, source_x, source_y, destination_x, destination_y, refresh=True):
        cmd = 'adb shell input swipe {source_x} {source_y} {destination_x} {destination_y}'
        formatted_cmd = cmd.format(
            source_x=source_x,
            source_y=source_y,
            destination_x=destination_x,
            destination_y=destination_y,
        )
        super().command(formatted_cmd.split())
        if refresh:
            self.refresh()

    def has_texts(self, strings):
        for s in strings:
            if not self.has_text(s):
                return False
        return True


    def has_text(self, string):
        for elem in self.text_elements:
            if string in elem:
                return True
        return False

    def type(self, text, field):
        try:
            node = self.edit_texts[field]
            node.press()
            number_of_characters = len(node.name)
            for i in range(number_of_characters):
                self.keyevent('del')
            node.type(text)
        except IndexError:
            raise IndexError("There is no field {} in this view".format(field))

    def back(self, refresh=True):
        cmd = 'adb shell input keyevent 4'
        super().command(cmd.split())
        if refresh:
            self.refresh()

    def get_sliders(self, tree):
        """Return all sliders from the current View.

        Sliders need a special method because a complete slider object also requires information from its parent
        in the View hierarchy.
        """
        sliders = list(filter(lambda x: x.attrib['class'] == 'android.widget.SeekBar', tree.findall('.//node')))
        result_list = []
        for parent in tree.iter():
            for child in parent:
                for s in sliders:
                    if s.attrib == child.attrib:
                        result_list.append(Slider.from_xml_elem(s, parent, handler=super().command))
        return result_list

    def get_times(self):
        """Returns a list of all text that matches the time format `H:M` for hours and minutes"""
        result = []
        for elem in [x for x in self.text_elements if ':' in x]:
            try:
                matches = [re.search('\d+:\d+', s) for s in elem.split() if re.search('\d+:\d+', s)]
                for m in matches:
                    t = datetime.strptime(m.group(0), '%H:%M')
                    result.append(t)
            except ValueError:
                pass
        return result

    def has_time(self, time, deviation=None):
        if type(time) is str:
            time = datetime.strptime(time, '%H:%M')
        deviation = timedelta(hours=deviation) if deviation else timedelta(hours=0)
        for cmp in self.get_times():
            hour = time.hour if time.hour <= 12 else time.hour - 12
            time = datetime(year=cmp.year, month=cmp.month, day=cmp.day, hour=hour, minute=time.minute)
            if cmp - deviation < time < cmp + deviation:
                return True

        return False

    def __str__(self):
        s = ''
        if self.buttons:
            s += '\nButtons:\n========\n' + '\n'.join(str(b) for b in self.buttons) + '\n'
        if self.sliders:
            s += '\nSliders:\n=========\n' + '\n'.join(str(slider) for slider in self.sliders) + '\n'
        if self.edit_texts:
            s += '\nEditTexts:\n=========\n' + '\n'.join(str(idx) + ': ' + str(edittext) for idx, edittext in enumerate(self.edit_texts)) + '\n'
        if self.text_elements:
            s += '\nAll Text Elements:\n===================\n' + '\n'.join(str(elem) for elem in self.text_elements if elem) + '\n'
        return s


    def press_xy(self, x, y, refresh=True):
        """Taps on the x, y coordinates of the connected Android device."""
        cmd = 'adb shell input tap {x} {y}'.format(x=x, y=y)
        super().command(cmd.split())
        if refresh:
            self.refresh()

    def __eq__(self, other):
        return set(self.buttons) == set(other.buttons) and set(self.sliders) == set(other.sliders)


class Pressable(object):
    """Defines a mixin for all pressable objects.

    Provides a press() method, to press on the screen at (x, y) coorddinates
    self.x and self.y for some number of seconds.
    """


    def press(self, seconds=0):
        virtual_screensize = 2047 # x and y
        real_size_x = 800
        real_size_y = 480
        adjusted_x = int(self.x * (virtual_screensize/real_size_x))
        adjusted_y = int(self.y *(virtual_screensize/real_size_y))
        before_sleep = [
            'sendevent /dev/input/event1 1 330 1',
            'sendevent /dev/input/event1 3 0 {}'.format(adjusted_x),
            'sendevent /dev/input/event1 3 1 {}'.format(adjusted_y),
            'sendevent /dev/input/event1 0 0 0',
            ]

        after_sleep = [
            'sendevent /dev/input/event1 1 330 0',
            'sendevent /dev/input/event1 0 0 0',
        ]

        for c in before_sleep:
            new_command = 'adb shell ' + c
            self.handler(new_command.split())

        time.sleep(seconds)

        for c in after_sleep:
            new_command = 'adb shell ' + c
            self.handler(new_command.split())


class Button(Pressable):
    """Defines a Button that can be clicked on an Android screen."""

    def __init__(self, name, x, y, enabled=True, handler=subprocess.run):
        self.name = name
        self.x = x
        self.y = y
        self.enabled = enabled
        self.handler = handler

    @classmethod
    def from_xml_elem(cls, elem, handler=subprocess.run):
        coords = center(*coordinates_from_bounds_string(elem.attrib['bounds']))
        name = elem.attrib['text'] if elem.attrib['content-desc'] != 'More options' else 'options'
        enabled = True if elem.attrib['enabled'] == 'true' else False
        return cls(name.strip(), coords.x, coords.y, enabled=enabled, handler=handler)

    @staticmethod
    def xml_is_button(elem):
        classes = {'android.widget.Button', 'android.widget.CheckedTextView', 'android.widget.TextView'}
        return (elem.attrib['class'] in classes or elem.attrib['content-desc'] == 'More options')

    @staticmethod
    def xml_is_settings(elem):
        text = elem.attrib['text']
        pattern = r'\w+ \d+, \d+, \d+:\d+:\d+ (AM|PM)'
        return re.search(pattern, text)


    def is_settings(self):
        """Returns True if self is a settings button, otherwise False.

        Since the settings button is the only one on-screen that contains 'AM' or 'PM',
        that is used as the true-false criteria.
        """
        return 'AM' in self.name or 'PM' in self.name

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return 'Button({}, {}, {})'.format(self.name, self.x, self.y)

    def __eq__(self, cmp):
        return self.x == cmp.x and self.y == cmp.y and self.name == cmp.name

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(str(self.x) + str(self.y))


class Slider(object):
    """Represents a UI slider element of class `android.widget.SeekBar` """

    def __init__(self, top, bottom, mid, name='', current_value=0, handler=subprocess.run):
        self.name = name
        self.top = top + 14 # the sliders are visually off by 14 on the sapphire
        self.bottom = bottom - 14 # the sliders are visually off by 14 on the sapphire
        self.mid = mid
        self.current_value = current_value
        self.handler = handler

    @classmethod
    def from_xml_elem(cls, elem, parent, handler=subprocess.run):
        upper_left, lower_right = coordinates_from_bounds_string(elem.attrib['bounds'])
        width = lower_right[0] - upper_left[0]
        mid = upper_left[0] + int(width/2)
        top = upper_left[1]
        bottom = lower_right[1]
        #parent = elem.xpath('..')[0]
        children = parent.findall('*')
        name = children[0].attrib['text']
        value_text = children[1].attrib['text']
        if value_text:
            current_value = int(children[1].attrib['text'])
        else:
            current_value = 0
        return cls(top=top, bottom=bottom, mid=mid, name=name, current_value=current_value, handler=handler)


    def slide(self, level):
        """Slides slider to level."""
        cmd = 'adb shell input swipe {src_x} {src_y} {dst_x} {dst_y}'
        if level >= 100:
            formatted = cmd.format(src_x=self.mid, src_y=self.bottom, dst_x=self.mid, dst_y=self.top - 40)
            self.handler(formatted.split())
        elif level <= 0:
            formatted = cmd.format(src_x=self.mid, src_y=self.top, dst_x=self.mid, dst_y=self.bottom + 60)
            self.handler(formatted.split())
        else:
            self.precise_slide(level)


    def precise_slide(self, level):
        bottom_y = self.get_adjusted_y(self.bottom)
        level_y = self.get_adjusted_y(self.get_slide_level_y(level))
        x = self.get_adjusted_x(self.mid)
        self.__press_down()
        self.__move_coordinates(x, bottom_y)
        halfway = int((bottom_y - level_y)/2)
        self.__move_coordinates(x, bottom_y - halfway)
        self.__move_coordinates(x, level_y)
        time.sleep(1)
        self.__press_up()


    def __move_coordinates(self, x, y):
        movement_commands = [
            'sendevent /dev/input/event1 3 0 {}'.format(x),
            'sendevent /dev/input/event1 3 1 {}'.format(y),
            'sendevent /dev/input/event1 0 0 0',
        ]
        for cmd in movement_commands:
            new_command = 'adb shell ' + cmd
            self.handler(new_command.split())

    def __press_down(self):
        cmd = 'adb shell sendevent /dev/input/event1 1 330 1'
        self.handler(cmd.split())

    def __press_up(self):
        unpress = [
            'sendevent /dev/input/event1 1 330 0',
            'sendevent /dev/input/event1 0 0 0',
        ]

        for c in unpress:
            new_command = 'adb shell ' + c
            self.handler(new_command.split())

    def get_adjusted_x(self, x):
        virtual_screensize = 2047 # x and y
        real_size_x = 800
        return int(x * (virtual_screensize/real_size_x))

    def get_adjusted_y(self, y):
        virtual_screensize = 2047 # x and y
        real_size_y = 480
        return int(y * (virtual_screensize/real_size_y))

    def get_slide_level_y(self, level):
        """ Returns a target y coordinate for a given slider level.  Uses numpy's curve fitting.

        The levels and coords are hardcoded from manual testing with Sapphire.
        When moving a slider to level 0, you had to move to pixel y-axis 415,
        for 15, pixel y-axis 374, etc...

        polyfit and poly1d return a function that fits this curve.

        In case of device or UI configuration changes, you can modify these to represent more accurate
        values.  Probably 3 or 4 sample points will be fine.
        """

        coords = [self.bottom, 377, 336, 234, 199, 171, self.top]
        levels = [          0,  13,  28,  65,  78,  88,      100]

        try:
            function = poly1d(polyfit(x=levels, y=coords, deg=4))
        except RankWarning:
            pass
        return int(function(level))

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return 'Slider({}, top={}, bottom={}, mid={}, current_value={})'.format(self.name, self.top, self.bottom, self.mid, self.current_value)

    def __eq__(self, other):
        return (self.name == other.name and self.top == other.top and
                self.bottom == other.bottom and self.mid == other.mid)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        string = '{}{}{}'.format(self.top, self.bottom, self.mid)
        return hash(string)


class EditText(Pressable):

    def __init__(self, x, y, index, name='', handler=subprocess.run):
        self.x = x
        self.y = y
        self.index = index
        self.name = name
        self.handler = handler

    @classmethod
    def from_xml_elem(cls, elem, handler=subprocess.run):
        coords = center(*coordinates_from_bounds_string(elem.attrib['bounds']))
        index = int(elem.attrib['index'])
        name = elem.attrib['text']
        return cls(x=coords.x, y=coords.y, index=index, name=name, handler=handler)

    def type(self, string):
        string  = str(string)
        if len(string.split()) > 1:
            raise RuntimeError('Cannot type a string with spaces')
        cmd = ['adb', 'shell', 'input', 'text', '{string}'.format(string=string)]
        self.handler(cmd)

    @staticmethod
    def xml_is_edit_text(elem):
        return elem.attrib['class'] == 'android.widget.EditText'

    def __str__(self):
        return 'EditText(name={}, x={}, y={}, index={})'.format(self.name, self.x, self.y, self.index)

    def __hash__(self):
        return hash(str(self.x) + str(self.y))
