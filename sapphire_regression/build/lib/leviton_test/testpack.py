import os
from zipfile import ZipFile
import xml.etree.ElementTree as ET

def main():
    sapphire, index = get_files()
    print(sapphire, index)
    print(get_files_parsed())


def get_files_parsed():
    sapphire, index = get_files()
    return ET.parse(sapphire), ET.parse(index)

def get_files():
    zips = [filename for filename in os.listdir() if filename.endswith('.zip')]
    for filename in zips:
        with ZipFile(filename) as myzip:
            names = [name for name in myzip.namelist() if name.endswith('.xml')]
            names = [name for name in names if 'Sapphire.xml' in name or 'index.xml' in name]


            if names:
                assert len(names) == 2
                for name in names:
                    if 'Sapphire.xml' in name:
                        sapphire = myzip.open(name)
                    if 'index.xml' in name:
                        index = myzip.open(name)
            else:
                break
    return sapphire, index


if __name__ == '__main__':
    main()
