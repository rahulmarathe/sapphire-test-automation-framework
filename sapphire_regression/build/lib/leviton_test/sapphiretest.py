# regression.py
"""This module defines the classes used in Sapphire regression testing"""
import os
import subprocess
import unittest
import gc
import time
import pickle
from functools import partial
from itertools import chain
from collections import namedtuple, OrderedDict
from datetime import datetime
from leviton_test.greenmax import binary_state, analog_state, voltage_from_percentage
from leviton_test.config import Configuration
from leviton_test.adb import ADB
from leviton_test.view import View
from leviton_test.ds import ds_toggle_relay
from leviton_test.utils import DelayedKeyboardInterrupt, Timer
from leviton_test import canparser


config = Configuration()
REPORT_FILENAME = os.path.join(os.getcwd(), 'report.rst')


def main(only=None):
    """Runs all test subclasses of SapphireTestCase."""
    with Timer() as timer:
        testclasses = SapphireTestCase.__subclasses__()
        if type(only) is list:
            testclasses = [cls for cls in testclasses if cls in only]
        elif only:
            testclasses = [cls for cls in SapphireTestCase.__subclasses__() if cls == only]

        for TestClass in testclasses:
            test = TestClass()
            test.run()
            test.report()
    print('Tests completed in {} minutes'.format(timer.elapsed_minutes))


class SapphireTestCase(object):
    """Defines a class for implementing Sapphire test cases.

    SapphireTestCase provides automatic reporting of pass-fail assertions,
    Sapphire/GreenMAX behavior and state abstractions, and methods for testing
    that the Sapphire behavior is reflected accurately in GreenMAX state.


    Methods
    -------

    test_relay_result:
        Test that a Sapphire action results in a particular GreenMAX state. This
        is the primary function for testing Sapphire/GreenMAX behavior.

    recent_message_batch_has:
        Tests that the previous test also resulted in particular CAN messages.

    test_relay_increment:
        Tests that a button causes a particular relay to increment by a certain
        amount.

    .. note::

        Mocking
        -------

        The two arguments in SapphireTestCases's constructor (ViewClass and path_to_xml),
        are exclusively used when mocking out a device.  They allow youto use a static xml file
        or a mock view object (that contains a fake xml and fake behavior) when developing tests.
        This is useful when developing on the regression leviton_test itself, because you can add features
        and create views to explicitly test the new feature, as opposed to depending on a device and its
        current screen state.

    Arguments
    ---------

    :arg:`path_to_xml` - The path to a fake or static xml view hierarchy to be used instead of pulling one from adb.
    :arg:`ViewClass` - the view class to be used in this test case, (useful for using mock views).

    """

    feedback_time = 5

    def __init__(self, ViewClass=View, path_to_xml=None):
        self.print_intro()
        self.path_to_xml = path_to_xml
        self.ViewClass = ViewClass
        self.all_passed = True
        self.view = ViewClass(path_to_xml=self.path_to_xml)
        self.can_channel = None
        can_attempts = 0
        while can_attempts < 3:
            try:
                self.can_channel = canparser.set_up_channel()
                break
            except Exception as err:
                print('Error in Kvaser CANLib connection, retrying...')
            can_attempts += 1

        if not self.can_channel:
            raise Exception('Unable to connect to Kvaser via CANlib')

        # TODO: retry menu swipe if button not found
        tab_position = getattr(self, 'tab_position', None)
        if tab_position:
            self.view.wakeup()

            if tab_position == 'left':
                self.view.scroll_menu_right()
            elif tab_position == 'right':
                self.view.scroll_menu_left()
            elif not tab_position:
                pass
            else:
                raise RuntimeError('Invalid tab position: {}.  Expected "left" or "right".')

        # Move to the tab before capturing the next View
        tab = getattr(self, 'tab', None)
        if tab:
            self.view.press(tab)
            # Set all channels to zero if testing a tab
            ds_toggle_relay(8)
        self.passed = []
        self.failed = []

    def get_view(self):
        return self.view

    def test_relay_result(self, relays, levels, slider=None, slider_level=None, button=None, button_position=None, wait_time=0, binary=False):
        """Asserts that a button or slider action results in specific relay states.


        When `button` is pressed, the list of `relays` turns to levels in `levels` after `wait_time` seconds.


        Testing Buttons
        ---------------

        Examples:

            To assert that button `Shock A` turns relay 5 to 20% and relay 6 to
            80% after a 5 seconds fade time:

            ::

                test_relay_result(button='Shock A', relays=[5, 6], levels=[20, 80], wait_time=5)


            To assert that button 'Foo' turns relays 1, 2, and 3 to 0, using the binary reader instead
            of the analog reader:

                test_relay_result(button='Foo', relays=[1, 2, 3], levels=[0, 0, 0], binary=True)

            The binary reader tests levels 0 or 1, NOT 100 (as is the case for the analog reader)

            Example:

            ::

                test_relay_result(button='Foo', relays=[1, 2, 3], levels=[1, 1, 1], binary=True)


        Testing Sliders
        ----------------

        Examples:

            To test that sliding slider `Babb 5-8` sliding to level 100 turns relays 5, 6, 7, and 8
            all to 100 (analog) after a 5 second wait:

            ::

                test_relay_result(slider='Babb 5-8', slider_level=100, relays=[5, 6, 7, 8], levels=[100, 100, 100, 100], wait_time=5)

            The relays, levels, and wait_time arguments are the same as when testing button behavior.

        """
        if len(relays) != len(levels):
            raise RuntimeError('Each relay must have a level, len(relays) == {} and len(levels) == {}'.format(len(relays, len(levels))))

        self.flush_can_bus()
        if button:
            self.view.press(button, button_position=button_position)
        else:
            self.view.slide(slider, slider_level)
        self.save_can_messages()
        self.flush_can_bus()
        if wait_time > 0:
            print('Waiting for {} seconds'.format(wait_time))
            time.sleep(wait_time)
        if binary:
            assert not [x for x in levels if x != 1 and x != 0], 'binary level can only be a list of 0s or 1s, not {}'.format(levels)
            state = binary_state()
            # function has the same parameters as self.relay_range_assertion_statement so that they can be used interchangably
            if button:
                formatf = 'Button {} turns relay {} to state {}'.format
                function = lambda expected_level, relay_number, actual_state, button: (expected_level == actual_state, formatf(button, relay_number, expected_level))
            else:
                formatf = 'Slider {} to level {} turns relay {} to state {}'.format
                function = lambda expected_level, relay_number, actual_state, slider, slider_level: (expected_level == actual_state, formatf(slider, slider_level, relay_number, expected_level))
        else:
            state = analog_state()
            function = self.relay_range_assertion_statement

        states = [state[x] for x in relays]
        for relay, level, state in zip(relays, levels, states):
            if button:
                assertion, msg = function(
                    expected_level=level,
                    relay_number=relay,
                    actual_state=state,
                    button=button,
                )
            else:
                assertion, msg = function(
                    expected_level=level,
                    relay_number=relay,
                    actual_state=state,
                    slider=slider,
                    slider_level=slider_level,
                )
            self.assertTrue(assertion, msg)

    def flush_can_bus(self):
        for _ in canparser.messages(self.can_channel, timeout=1):
            pass

    def save_can_messages(self):
        """Saves CAN messages from Kvaser to self.saved_can_messages"""
        self.saved_can_messages = []
        for msg in canparser.messages(self.can_channel, timeout=2):
            self.saved_can_messages.append(msg)

    def recent_message_batch_has(self, msg_type, group_number=None, channel=None, target_level=None, description=None):
        """Tests if the previous test resulted in messages matching this method's arguments.

        If an argument is not passed in, or is None, then that condition is not tested.

        For example, if :arg:`msg_type` is 'Group Channel Fade', this test will pass
        only if there was a 'Group Channel Fade' message in the group of messages
        triggered by the last test.
        """

        truth = False
        messages = [msg for msg in self.saved_can_messages if canparser.command_type(msg) == msg_type]

        get_target_level = {
            'Channel Fade': canparser.target_level,
            'Channel Status Publish': canparser.target_level,
            'Group Fade': canparser.group_target_level,
        }.get(msg_type, None)

        if not get_target_level:
            raise Exception('No target_level function implemented for message type {}'.format(msg_type))

        truth_functions = [
            lambda m: get_target_level(m) == target_level / 100 if target_level else True,
            lambda m: canparser.channel_number(m) == channel if channel else True,
            lambda m: canparser.group_number(m) == group_number if group_number else True,
        ]

        # False if no messages sent or no message returns True for every
        # function in truth_functions
        passed = False
        for msg in messages:
            passed = all(f(msg) for f in truth_functions)
            if passed:
                break

        self.assertTrue(truth, description)

    def print_intro(self):
        message = 'Starting test {}'.format(self.__class__.__name__)
        print(message)

    def run(self):
        raise Exception("run() must be implemented")

    def report(self):
        """Displays results of tests and outputs a .rst summary."""
        with open(REPORT_FILENAME, 'a') as f:
            now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M')
            intro = 'Sapphire Regression Test: {}\n'.format(now)
            border = '=' * len(intro) + '\n'
            f.write('\n\n' + border + intro + border)
            title = '\n{}\n{}'.format(self.__class__.__name__, '=' * len(self.__class__.__name__))
            print(title)
            f.write('\n' + title)
            failed_string = "Failed Tests ({}):".format(len(self.failed))
            print(failed_string)
            for test in self.failed:
                string = '    - ' + test
                print(string)

            passed_string = "Passed Tests ({}):".format(len(self.passed))
            print(passed_string)
            for test in self.passed:
                string = '    + ' + test
                print(string)

            if self.failed:
                max_length = len(max(self.failed, key=lambda x: len(x)))
            else:
                max_length = 1
            row = ('=' * max_length) + ' ' + ('=' * 10)
            f.write('\n\n' + row + '\n' + title + '\n' + row + '\n')
            for test in self.failed:
                filler = ' ' * (max_length - len(test))
                f.write(test + filler + ' Failed' + '\n')
            for test in self.passed:
                filler = ' ' * (max_length - len(test))
                f.write(test + filler + ' Passed' + '\n')
            f.write(row + '\n')

    def relay_range_assertion_statement(self, expected_level, relay_number, actual_state, slider=None, slider_level=None, button=None):
        """Returns a (bool, str) 2-tuple.  For testing the GreenMAX response to a Sapphire button press
        or slider slide.  The str in the returned tuple is a message describing the test result.

        .. note:: This method doesn't actually perform the action, it just evaluates state, and creates
                  an appropriate truth value and message based on that state. It is meant to be used
                  after the slider or button action has already been performed.

        Arguments
        =========
            expected_level : int
                - The state you expect relay :arg:`relay_number` to be.
            relay_number: int
                - The number of the relay whose state you are testing
            actual_state : dict
                - A dictionary containing the current GreenMAX state.  It should be the analog state. [1]
            slider : str
                - The name of the slider under test
            slider_level : int
                - The level :arg:`slider` is slid to
            button : str
                - The name of the button under test

        .. warning:: Only use :arg:`button` or :arg:`slider`, but not both.

        The bool is True if the `slider` turned to `slider_level`, or `button` being pressed, resulted
        in the relay state being reflected in the :arg:`actual_state` dictionary.

        Notes
        =====
            1. The analog state comes from :func:`greenmax.analog_state`.
        """
        # Uncomment the following code when you can test that it works
        #assert not (slider and button), "Do not call with args slider AND button, use only one at a time"
        if slider:
            msg = 'Sliding {} to level {} turns relay {} '.format(slider, slider_level, relay_number)
        else:
            msg = '{} turns relay {} '.format(button, relay_number)
        deviation = .5
        if expected_level == 0:
            expected_state1 = voltage_from_percentage((expected_level/100))
            lower1 = expected_state1 - deviation
            upper1 = expected_state1 + deviation
            expected_state2 = 0.72 # Roughly zero volts, not exactly zero due to voltage reader wierdness
            lower2 = expected_state2 - deviation
            upper2 = expected_state2 + deviation
            msg += 'to level between {} and {} or between {} and {}'.format(round(lower1, 2), round(upper1, 2), round(lower2, 2), round(upper2, 2))
            msg += ', Real Voltage: {0:.2f}'.format(actual_state)
            statement = lower1 <= actual_state <= upper1 or lower2 <= actual_state <= upper2
        else:
            expected_state = voltage_from_percentage((expected_level/100))
            lower = expected_state - deviation
            upper = expected_state + deviation
            msg += 'to level between {} and {}'.format(round(lower, 2), round(upper, 2))
            msg += ', Real Voltage: {0:.2f}'.format(actual_state)
            statement = lower <= actual_state <= upper
        return statement, msg

    def test_relay_increment(self, button, relays, increments, reps=2, dev=.2, wait_time=0):
        """Tests that pressing `button` makes `relays` increment by value `increments`.

        For example ::

                test_relay_increment(button='A:Lower', relays=[14], increments=[-.10])

        The above call tests that button `A:Lower` decrements relay 14 by .10.
        """
        for idx, relay in enumerate(relays):
            inc = increments[idx] * 10
            text = 'raises' if inc > 0 else 'lowers'
            for _ in range(reps):
                pre = analog_state()[relay]
                pre = 0 if pre > 13 else pre
                self.view.press(button)
                if wait_time > 0:
                    print("Waiting for {} seconds".format(wait_time))
                    time.sleep(wait_time)
                post = analog_state()[relay]
                post = 0 if post > 13 else post
                difference = post - pre
                cmp = difference > 0 if inc > 0 else difference < 0
                lower = inc - dev
                upper = inc + dev
                self.assertTrue(
                    cmp and lower < difference < upper,
                    '{} {} channel {} between {}v and {}v, real difference: {}v'.format(button, text, relay, lower, upper, round(difference,3))
                )

    def assertTrue(self, assertion, message):
        """Adds the message to the passed list if :arg:`assertion` is true.
        Otherwise, adds :arg:`message` to the failed list.
        """
        if not assertion:
            if config.SAPPHIRE_VERBOSE:
                print('failed: - {}'.format(message))
            self.failed.append(message)
            self.all_passed = False
        else:
            if config.SAPPHIRE_VERBOSE:
                print('passed: + {}'.format(message))
            self.passed.append(message)
