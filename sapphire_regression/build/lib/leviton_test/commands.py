import xml.etree.ElementTree as ET
import os
import re
from itertools import cycle
from collections import namedtuple
from leviton_test import testpack
Command = namedtuple('Command', ['command', 'level', 'channel'])


global DB_PATH


def get_command_vectors(tree):
    root = tree.getroot()
    actions = root.findall('.//Action')
    return set(x.get('command') for x in actions if re.search(r'Vector\..*.Scene\..*', x.get('command') ))


def get_vector_name(command):
    for s in command.split('.'):
        if '-' in s:
            return s

def format_command_attr(attr, level):
    if 'G' in attr:
        return attr
    else:
        if level == '100':
            s = '{}_on'
        elif level == '0':
            s = '{}_off'
        else:
            return 'invalid_level'
        num = attr.strip('C').strip('G')
        return s.format(num)


def in_list_of_commands(command, commands):
    for cmd, vector in commands:
        if cmd == command:
            return True
    return False


def command_dictionary_sorted_keys():
    return sorted(command_dictionary().keys(), key=lambda x: int(x.split('_')[0]))


def command_generator():
    dictionary = command_dictionary()
    for c in cycle(command_dictionary_sorted_keys()):
        yield dictionary[c]


def command_key_generator():
    yield from cycle(command_dictionary_sorted_keys())


def on_command_key_generator():
    yield from cycle(on_command_sorted_keys())


def on_command_sorted_keys():
    for key in command_dictionary_sorted_keys():
        if 'on' in key:
            yield key

def on_command_in_bytes(cmd):
    return on_commands().get(cmd.lower())

def command_in_bytes(cmd):
    return command_dictionary()[cmd.lower()]


def on_commands():
    return {key: val for key, val in command_dictionary().items() if 'on' in key}


def command_dictionary():
    return  { key: val for key, val in get_commands() if key != 'invalid_level' }


def get_commands():
    root, index = testpack.get_files_parsed()
    commands = get_command_vectors(index)
    result = []
    for c in commands:
        vector_name = get_vector_name(c)
        match = ".//vector[@name='{}']".format(vector_name)
        elem = root.findall(match)
        if not elem:
            match = ".//scalar[@name='{}']".format(vector_name)
            elem = root.findall(match)
        if not elem:
            match = ".//scene[@name='{}']".format(vector_name)
            elem = root.findall(match)
        if elem:
            elem = elem[0]
        if 'vector' in repr(elem):
            primary = elem.findall("./scene[@name='primary']/channelSetting")
            if len(primary) == 1:
                if 'primary' in c:
                    primary = primary[0]
                    channel = primary.get('channel')
                    if not 'G' in channel:
                        level = primary.get('level')
                        command = format_command_attr(channel, level)
                        result.append((command, c))
                        if not in_list_of_commands(command, result):
                            result.append((command, c))
            secondary = elem.findall("./scene[@name='secondary']/channelSetting")
            if len(secondary) == 1:
                if 'secondary' in c:
                    secondary = secondary[0]
                    channel = secondary.get('channel')
                    if not 'G' in channel:
                        level = secondary.get('level')
                        command = format_command_attr(channel, level)
                        if not in_list_of_commands(command, result):
                            result.append((command, c))

    return result


def button_list():
    """Returns a list of dictionaries.

    Each element is a dictionary with keys:
        page : str
            The page the button is found on
        button : str
            The text on the button
        vectors: list of str
            The vectors associated with the button (usually a primary and a secondary)
    """
    sapphire, index = testpack.get_files_parsed()
    buttons = []
    for page in index.findall('.//Page'):
        name = page.attrib['name']
        pagearea = page.find(".//Panel[@name='Page Area']")
        for button in pagearea.findall(".//MultistateButton"):
            vectors = []
            text = button.attrib['text']
            for action in button.findall(".//Action"):
                vector = action.attrib['command']
                vectors.append(vector)
                new = {
                    'page': name,
                    'button': text,
                    'vectors': list(set(vectors)),
                }
                if new not in buttons:
                    buttons.append(new)
    return buttons


def vector_to_commands_dictionary():
    """Returns a dictionary.

    The return dictionary maps a vector from Sapphire's XML config to a command
    dictionary, containing the command, level, and channel.
    """
    sapphire, index = testpack.get_files_parsed()
    mapping = dict()
    for vector_elem in sapphire.findall(".//vector"):
        vector = vector_elem.attrib['name']
        for scene in vector_elem.findall(".//scene"):
            nary = scene.attrib['name'] # primary or secondary or a guid (for ExclusiveScene)

            if nary == 'primary' or nary == 'secondary':
                vectorstring = 'Vector.' + vector + '.Scene.' + nary
            else:
                vectorstring = 'Vector.' + vector + '.ExclusiveScene.' + nary

            if vectorstring not in mapping:
                mapping[vectorstring] = []

            for channelsetting in scene.findall('channelSetting'):
                command = channelsetting.attrib['command']
                level = channelsetting.attrib['level']
                channel = channelsetting.attrib['channel']
                mapping[vectorstring].append(Command(command=command, level=level, channel=channel))

    return mapping


def commands_to_vector_dictionary():
    vdict = vector_to_commands_dictionary()
    commands_to_vector = dict()
    for vector, commands in vdict.items():
        commandtuple = tuple(commands)
        if commandtuple not in commands_to_vector:
            commands_to_vector[commandtuple] = [vector]
        else:
            commands_to_vector[commandtuple].append(vector)
    return commands_to_vector


if __name__ == '__main__':
    import os
    import json
    import sqlite3
    base = os.path.join('C:\\', 'Users', 'BEK', 'sapphire_regression', 'trunk', 'sapphire_regression', 'scripts', 'data')
    index = os.path.join(base, 'index.xml')
    sapphire = os.path.join(base, 'Sapphire.xml')
    db_path = os.path.join('C:\\', 'Users', 'BEK', 'sapphire_regression', 'trunk', 'sapphire_regression', 'scripts', 'DB')
    buttons = button_list(index, sapphire)

    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    commands = vector_to_commands_dictionary()

    for row in c.execute('SELECT event_name, event_priority, event_time, event_repeat_days, event_command FROM event_tbl'):
        name, priority, time, db_days, vector = row
        print(name)
        print(commands[vector], vector)
        print()
