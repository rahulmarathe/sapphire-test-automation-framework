# config.py
"""This file gathers the locally defined config.py and adds configuration
globals to this import space.

"""
import os
import sys
import importlib.util


class Configuration(object):

    def __init__(self):
        """Loads the user's configuration.

        If the user's current directory has a config.py use the configuration settings
        in that config.py.  Otherwise, use the configuration settings found in the system's
        environment variables.

        If a configuration variable isn't found, set it to None.

        """

        configuration = dict()
        module_path = os.path.join(os.getcwd(), 'config.py')
        if os.path.exists(module_path):
            spec = importlib.util.spec_from_file_location('config', module_path)
            config_module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(config_module)
            # The IP of the Android device
            SAPPHIRE_IP = getattr(config_module, 'SAPPHIRE_IP', None)
            # The path to the Binary csv generator output
            GM_BINARY_CSV_PATH = getattr(config_module, 'GM_BINARY_CSV_PATH', None)
            # The path to the binary csv generator (Labview probably)
            GM_BINARY_EXE_PATH = getattr(config_module, 'GM_BINARY_EXE_PATH', None)
            # The path to the analog csv generator (Labview probably)
            GM_ANALOG_EXE_PATH = getattr(config_module, 'GM_ANALOG_EXE_PATH', None)
            # The path to the analog csv generator output
            GM_ANALOG_CSV_PATH = getattr(config_module, 'GM_ANALOG_CSV_PATH', None)
            # The path to the DS executable
            GM_DS_EXE_PATH = getattr(config_module, 'GM_DS_EXE_PATH', None)
            # The wait time (aka timeout) for any executable or subprocess
            SAPPHIRE_WAIT_TIME = getattr(config_module, 'SAPPHIRE_WAIT_TIME', None)
            # The verbosity, true or false
            SAPPHIRE_VERBOSE = getattr(config_module, 'SAPPHIRE_VERBOSE', None)

        else:
            # The IP of the Android device
            SAPPHIRE_IP = os.getenv('SAPPHIRE_IP')
            # The path to the Binary csv generator output
            GM_BINARY_CSV_PATH = os.getenv('GM_BINARY_CSV_PATH')
            # The path to the binary csv generator (Labview probably)
            GM_BINARY_EXE_PATH = os.getenv('GM_BINARY_EXE_PATH')
            # The path to the analog csv generator (Labview probably)
            GM_ANALOG_EXE_PATH = os.getenv('GM_ANALOG_EXE_PATH')
            # The path to the analog csv generator output
            GM_ANALOG_CSV_PATH = os.getenv('GM_ANALOG_CSV_PATH')
            # The path to the DS executable
            GM_DS_EXE_PATH = os.getenv('GM_DS_EXE_PATH')
            # The wait time (aka timeout) for any executable or subprocess
            SAPPHIRE_WAIT_TIME = os.getenv('SAPPHIRE_WAIT_TIME')
            # The verbosity, true or false
            SAPPHIRE_VERBOSE = os.getenv('SAPPHIRE_VERBOSE')


        self._SAPPHIRE_IP = SAPPHIRE_IP
        self._GM_BINARY_CSV_PATH = GM_BINARY_CSV_PATH
        self._GM_BINARY_EXE_PATH = GM_BINARY_EXE_PATH
        self._GM_ANALOG_CSV_PATH = GM_ANALOG_CSV_PATH
        self._GM_ANALOG_EXE_PATH = GM_ANALOG_EXE_PATH
        self._GM_DS_EXE_PATH = GM_DS_EXE_PATH
        self._SAPPHIRE_WAIT_TIME = SAPPHIRE_WAIT_TIME
        self._SAPPHIRE_VERBOSE = SAPPHIRE_VERBOSE


    @property
    def SAPPHIRE_IP(self):
        return self.get_config_var('_SAPPHIRE_IP')

    @property
    def GM_BINARY_CSV_PATH(self):
        return self.get_config_var('_GM_BINARY_CSV_PATH')

    @property
    def GM_BINARY_EXE_PATH(self):
        return self.get_config_var('_GM_BINARY_EXE_PATH')

    @property
    def GM_ANALOG_EXE_PATH(self):
        return self.get_config_var('_GM_ANALOG_EXE_PATH')

    @property
    def GM_ANALOG_CSV_PATH(self):
        return self.get_config_var('_GM_ANALOG_CSV_PATH')

    @property
    def GM_DS_EXE_PATH(self):
        return self.get_config_var('_GM_DS_EXE_PATH')

    @property
    def SAPPHIRE_WAIT_TIME(self):
        return self.get_config_var('_SAPPHIRE_WAIT_TIME')

    @property
    def SAPPHIRE_VERBOSE(self):
        return self.get_config_var('_SAPPHIRE_VERBOSE')


    def get_config_var(self, var_name):
        attr = getattr(self, var_name, None)
        if not attr:
            msg = 'No configuration variable "{}" found, please define this '.format(var_name.strip('_'))
            msg += 'configuration variable in your environment variables, '
            msg += "or in a 'config.py' file in the current working directory"
            raise RuntimeError(msg)

        return attr
