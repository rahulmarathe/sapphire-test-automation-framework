# adb.py
"""This module provides tools for connecting and interacting with adb."""
import subprocess
import os
import xml.etree.ElementTree as etree
import time
import tempfile
from leviton_test.config import Configuration
from leviton_test.Serial_Open_ADBD import send_serial_command


class ADB(object):

    def __init__(self, *args, **kwargs):
        self.config = Configuration()

    def connect(self):
        if 'serial_port_open' not in os.environ:
            os.environ['serial_port_open'] = 'True'
            send_serial_command(port = 'COM8', baud = '115200')
        """Connect to :arg:`ip` via ADB, raise a RuntimeError if unable to connect."""
        cmd = 'adb connect {}'.format(self.config.SAPPHIRE_IP)
        result = self.command(cmd.split(), stdout=subprocess.PIPE)
        output = result.stdout.decode('utf8')
        if 'unable to connect to' in output:
            message = 'Android Debug Bridge could not connect to {}, try changing the IP in `config.ini` or typing `start adbd` into CLI'.format(self.config.SAPPHIRE_IP)
            raise ConnectionAbortedError(message)

    def command(self, params, *args, **kwargs):
        return subprocess.run(params, *args, **kwargs)

    def wakeup(self):
        self.connect()
        self.command('adb shell input keyevent KEYCODE_WAKEUP'.split())
        time.sleep(1)

    def get_parsed_tree(self):
        self.connect()
        path = self.tree_from_android()
        result = etree.parse(path)
        if os.path.exists(path):
            os.remove(path)
        return result

    def tree_from_android(self):
        """Returns the path to a saved XML view hierarchy of the connected android device's current
        screen.

        Assumes the default location of /storage/emulated/legacy/window_dump.xml for the
        `automator dump`, and a save location of `./views/`.

        """
        self.command('adb shell uiautomator dump'.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        path = tempfile.gettempdir()
        cmd = 'adb pull /storage/emulated/legacy/window_dump.xml {}'.format(path)
        cmd_list = ['adb', 'pull', '/storage/emulated/legacy/window_dump.xml', path]
        self.command(cmd_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        old_file = os.path.join(path, 'window_dump.xml')
        filename = 'temp.xml'
        new_file = os.path.join(path, filename)
        self.replace_file(old_file, new_file)
        return new_file

    def replace_file(self, old_file, new_file):
        if os.path.exists(new_file):
            os.remove(new_file)
        os.rename(old_file, new_file)

    def get_brightness(self):
        result = self.command('adb shell settings get system screen_brightness'.split(), stdout=subprocess.PIPE)
        result = result.stdout.decode('utf8').strip()
        return int(result)
