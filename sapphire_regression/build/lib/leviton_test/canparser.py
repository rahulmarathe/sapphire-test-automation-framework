from canlib import canlib
import unittest
from itertools import zip_longest
from datetime import datetime, timedelta
import pytz
import math
import time


# Format Functions
# ++++++++++++++++
#  The functions in this section are for formatting messages into human-readable form.


def str_from_msg(msg):
    """Returns a formatted string representation of `msg`.

    :param  msg:  The raw message from the Kvaser CANlib, use return value of :py:func:`~leviton_test.canparser.messages`
    :type msg: A list of bytes

    Example


        >>> ch = set_up_channel(0)
        >>> for msg in messages(ch):
        >>>     print(str_from_msg(msg))
        Channel Fade: channel=1, target=0%, time=10.0, increment=0.1, start=0, priority=7
        Channel Fade: channel=2, target=0%, time=20.0, increment=0.1, start=0, priority=7
        .
        .
        .

    """
    formatted = format_msg(msg)
    if formatted == msg:
        return 'No parsing implemented for message: {}'.format(list(msg))
    else:
        return formatted


def format_msg(msg):
    """Returns a formatted string from msg.

    This is a factory function uses the correct format function based on the command_type(msg).
    """
    command = command_type(msg)
    format_func = {
        'Channel Fade': format_channel_fade_msg,
        'Channel Status Publish': format_channel_status_publish_msg,
        'Group Status Publish': format_group_status_publish_msg,
        'Group Fade': format_group_fade_msg,
    }.get(command, None)
    if format_func:
        return format_func(msg)
    else:
        return msg


def format_channel_fade_msg(msg):
    """Returns a human-readable str of msg."""
    channel = channel_number(msg)
    target = target_level(msg)
    time = fade_time(msg)
    increment = increment_value(msg)
    start = start_time(msg)
    priority = priority_level(msg)
    return 'Channel Fade: channel={:d}, target={:.0%}, time={}, increment={}, start={}, priority={}'.format(channel, target, time, increment, start, priority)


def format_channel_status_publish_msg(msg):
    """Returns a human-readable str of a Channel Status Publish msg."""
    # Channel Status Publish channel level w/ Fade
    target = target_level(msg)
    time = fade_time(msg)
    increment = increment_value(msg)
    start = start_time(msg)
    priority = priority_level(msg)
    relativity = 'absolute' if is_absolute_fade(msg) else 'relative'
    if is_fade_time(msg):
        rate_or_time = 'time'
    else:
        rate_or_time = 'rate'

    flag_bits = bits_from_msg(msg)[2]
    if flag_bits[0] == '1':
        channel = channel_number(msg)
    # Channel Status Publish channel level
    else:
        channel = channel_number_for_channel_status_publish_with_channel_level(msg)

    return 'Channel Status Publish: channel={:d}, target={:.0%}, {}={}, increment={}, start={}, priority={}, {} fade'.format(channel, target, rate_or_time, time, increment, start, priority, relativity)


def format_group_status_publish_msg(msg):
    """Returns a human-readable str of a Group Status Publish msg."""
    time = fade_time(msg)
    increment = increment_value(msg)
    start = start_time(msg)
    priority = priority_level(msg)
    group = group_number(msg)
    relativity = 'absolute' if is_absolute_fade(msg) else 'relative'
    rate_or_time = 'time'
    if not is_fade_time(msg):
        rate_or_time = 'rate'
    return 'Group Status Publish: group={}, {}={}, increment={}, start={}, priority={}, {} fade'.format(group, rate_or_time, time, increment, start, priority, relativity)


def format_group_fade_msg(msg):
    group = group_number(msg)
    time = fade_time(msg)
    increment = increment_value(msg)
    start = start_time(msg)
    priority = priority_level(msg)
    target = group_target_level(msg)
    relativity = 'absolute' if is_absolute_fade(msg) else 'relative'
    rate_or_time = 'time'
    if not is_fade_time(msg):
        rate_or_time = 'rate'
    return 'Group Fade: room=N/A, group={:d}, target={:.0%}, time={}, increment={}, start={}, priority={}'.format(group, target, time, increment, start, priority, relativity)


# Message processing
# ++++++++++++++++++
#   The functions in this section are for processing messages


def bits_from_msg(msg):
    """Returns a str of bits from msg.

    :param list msg: A Kvaser CANLib message, (the return value of :py:func:`~leviton_test.canparser.messages`)

    To print messages in bit form.

    .. code-block:: python

        >>> ch = set_up_channel(0)
        >>> for msg in messages(ch):
        >>>     bits = bits_from_msg(msg)
        >>>     print(bits)
        ['10', '1000001', '10', '1110000', '110010', '0', '0', '0']
        ['11111111']
        .
        .
        .

    """
    return ['{0:b}'.format(num) for num in msg]


def command_type(msg):
    """Returns a str of the name of the command type in msg"""
    if len(msg) < 2:
        return 'Not Supported'
    return {
        45: 'Channel Fade',
        50: 'Channel Status Publish',
        70: 'Group Status Publish',
        65: 'Group Fade',
    }.get(msg[1])


def priority_level(msg):
    """Returns the priority int found in msg"""
    bits = bits_from_msg(msg)
    priority = bits[3][:-4]
    return int(priority, base=2)


def group_number(msg):
    """Returns the group number from message."""
    bits = bits_from_msg(msg)
    return int(bits[-2] + bits[-1], base=2)


def start_time(msg):
    """Returns the fade rate start time of a fade rate msg."""
    bits = bits_from_msg(msg)
    start_time_bits = bits[3][-4:][:3]
    return {
        '000': 0,
        '001': 52,
        '010': 564,
        '011': 1588,
        '100': 3636,
        '101': 7732,
        '110': 15924,
        '111': 32308,
    }.get(start_time_bits)


def increment_value(msg):
    "Returns the fade rate/time increment value from msg"
    bits = bits_from_msg(msg)
    start_time_bits = bits[3][-4:][:3]
    return {
        '000': 0.1,
        '001': 1,
        '010': 2,
        '011': 4,
        '100': 8,
        '101': 16,
        '110': 32,
        '111': 64,
    }.get(start_time_bits)


def fade_time(msg):
    """Returns the fade time for a fade time msg."""
    bits = bits_from_msg(msg)
    increment_val = increment_value(msg)
    start = start_time(msg)
    multiplier = int(bits[4], base=2)
    return round(multiplier * increment_val + start, 2)


def fetch_channel_number_function(msg):
    if command_type(msg) == 'Channel Fade':
        return channel_number

    flag_bits = bits_from_msg(msg)[2]
    if flag_bits[0] == '1':
        return channel_number
    else:
        return channel_number_for_channel_status_publish_with_channel_level


def channel_number(msg):
    """Returns the channel number from msg."""
    bits = bits_from_msg(msg)
    return int(bits[5], base=2) - 127


def channel_number_for_channel_status_publish_with_channel_level(msg):
    """Returns the channel number from msg."""
    bits = bits_from_msg(msg)
    return int(bits[4], base=2) - 127


def group_number(msg):
    """Returns the group number from msg."""
    bits = bits_from_msg(msg)
    return int(''.join(bits[6:]) , base=2)


def target_level(msg):
    """Returns the target level found in msg."""
    bits = bits_from_msg(msg)
    return round(int(bits[-1], base=2) / 255, 2)


def group_target_level(msg):
    """Returns the group target level found in msg."""
    bits = bits_from_msg(msg)
    return round(int(bits[-1], base=2) / 255, 2)


def is_fade_rate(msg):
    """Returns True if msg's flag bit indicates it has a fade rate as opposed to a fade time.

    This function assumes msg IS a msg containing a fade rate or fade time, otherwise it will output nonsense.
    """
    return not is_fade_time(msg)


def is_fade_time(msg):
    """Returns True if msg's flag bit indicates it is a fade time as opposed to a fade right.

    This function assumes msg IS a msg containing a fade rate or fade time, otherwise it will output nonsense.
    """
    flag_bits = msg[2]
    if flag_bits > 3:
        raise Exception("Too many flag bits in msg {}, should only be 2 bits".format(msg))
    return flag_bits == 0 or flag_bits == 2


def hardware_level(msg):
    """Returns the HW level found in msg."""
    bits = bits_from_msg(msg)
    return round(int(bits[-2], base=2) / 255,  2)


def is_absolute_fade(msg):
    """Returns True if msg contains an absolute fade as opposed to a relative fade."""
    flag_bits = msg[2]
    if flag_bits > 3:
        raise Exception("Too many flag bits in msg {}, should only be 2 bits".format(msg))
    return flag_bits == 0 or flag_bits == 1


def is_relative_fade(msg):
    """Returns True if msg contains a relative fade as opposed to an absolute fade."""
    return not is_absolute_fade(msg)


# ++++++++++++++++++++++++
# Kvaser API and main loop
# ++++++++++++++++++++++++


def set_up_channel(channel=0,
                 openFlags=canlib.canOPEN_ACCEPT_VIRTUAL,
                 bitrate=canlib.canBITRATE_125K,
                 bitrateFlags=canlib.canDRIVER_NORMAL):
    """Returns a Kvaser CANLib channel.

    Parameters
    ----------
        channel: int
            The channel number to setup.

    Returns
    -------
        canlib.canlib.canChanenl
            The CAN channel to read and write messages from. Also, see: `canlib.canlib.canChannel <https://github.com/Kvaser/canlib-samples/blob/master/Samples/Python/canlib.py#L840/>`_
    Examples
    --------

    >>> from leviton_test.canparser import (
    ...     set_up_channel,
    ...     messages,
    ...     str_from_msg,
    ... )
    >>> channel = set_up_channel(0)
    Using channel: Kvaser Leaf Light HS (channel 0), EAN: 73-30130-00241-8
    >>> type(channel)
    <class 'canlib.canlib.canChannel'>
    >>> for msg in messages(channel):
    >>>     print(str_from_msg(msg))
    Channel Fade: channel=1, target=0%, time=10.0, increment=0.1, start=0, priority=7
    .
    .
    .

    """
    cl = canlib.canlib()
    ch = cl.openChannel(channel, openFlags)
    print("Using channel: %s, EAN: %s" % (ch.getChannelData_Name(),
                                          ch.getChannelData_EAN()))
    ch.setBusOutputControl(bitrateFlags)
    ch.setBusParams(bitrate)
    ch.busOn()
    return ch


def tear_down_channel(ch):
    """Tear down the Kvaser channel.

    Parameters
    ----------
        ch: `canlib.canlib.canChannel <https://github.com/Kvaser/canlib-samples/blob/master/Samples/Python/canlib.py#L840/>`_
            The Kvaser channel that you want to tear down, (was probably the return value of :py:func:`~leviton_test.canparser.set_up_channel`)

    Examples
    --------

    >>> from leviton_test.canparser import (
    ...     set_up_channel,
    ...     tear_down_channel,
    ... )
    >>> channel = set_up_channel(0)
    Using channel: Kvaser Leaf Light HS (channel 0), EAN: 73-30130-00241-8
    >>> tear_down_channel(channel)  # Channel closed

    """
    ch.busOff()
    ch.close()


def broadcast_msg(channel, msg, header=67076191):
    """Writes a message to a Kvaser CAN channel.

    For time messages, see functions :py:func:`~leviton_test.canparser.time_msg_from_datetime` and :py:func:`~leviton_test.canparser.date_msg_from_datetime`.

    Parameters
    ----------
    channel: `canlib.canlib.canChannel <https://github.com/Kvaser/canlib-samples/blob/master/Samples/Python/canlib.py#L840/>`_
        The Kvaser CANLib channel, returned from :py:func:`~leviton_test.canparser.set_up_channel`
    msg: list(int)
        A Kvaser format can message.
    header: int
        The CAN header

    Returns
    -------
    None
        This function sends the msg on the bus and returns None

    Examples
    --------

    >>> from leviton_test.canparser import set_up_channel, broadcast_msg, tear_down_channel
    >>> command = [0, 80, 3, 0] # device status publish
    >>> header = 67076191
    >>> channel = set_up_channel(0)
    >>> broadcast_msg(channel, command, header)
    >>> tear_down_channel(channel)

    """
    channel.write(id_=header, msg=msg, flag=canlib.canMSG_EXT)


def time_msg_from_datetime(datetime):
    """Returns a LumaCAN time message.

    Parameters
    ----------
    datetime: datetime
        The datetime that contains the hour, minute and second for the time message.

    Returns
    -------
    list
        The list of :class:`int` in Kvaser 8-frame format to be broadcast on the bus.

    Note
    ----
    When sending a time message, always send the date message first!  This is the LumaCAN standard.

    Examples
    --------
    Creating and broadcasting a date and time message.  This will broadcast a time change
    to April 4th, 2016 at 10:30:20 AM.

    >>> from datetime import datetime
    >>> from leviton_test.canparser import (
    ...     set_up_channel,
    ...     date_msg_from_datetime,
    ...     time_msg_from_datetime,
    ...     broadcast_msg
    ... )
    >>> channel = set_up_channel(0)
    >>> date_message = date_msg_from_datetime(dt)
    >>> time_message = time_msg_from_datetime(dt)
    >>> broadcast_msg(channel, date_message)
    >>> broadcast_msg(channel, time_message)


    The return value is an 8-frame Kvaser message:

    >>> dt = datetime(2016, 2, 1, 2, 2, 2)
    >>> time_msg_from_datetime(dt)
    [0, 11, 3, 2, 2, 2]


    """
    if is_dst(datetime):
        datetime -= timedelta(hours=1)

    return create_time_msg(hour=datetime.hour, minute=datetime.minute, second=datetime.second)


def create_time_msg(
        hour=datetime.now().hour,
        minute=datetime.now().minute,
        second=datetime.now().second,
        dst_active=False,
        dst_enabled=False,
        dst_pre_2007=False,
        dst_europe=False
        ):
    return [0, 11, 3, hour, minute, second]


def date_msg_from_datetime(datetime):
    """Returns a LumaCAN date message.

    Parameters
    ----------
    datetime: datetime
        The datetime that contains the year, month, and day for the date message.

    Returns
    -------
    list
        The list of :class:`int` in Kvaser 8-frame format to be broadcast on the bus.

    Note
    ----
    When sending a time message, always send the date message first!  This is the LumaCAN standard.

    Examples
    --------
    Creating and broadcasting a date and time message.  This will broadcast a time change
    to April 4th, 2016 at 10:30:20 AM.

    >>> from datetime import datetime
    >>> from leviton_test.canparser import (
    ...     set_up_channel,
    ...     date_msg_from_datetime,
    ...     time_msg_from_datetime,
    ...     broadcast_msg
    ... )
    >>> dt = datetime(2016, 4, 2, 10, 30, 20)
    >>> channel = set_up_channel(0)
    >>> date_message = date_msg_from_datetime(dt)
    >>> time_message = time_msg_from_datetime(dt)
    >>> broadcast_msg(channel, date_message)
    >>> broadcast_msg(channel, time_message)

    """
    return create_date_msg(year=datetime.year-2000, month=datetime.month, day=datetime.day)


def create_date_msg(century=32, year=16, month=12, day=25):
    return [0, 13, century, year, month, day]


def is_dst(datetime):
    tz = pytz.timezone('America/Los_Angeles')
    now = pytz.utc.localize(datetime.utcnow())
    return now.astimezone(tz).dst() != timedelta(0)


def create_header():
    return 67076191


def messages(ch, limit=math.inf, filter=lambda x: x, timeout=0):
    """Returns a generator of only the message part of kvaser output.

    Parameters
    ----------
        ch: canlib.canlib.canChannel
            The channel returned from :py:func:`~leviton_test.canparser.set_up_channel`.  Also, see: `canlib.canlib.canChannel <https://github.com/Kvaser/canlib-samples/blob/master/Samples/Python/canlib.py#L840/>`_
        limit: int
            The max number of messages before tearing down the channel
        filter: func
            A function that returns a boolean, messages will only return messages that return True
        timeout: int
            The time in seconds that messages will loop, leave at zero for infinite loop.

    The following example prints out raw CAN messages.

    .. code-block:: python

        >>> ch = set_up_channel(0)
        >>> for msg in messages(ch):
        >>>     print(msg)
        bytearray(b'\\x00\\rQ\\x0f\\n\\x1d')
        bytearray(b'\\x00\\x0b\\x01\\x13&\\x00')
        .
        .
        .

    """
    for (msgId, msg, dlc, flg, time) in tail_kvaser_channel(ch, timeout=timeout):
        if limit <= 0:
            break
        elif filter(msg):
            limit -= 1
            yield msg
        else:
            pass


def tail_kvaser_channel(ch, timeout=0):
    """Returns a generator of real-time kvaser output.

    This generator behaves like linux's `tail -f` command.

    Usage:

        for (msgId, msg, dlc, flg, time) in tail_kvaser_channel():
            print(msg)

    """
    start = time.time()
    while True:
        elapsed = time.time() - start
        if timeout > 0 and elapsed > timeout:
            raise StopIteration
        try:
            yield ch.read()
        except (canlib.canNoMsg) as ex:
            pass
        except (canlib.canError) as ex:
            pass
