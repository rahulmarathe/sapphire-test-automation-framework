-- Occupancy script
api = require('levitonAPI')

runLumaScript = true
occstate = 0

function Ch2_on_change(channel)
  print('Ch2 changed state')
  api.queryChannel(1)
  api.queryChannel(3)
  api.queryChannel(4)
  api.queryChannel(5)
  api.queryChannel(6)
  api.queryChannel(7)
  api.queryChannel(8)
  api.queryChannel(9)
  api.queryChannel(10)
  api.queryChannel(11)
  api.queryChannel(12)
  api.queryChannel(13)
  api.queryChannel(14)
  api.queryChannel(15)
  api.queryChannel(16)
end


function Ch1_on_nonZero(channel)
  print('Ch1 On')
  api.debug('Ch1 On')
  api.registerEvent('channel', 'onChange', 2, 'Ch2_on_change')
  api.queryInput(50) --query AI 1
end

function Ch1_on_Zero(channel)
  print('Ch1 off')
  api.debug('Ch1 Off')
  api.unRegisterEvent('channel', 'onChange', 2, 'Ch2_on_change')
end

function behavior_change(behavior)
  print('behavior changed to schedule 4')
end


function stop()
    runLumaScript = false
end

function Main()
  api.registerEvent('channel', 'onNonZero', 1, 'Ch1_on_nonZero')
  api.registerEvent('channel', 'onZero', 1, 'Ch1_on_Zero')
  api.registerEvent('behavior', 'onChange', 4, 'behavior_change' )
  while runLumaScript do
    os.sleep(1)
  end
end
