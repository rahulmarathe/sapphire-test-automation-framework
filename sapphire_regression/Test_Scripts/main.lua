-- Occupancy script
api = require('levitonAPI')

runLumaScript = true
photoCellLevel = 0
api.saveConfigParam('photoCellLevel', 0)
pcmax = 255
tolerance = 5
occstate = 0

function occSensorZero(input)
  print('Occ Sensor Unoccupied')
  api.debug('Occ Sensor Unoccupied')
  os.sleep(1)
  occstate = 0
end

function occSensorNonZero(input, value)
  print('Occ Sensor Occupied')
  api.debug('Occ Sensor Occupied')
  os.sleep(1)
  occstate = 1
end


function ch_query_reply(channel)
  print()
end

function level_check(level, target)
  if (((level - tolerance) <= target) and ((level + tolerance) >= target)) then
    return true
  end
  return false
end

function photoCell(input, value)
  print(value)
  if level_check(value, 0) then
    print('Print Statement:Setting PhotoCell to 0')
    api.debug('Debug Statement:Setting PhotoCell to 0')
    api.saveConfigParam('photoCellLevel', 0)

  elseif level_check(value, (255*(10/100))) then
    print('Print Statement:Setting PhotoCell to 1')
    api.debug('Performing Channel Corner Checks')
    api.setChannel({[26113]=190}, 22036, 1)
    api.setChannel({[24219]=54}, 3552, 2)
    api.setChannel({[3537]=52}, 90, 3)
    api.setChannel({[27109]=29}, 43700, 5)
    api.setChannel({[20027]=77}, 9220, 8)
    api.setChannel({[12814]=60}, 250, 10)
    api.setChannel({[14425]=177}, 44916, 11)
    api.setChannel({[18075]=130}, 1700, 12)
    api.setChannel({[6158]=48}, 58612, 13)
    api.setChannel({[500]=101},15316, 14)
    api.setChannel({[25]=102},65012, 15)--fade time out of range
    api.setChannel({[25]=102}, -1, 15)--fade time below range
    api.setChannel({[30]=255}, 1, 18)--priority out of range
    api.setChannel({[32768]=255}, 1, 7)--ch number out of range
    api.setChannel({[-1]=0}, 1, 7)-- ch number below range
    api.setChannel({[-2]=0}, 0, 7)-- ch number below range
    os.sleep(1)
    api.saveConfigParam('photoCellLevel', 1)
    api.registerEvent('channel', 'onChange', 5, 'Ch5_on_change')

  elseif level_check(value, (255*(20/100))) then
    print('Print Statement:Setting PhotoCell to 2')
    os.sleep(1)
    api.saveConfigParam('photoCellLevel', 2)

  elseif level_check(value, (255*(30/100))) then
    print('Print Statement:Setting PhotoCell to 3')
    api.debug('Performing Query Channel Corner Test')
    api.queryChannel(-1)
    api.queryChannel(0)
    api.queryChannel(1333)
    api.queryChannel(30765)
    api.queryChannel(2700)
    api.queryChannel(348)
    api.queryChannel(21025)
    api.queryChannel(26777)
    api.queryChannel(17565)
    api.queryChannel(76984)
    api.queryChannel(15975)
    api.queryChannel(4287)
    api.queryChannel(32767)
    api.queryChannel(32769)--channel number out of range
    os.sleep(1)
    api.saveConfigParam('photoCellLevel', 3)

  elseif level_check(value, (255*(40/100))) then
    print('Print Statement:Setting PhotoCell to 4')
    api.debug('Debug Statement:Setting PhotoCell to 4')
    api.publishInput(0, 0, 10)
    api.publishInput(1, 427, 20)
    api.publishInput(2, 15622, 30, 100)
    api.publishInput(3, 60000, 40, 100)
    api.publishInput(4, 32767, 50, 100)
    api.publishInput(5, 11223, 60)
    api.publishInput(6, 10, 10)
    api.publishInput(3, 49000, 255)
    api.publishInput(3, 65535, 255)
    api.publishInput(3, 65536, 255)
    os.sleep(1)
    api.saveConfigParam('photoCellLevel', 4)


  elseif level_check(value, (255*(50/100))) then
    print('Print Statement:Setting PhotoCell to 5')
    api.debug('Performing Innput Query Corner Test')
    api.queryInput(-2)
    api.queryInput(-1)
    api.queryInput(0)
    api.queryInput(58255)
    api.queryInput(46466)
    api.queryInput(5649)
    api.queryInput(29872)
    api.queryInput(20155)
    api.queryInput(47241)
    api.queryInput(9068)
    api.queryInput(54153)
    api.queryInput(21111)
    api.queryInput(52)
    api.queryInput(65535)
    api.queryInput(65536) -- input number out of range
    api.queryInput(-3) -- input number below range
    os.sleep(1)
    api.saveConfigParam('photoCellLevel', 5)


  elseif level_check(value, (255*(60/100))) then
    print('Print Statement:Setting PhotoCell to 6')
    api.debug('Debug Statement:Setting PhotoCell to 6')
    os.sleep(1)
    api.saveConfigParam('photoCellLevel', 6)

  elseif level_check(value, (255*(70/100))) then
    print('Print Statement:Setting PhotoCell to 7')
    api.debug('Debug Statement:Setting PhotoCell to 7')
    for i = 1,32767,1 do
      for v = 0,255,5 do
        api.setChannel({[i]=v})
      end
    end
    os.sleep(1)
    api.saveConfigParam('photoCellLevel', 7)

  elseif level_check(value, (255*(80/100))) then
    print('Print Statement:Setting PhotoCell to 8')
    api.debug('Debug Statement:Setting PhotoCell to 8')
    for i = 1,1000,1 do
      api.queryChannel(i)
    end
    os.sleep(1)
    api.saveConfigParam('photoCellLevel', 8)

  elseif level_check(value, (255*(90/100))) then
    print('Print Statement:Setting PhotoCell to 9')
    api.saveConfigParam('photoCellLevel', 9)


  elseif level_check(value, (255)) then
    print('Print Statement:Setting PhotoCell to 10')
    api.saveConfigParam('photoCellLevel', 10)


  else
    print('value out of range')
  end
end

function stop()
    runLumaScript = false
end

function Main()
  api.registerEvent('input', 'onZero', -1, 'occSensorZero')
  api.registerEvent('input', 'onNonZero', -1, 'occSensorNonZero')
  api.registerEvent('input', 'onChange', -2, 'photoCell')
  while runLumaScript do
    pcstate = api.getConfigParam('photoCellLevel', 0)
    print('lua: PC: ', pcstate)
    print('lua: Occ: ', occstate)
    os.sleep(5)
  end
end
