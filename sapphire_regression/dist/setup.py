# setup.py
"""Install the Leviton Test Framework.

Requirements
============

1. Python 3.6
2. pip
3. wheel

"""
import argparse
import pip

parser = argparse.ArgumentParser(description='Install the Leviton Test Framework.')
group = parser.add_mutually_exclusive_group()

group.add_argument('--install', help='Install the Leviton Test Framework and dependencies.', action='store_true')
group.add_argument('--uninstall', help='Uninstall the Leviton Test Framework only.', action='store_true')
group.add_argument('--uninstall-all', help='Uninstall the Leviton Test Framework only and dependencies.', action='store_true')

args = parser.parse_args()
if args.install:
    cmd = ['install', '--no-index', '--find-links=./wheelhouse/', 'leviton_test-0.5.0-py3-none-any.whl']
elif args.uninstall:
    cmd = ['uninstall', 'leviton-test']
elif args.uninstall_all:
    cmd = ['uninstall', 'leviton-test', 'numpy', 'pytz', 'canlib', 'astral']
else:
    raise Exception('Please use one of the args --install, --uninstall, or --uninstall-all')
    
pip.main(cmd)
