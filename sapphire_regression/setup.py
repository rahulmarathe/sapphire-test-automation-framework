from setuptools import setup, find_packages

setup(
    name='leviton_test',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='0.5.0',
    description='A leviton_test for testing leviton products',

    # Author details
    author='Bobby Eshleman, Scott Mettlen',
    author_email='bobby.eshleman@leviton.com',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3.5',
    ],

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=['scripts', 'views', 'tests', 'greenmax_mocks']),

    # Alternatively, if you want to distribute just a my_module.py, uncomment
    # this:
    #py_modules=["leviton_test"],

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=['astral', 'numpy', 'canlib', 'pytz', 'pyserial'],
)
