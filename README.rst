Test Automation Framework
=========================

This is the test automation framework for Leviton applications.  It currently supports testing of the Android UI, CAN
network traffic, and has been extended for easy deployment of Sapphire specific tests.


Version Control
===============

Cloning with Git
----------------

This repository depends on the submodule `lumacan3`, to clone this repository in addition to the submodule type the following:

::

    git clone https://bobbyeshleman@bitbucket.org/levitontat/test-automation-framework.git
    cd test-automation-framework/leviton_test/lumacan3
    git submodule init
    git config submodule.sub/sapphire_regression/leviton_test/lumacan3.url https://myuser:mypass@bitbucket.org/levitontat/lumacan3-protocol-debug-tool.git
    git submodule update
    git checkout master

You should now have the main repo and the submodule on their respective master branches.  To learn more about submodules go `here <https://git-scm.com/book/en/v2/Git-Tools-Submodules>`_.